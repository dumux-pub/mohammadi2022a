# Uncertainty-Aware Validation Benchmarks for Coupling Free Flow and Porous-Medium Flow

This repository provides the scripts to perform the uncertainty-aware validation
benchmarks using [BayesValidRox](
  https://git.iws.uni-stuttgart.de/inversemodeling/bayesian-validation
  ) which is an open-source, object-oriented Python package for
surrogate-assisted Bayesain Validation of computational models.
This framework provides an automated workflow for surrogate-based sensitivity
analysis, Bayesian calibration, and validation of computational models with a
modular structure.

There are two ways to use this repository:
* [installing](#installation) everything on your local machine in a Python
virtual environment or
* running a [docker container](#running-the-benchmark-with-docker) with
everything preinstalled on in.

## Computational models
The following three modeling concepts have been compared to a reference solution
provided by a highly-resolved pore-scale Stokes simulation:
* Stokes-Darcy model with a Beaver-Joseph interface coupling condition
* Stokes-Darcy model with an arbitrary interface coupling condition
* Stokes-Pore-Network model

For more information about the investigated model setup, the modeling concepts
and the framework, see [this publication](
  https://arxiv.org/abs/2106.13639
  ).

## Installation
The best practice is to create a virtual environment and install the package inside it.

To create and activate the virtual environment run the following command in the terminal:
```bash
  python3 -m venv ffpm_benchmark
  cd ffpm_benchmark
  source bin/activate
```
You can replace `ffpm_benchmark` with your preferred name. For more information on virtual environments see [this link](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/).

### Requirements
Run the following to install required python packages:
```bash
  pip3 install pip --upgrade
  python3 -m pip install -r requirements.txt
```

Install some necessary prerequisites. You need super user rights.
```bash
sudo apt-get install dvipng texlive-latex-extra texlive-fonts-recommended cm-super
sudo apt-get install python3-vtk7
sudo apt install paraview-python
```
### How-To-Run
Let us first clone this repository:
```bash
git clone https://git.iws.uni-stuttgart.de/dumux-pub/mohammadi2022a.git
```

The second step is to build the models. To do so, you only need to give the following commands:
```bash
cd models
./installdumux.py
```

After successful execution of the commands above, you need to run the following commands to start the Bayesian validation:
```bash
cd ../validation_benchmark
./benchmark_PAA.py
```

## Running the benchmark with docker

To run the validation benchmark using a docker container, you can
simply use the convenience script `docker_ffpm_benchmark.sh`.

To spin up a container simply follow the following steps:
1. create a new folder `ffpm_benchmark`
```bash
mkdir ffpm_benchmark
```
2. navigate to the new folder
```bash
cd ffpm_benchmark
```
4. download the script from the git repository
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/mohammadi2022a/-/raw/master/docker/docker_ffpm_benchmark.sh
chmod u+x docker_ffpm_benchmark.sh
```
3. open the image by running
```bash
./docker_ffpm_benchmark.sh open
```
The container will spin up. It will mount the `ffpm_benchmark`
directory into the container at `/ffpm_benchmark/shared`. Copy files
in this folder to share them with the host machine. These
could be files that you want to visualize on the host machine e.g. pdf files
produced by the scripts. You can use the following command to copy files or an
entire directory to `shared` directory:
```bash
cp [FILE PATH] ~/shared/
cp -R [FOLDER PATH] ~/shared/
```

## Authors
- [@farid](https://git.iws.uni-stuttgart.de/farid)
