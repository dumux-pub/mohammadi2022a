#!/usr/bin/env python3

"""
One click install script for dumux
"""
import os
import sys
import subprocess
import shutil
from distutils.spawn import find_executable
from pkg_resources import parse_version

def show_message(message):
    print("*" * 120)
    print(message)
    print("*" * 120)


def check_cpp_version():
    requiredversion = "7"
    result = subprocess.check_output(["g++", "-dumpversion"]).decode().strip()
    if parse_version(result) < parse_version(requiredversion):
        print("-- An error occured while checking for prerequistes.")
        raise Exception("g++ greater than or equal to {} is required for dumux releases >=3.2!".format(requiredversion))


def run_command(command):
    with open("../installdumux.log", "a") as log:
        popen = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        for line in popen.stdout:
            log.write(line)
            print(line, end='')
        for line in popen.stderr:
            log.write(line)
            print(line, end='')
        popen.stdout.close()
        popen.stderr.close()
        return_code = popen.wait()
        if return_code:
            print("\n")
            message = "\n    (Error) The command {} returned with non-zero exit code\n".format(command)
            message += "\n    If you can't fix the problem yourself consider reporting your issue\n"
            message += "    on the mailing list (dumux@listserv.uni-stuttgart.de) and attach the file 'installdumux.log'\n"
            show_message(message)
            sys.exit(1)


def git_clone(url, branch=None):
    clone = ["git", "clone"]
    if branch:
        clone += ["-b", branch]
    result = run_command(command=[*clone, url])


# clear the log file
open('installdumux.log', 'w').close()

#################################################################
#################################################################
## (1/4) Check some prerequistes
#################################################################
#################################################################
programs = ['wget', 'git', 'gcc', 'g++', 'cmake', 'pkg-config']
show_message("(1/4) Checking all prerequistes: " + " ".join(programs) + "...")

# check some prerequistes
for program in programs:
    if find_executable(program) is None:
        print("-- An error occured while checking for prerequistes.")
        raise Exception("Program {} has not been found.".format(program))

if find_executable('paraview') is None:
    print("-- Warning: paraview seems to be missing. You may not be able to view simulation results!")

check_cpp_version()

show_message("(1/4) Step completed. All prerequistes found.")

#################################################################
#################################################################
## (2/4) Clone modules
#################################################################
#################################################################
show_message("(2/4) Cloning repositories. This may take a while. Make sure to be connected to the internet...")

dune_version=2.8
# the core modules
for module in ['common', 'geometry', 'grid', 'localfunctions', 'istl']:
    if not os.path.exists("dune-{}".format(module)):
        git_clone('https://gitlab.dune-project.org/core/dune-{}.git'.format(module), "releases/{}".format(dune_version))
    else:
        print("-- Skip cloning dune-{} because the folder already exists.".format(module))

# Dune-subgrid
if not os.path.exists("dune-subgrid"):
    git_clone('https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git'.format(module), "releases/{}".format(dune_version))
else:
    print("-- Skip cloning dune-{} because the folder already exists.".format(module))

# Dune-foamgrid
if not os.path.exists("dune-foamgrid"):
    git_clone('https://gitlab.dune-project.org/extensions/dune-foamgrid.git'.format(module), "releases/{}".format(dune_version))
else:
    print("-- Skip cloning dune-{} because the folder already exists.".format(module))

# dumux
if not os.path.exists("dumux"):
    git_clone('https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git', "feature/box-staggered-coupling")
else:
    print("-- Skip cloning dumux because the folder already exists.")

#################################################################
#################################################################
## (3/4) Configure and build
#################################################################
#################################################################
show_message("(3/4) Configure and build dune modules and dumux using dunecontrol. This may take several minutes...")

# run dunecontrol
if not os.path.isfile("cmake.opts"):
    subprocess.run(["wget","https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/raw/releases/3.2/cmake.opts"])
else:
    print("-- The file cmake.opts already exists. The existing file will be used to configure dumux.")

run_command(command=["./dune-common/bin/dunecontrol", "--opts=cmake.opts", "all"])

show_message("(3/4) Step completed. Succesfully configured and built dune and dumux.")

#################################################################
#################################################################
## Build models
#################################################################
#################################################################
show_message("(4/4) Build models...")

# ---- Stokes Model ----
os.chdir('./sfb1313-benchmark-a/build-cmake/scenarios/regular2d/stokes')

# run dunecontrol
run_command(command=["make", "-j", "4", "stokes_regular_2d"])

# Copy executable to the model directory
new_path = '../../../../../../validation_benchmark/models/stokes'
run_command(command=["cp", "stokes_regular_2d", new_path])

# ---- Stokes-Darcy Model ----
os.chdir('../stokesdarcy')

# run dunecontrol
run_command(command=["make", "-j", "4", "stokes_darcybox_regular_2d"])

# Copy executable to the model directory
new_path = '../../../../../../validation_benchmark/models/stokesdarcy'
run_command(command=["cp", "stokes_darcybox_regular_2d", new_path])

# ---- Stokes-PNM Model ----
os.chdir('../../../../../../models/dumux')

# Change branch
run_command(command=["git", "checkout", "master"])

# make executable
os.chdir('../sfb1313-benchmark-a/build-cmake/scenarios/regular2d/stokespnm')
run_command(command=["make", "-j", "4", "stokes_pnm_regular_2d"])

# Copy executable to the model directory
new_path = '../../../../../../validation_benchmark/models/stokespnm'
run_command(command=["cp", "stokes_pnm_regular_2d", new_path])

# Remove dumux and dune
os.chdir('../../../../../')
shutil.rmtree("dumux/")
shutil.rmtree("dune-common/")
shutil.rmtree("dune-foamgrid/")
shutil.rmtree("dune-geometry/")
shutil.rmtree("dune-grid/")
shutil.rmtree("dune-istl/")
shutil.rmtree("dune-localfunctions/")
shutil.rmtree("dune-subgrid/")
os.remove("cmake.opts")
show_message("(4/4) Step completed. Succesfully built models.")

#################################################################
#################################################################
## Show message how to check that everything works
#################################################################
#################################################################
show_message("(Installation complete) To run the bayesian analysis, please run the following commands (can be copied to command line):\n\n"
             "  cd ../validation_benchmark/\n"
             "  ./benchmark_PAA.py\n")
