This module requires to checkout the `feature/box-staggered-coupling` dumux branch
when solving the Stokes-Darcy or Stokes test. And to checkout `master` for the PNM tests.
These dependencies will be resolved soon.

There's currently one scenario implemented in `scenarios/regular2d`.

After running dunecontrol, go to the folder, build and execute:

* for the __Stokes reference__ model

```bash
cd sfb1313-benchmark-a/build-cmake/scenarios/regular2d/stokes
make stokes_regular_2d
./stokes_regular_2d INPUTFILE
```
where `INPUTFILE` is `params_squared_inclusion.input`, `params_squared_inclusion_leftInflow.input` or `params_circular_inclusion.input`.

* for the __Stokes-Darcy coupled__ model
```bash
cd sfb1313-benchmark-a/build-cmake/scenarios/regular2d/stokesdarcy
make stokes_darcy_regular_2d
./stokes_darcy_regular_2d INPUTFILE
```
where `INPUTFILE` is `params_[IC]_squared_inclusion.input` or `params_[IC]_squared_inclusion_leftInflow.input` with `[IC]` being `ER` for the Generalized interface condition and `BJ` for the Classical Beaver-Joseph condition.

* for the __Stokes-PNM__ coupled model
```bash
cd sfb1313-benchmark-a/build-cmake/scenarios/regular2d/stokespnm
make stokes_pnm_regular_2d
./stokes_regular_2d INPUTFILE
```
where `INPUTFILE` is `params_squared_inclusion.input` or `params_squared_inclusion_leftInflow.input`.
