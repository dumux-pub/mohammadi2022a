// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Upscaling problem
 */
 #include <config.h>

 #include <ctime>
 #include <iostream>

 #include <dune/common/parallel/mpihelper.hh>
 #include <dune/common/timer.hh>
 #include <dune/grid/io/file/vtk.hh>
 #include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/io/grid/gridmanager.hh>
#include "elementselector.hh"

#include "../stokesproblem.hh"
#include "properties.hh"

template<class FluxOverSurface, class GridVariables, class SolutionVector>
void getOutletFluxes(const GridVariables& gridVars, const SolutionVector& sol)
{
    FluxOverSurface flux(gridVars, sol);
    using GridGeometry = typename GridVariables::GridGeometry;
    const GridGeometry& gridGeometry = gridVars.gridGeometry();

    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    const auto xMax = gridGeometry.bBoxMax()[0];
    const auto yMin = gridGeometry.bBoxMin()[1];
    const auto yMax = gridGeometry.bBoxMax()[1];
#if DIM == 3
    const auto zMin = gridGeometry.bBoxMin()[2];
    const auto zMax = gridGeometry.bBoxMax()[2];
#endif

    // The second plane is placed at the outlet of the channel.
#if DIM == 3
    const auto p0outlet = GlobalPosition{xMax, yMin, zMin};
    const auto p1outlet = GlobalPosition{xMax, yMax, zMin};
    const auto p2outlet = GlobalPosition{xMax, yMin, zMax};
    const auto p3outlet = GlobalPosition{xMax, yMax, zMax};
    flux.addSurface("outlet", p0outlet, p1outlet, p2outlet, p3outlet);
#else
    const auto p0outlet = GlobalPosition{xMax, yMin};
    const auto p1outlet = GlobalPosition{xMax, yMax};
    flux.addSurface("outlet", p0outlet, p1outlet);
#endif

    flux.calculateMassOrMoleFluxes();

    const auto horizontalFlux = flux.netFlux("outlet");

    std::cout << "\n##################################\n";
    std::cout << "\nFluxes:\n";
    std::cout << "\nhorizontal: " << horizontalFlux << "\n" <<  std::endl;
}

template<class FluxOverSurface, class GridVariables, class SolutionVector>
void getThroatAveragedValues(const GridVariables& gridVars, const SolutionVector& sol, const std::string& paramGroup = "")
{
    FluxOverSurface flux(gridVars, sol);
    using GridGeometry = typename GridVariables::GridGeometry;
    const GridGeometry& gridGeometry = gridVars.gridGeometry();

    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

#if DIM == 3
    static_assert(false);
#endif

    const auto numThroats = Dumux::getParamFromGroup<int>(paramGroup, "ThroatAveragedValues.NumThroats");
    std::vector<double> areas;

    for (int i = 0; i < numThroats; ++i)
    {
        // example Throat0 = 0 0 1 2 # xmin xmax ymin ymax
        const auto throatCoords = Dumux::getParamFromGroup<std::array<double, 4>>(paramGroup, "ThroatAveragedValues.Throat" + std::to_string(i));
        const auto p0 = GlobalPosition{throatCoords[0], throatCoords[2]};
        const auto p1 = GlobalPosition{throatCoords[1], throatCoords[3]};
        areas.push_back(std::max(throatCoords[1]-throatCoords[0], throatCoords[3]-throatCoords[2]));
        std::cout << "Adding " << "Throat" + std::to_string(i) << " at: min = " << p0 << ", max = " << p1 << std::endl;
        flux.addSurface("Throat" + std::to_string(i), p0, p1);
    }

    flux.calculateMassOrMoleFluxes();

    std::cout << "\n##################################\n";

    for (int i = 0; i < numThroats; ++i)
    {
        const auto throat = "Throat " + std::to_string(i);
        const auto massFlux = flux.netFlux("Throat" + std::to_string(i));
        static const auto rho = Dumux::getParamFromGroup<double>(paramGroup, "Component.LiquidDensity");
        std::cout << throat << ": mass flux: " << massFlux << ", area: " << areas[i] << " mean velocity: " << (massFlux/rho)/areas[i] << std::endl;
    }
}


int main(int argc, char** argv)
{
    using namespace Dumux;

    Dune::Timer timer;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::StokesProblem;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using HostGrid = typename GetProp<TypeTag, Properties::Grid>::HostGrid;

    const auto paramGroup = std::to_string(DIM) + "D";

    // try to create a grid (from the given grid file or the input file)
    GridManager<HostGrid> hostGridManager;
    using Scalar = typename HostGrid::ctype;
    using Element = typename Grid::LeafGridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using IntVector = std::vector<int>;
    using ScalarVector = std::vector<Scalar>;

    const auto lowerLeft = getParamFromGroup<GlobalPosition>(paramGroup, "Grid.LowerLeft");
    const auto upperRight = getParamFromGroup<GlobalPosition>(paramGroup, "Grid.UpperRight");

    std::array<ScalarVector, DIM> positions;
    positions[0].push_back(lowerLeft[0]);
    positions[0].push_back(upperRight[0]);
    positions[1].push_back(lowerLeft[1]);
    const auto cavityHeight = getParamFromGroup<Scalar>(paramGroup, "Grid.CavityHeight");
    const auto interfaceOffsetY = getParamFromGroup<Scalar>(paramGroup, "Grid.InterfaceOffsetY", 0.0);
    positions[1].push_back(lowerLeft[1] + cavityHeight + interfaceOffsetY);
    positions[1].push_back(upperRight[1]);

    // the number of cells
    std::array<IntVector, DIM> cells;
    const auto cellsInput = getParamFromGroup<IntVector>(paramGroup, "Grid.Cells");
    const int cellsYPorousMedium = std::ceil(cavityHeight / (upperRight[1] - lowerLeft[1]) * cellsInput[1]);
    cells[0] = IntVector{cellsInput[0]};
    cells[1] = IntVector{cellsYPorousMedium, cellsInput[1]-cellsYPorousMedium};

    // grading factor (has a default)
    std::array<ScalarVector, DIM> grading;
    for (int i = 0; i < DIM; ++i)
    {
        grading[i].resize(positions[i].size()-1, 1.0);
        // grading[i] = getParamFromGroup<ScalarVector>(paramGroup, "Grid.Grading" + std::to_string(i), grading[i]);
    }

    hostGridManager.init(positions, cells, grading, paramGroup);

    ElementSelector<typename HostGrid::LeafGridView> elementSelector;

    Dumux::GridManager<Grid> subgridManager;
    subgridManager.init(hostGridManager.grid(), elementSelector, paramGroup);

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = subgridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
    gridGeometry->update();

    // the problem (boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = leafGridView.size(0);
    const auto numDofsFace = leafGridView.size(1);
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOField = GetPropType<TypeTag, Properties::IOFields>;
    StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOField::initOutputModule(vtkWriter); //!< Add model specific output fields

    // use the staggered FV assembler
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    using FluxOverSurface = Dumux::FluxOverSurface<GridVariables,
                                                   SolutionVector,
                                                   GetPropType<TypeTag, Properties::ModelTraits>,
                                                   GetPropType<TypeTag, Properties::LocalResidual>>;

    // linearize & solve
    Dune::Timer assembleAndSolveTimer;

    nonLinearSolver.solve(x);

    std::cout << "Assembly and solve took " << assembleAndSolveTimer.elapsed() << " seconds" << std::endl;
    std::cout << "numFaceDofs: " << numDofsFace << ", numCellCenterDofs: " << numDofsCellCenter << ", total: " << numDofsFace + numDofsCellCenter << std::endl;

    // write vtk output
    vtkWriter.write(1.0);

    getOutletFluxes<FluxOverSurface>(*gridVariables, x);
    getThroatAveragedValues<FluxOverSurface>(*gridVariables, x);

    const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
    std::cout << "Simulation took " << timer.elapsed() << " seconds on "
              << comm.size() << " processes.\n"
              << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
