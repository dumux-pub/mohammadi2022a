// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Auxiliary class used to select the elements for the final grid
 */
#ifndef DUMUX_MICROMODEL_ELEMENT_SELECTOR_HH
#define DUMUX_MICROMODEL_ELEMENT_SELECTOR_HH

#include <string>
#include <dune/common/fvector.hh>
#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dumux/geometry/intersectspointgeometry.hh>

namespace Dumux
{

/*!
 * \brief Auxiliary class used to select the elements for the final grid
 */
template <class GridView>
class ElementSelector
{
    using Scalar = typename GridView::ctype;
    static constexpr int coordDim = 2;

    using GlobalPosition = Dune::FieldVector<Scalar, coordDim>;
    using Rectangle = Dune::AxisAlignedCubeGeometry<Scalar, coordDim, coordDim>;

public:
    ElementSelector(const std::string& modelParamGroup = "")
    {
        const int numChannelsY = getParamFromGroup<int>(modelParamGroup, "Grid.NumChannelsY");
        const int numChannelsX = getParamFromGroup<int>(modelParamGroup, "Grid.NumChannelsX");
        const int numPillarsX = numChannelsX + 1;
        const int numPillarsY = numChannelsY + 1;

        const Scalar channelWidthX = getParamFromGroup<Scalar>(modelParamGroup, "Grid.ChannelWidthX");
        const Scalar channelWidthY = getParamFromGroup<Scalar>(modelParamGroup, "Grid.ChannelWidthY");
        const Scalar pillarWidthX = getParamFromGroup<Scalar>(modelParamGroup, "Grid.PillarWidthX");
        const Scalar pillarWidthY =  getParamFromGroup<Scalar>(modelParamGroup, "Grid.PillarWidthY");

        const Scalar lengthCavity = pillarWidthX*numPillarsX + numChannelsX*channelWidthX;
        const Scalar heightCavity = pillarWidthY*numPillarsY + numChannelsY*channelWidthY;

        upperRight_[0] = lengthCavity;
        upperRight_[1] = heightCavity;

        const int numCellsXInput = getParamFromGroup<std::vector<int>>(modelParamGroup, "Grid.Cells")[0];
        const int numCellsYInput = getParamFromGroup<std::vector<int>>(modelParamGroup, "Grid.Cells")[1];
        const auto upperRightInput = getParamFromGroup<std::vector<Scalar>>(modelParamGroup, "Grid.UpperRight");

        const Scalar gridCellSizeX = upperRightInput[0] / numCellsXInput;
        const Scalar gridCellSizeY = upperRightInput[0] / numCellsYInput;


        auto isMultipleOf = [](Scalar big, Scalar small)
        {
            const Scalar eps = 1e-10;
            return (std::abs(std::remainder(big, small)) < eps);
        };


        if (!isMultipleOf(pillarWidthX, gridCellSizeX))
            DUNE_THROW(Dumux::ParameterException, "Grid cell size in x direction (" <<  gridCellSizeX  << ") is not a multiple of the pillar width (" <<  pillarWidthX  <<") ).");

        if (!isMultipleOf(pillarWidthY, gridCellSizeY))
            DUNE_THROW(Dumux::ParameterException, "Grid cell size in y direction (" <<  gridCellSizeY  << ") is not a multiple of the pillar width (" <<  pillarWidthY  <<") ).");

        if (!isMultipleOf(channelWidthX, gridCellSizeX))
            DUNE_THROW(Dumux::ParameterException, "Grid cell size in x direction (" <<  gridCellSizeX  << ") is not a multiple of the channel width (" <<  channelWidthX  <<") ).");

        if (!isMultipleOf(channelWidthY, gridCellSizeY))
            DUNE_THROW(Dumux::ParameterException, "Grid cell size in y direction (" <<  gridCellSizeY  << ") is not a multiple of the channel width (" <<  channelWidthY  <<") ).");

        // std::cout << std::setprecision(15) << "lengthCavity " << lengthCavity << std::endl;
        // std::cout << std::setprecision(15) << "numChannelsX " << numChannelsX << std::endl;
        // std::cout << std::setprecision(15) << "channelWidthX " << channelWidthX << std::endl;
        // std::cout << std::setprecision(15) << "numPillarsX " << numPillarsX << std::endl;
        // std::cout << std::setprecision(15) << "pillarWidthX " << pillarWidthX << std::endl;

        Scalar offsetX = 0.0;
        Scalar offSetY = 0.0;

        for (int iX = 0; iX < numPillarsX; ++iX)
        {
            const Scalar lowerLeftX = offsetX;
            const Scalar upperRightX = lowerLeftX+pillarWidthX;
            for(int iY = 0; iY < numPillarsY; ++iY)
            {
                const Scalar lowerLeftY = offSetY;
                const Scalar upperRightY = lowerLeftY+pillarWidthY;
                list_.emplace_back(GlobalPosition{lowerLeftX, lowerLeftY}, GlobalPosition{upperRightX, upperRightY});
                offSetY += channelWidthY+pillarWidthY;
            }
            offsetX += channelWidthX+pillarWidthX;
            offSetY = 0.0;
        }
    }


    //! Select all elements within a circle around a center point.
    template<class Element>
    bool operator() (const Element& element) const
    {
        const Scalar eps = 1e-8;

        for (int i = 0; i < element.geometry().corners(); ++i)
        {
            const auto corner = element.geometry().corner(i);
            const auto eX = corner[0];
            const auto eY = corner[1];

            if (eX > upperRight_[0] + eps || eY > upperRight_[1] + eps)
                return false;
        }

        const auto center = element.geometry().center();

        for (auto&& rectangle : list_)
            if(intersectsPointGeometry(center, rectangle))
                return false;

        return true;
    }

private:
    std::vector<Rectangle> list_;
    GlobalPosition upperRight_;
};

} //end namespace

#endif
