// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Upscaling problem
 */
 #include <config.h>

 #include <ctime>
 #include <iostream>

 #include <dune/common/parallel/mpihelper.hh>
 #include <dune/common/timer.hh>
 #include <dune/grid/io/file/vtk.hh>
 #include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/io/grid/gridmanager.hh>
#include "elementselector.hh"

#include "problem.hh"

template<class FluxOverSurface, class GridGeometry>
void getFluxes(FluxOverSurface& flux, const GridGeometry& gridGeometry)
{
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    const auto xMin = gridGeometry.bBoxMin()[0];
    const auto yMin = gridGeometry.bBoxMin()[1];
    const auto yMax = gridGeometry.bBoxMax()[1];
#if DIM == 3
    const auto zMin = gridGeometry.bBoxMin()[2];
    const auto zMax = gridGeometry.bBoxMax()[2];
#endif

    // The second plane is placed at the outlet of the channel.
#if DIM == 3
    const auto p0inlet = GlobalPosition{xMin, yMin, zMin};
    const auto p1inlet = GlobalPosition{xMin, yMax, zMin};
    const auto p2inlet = GlobalPosition{xMin, yMin, zMax};
    const auto p3inlet = GlobalPosition{xMin, yMax, zMax};
    flux.addSurface("inlet", p0inlet, p1inlet, p2inlet, p3inlet);
#else
    const auto p0inlet = GlobalPosition{xMin, yMin};
    const auto p1inlet = GlobalPosition{xMin, yMax};
    flux.addSurface("inlet", p0inlet, p1inlet);
#endif

    flux.calculateMassOrMoleFluxes();

    const auto horizontalFlux = flux.netFlux("inlet");

    std::cout << "\n##################################\n";
    std::cout << "\nTotal horizontal mass flow: " << horizontalFlux << " kg/s (per m in 2D)\n" <<  std::endl;
}


int main(int argc, char** argv)
{
    using namespace Dumux;

    Dune::Timer timer;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::UpscalingProblem;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using HostGrid = typename GetProp<TypeTag, Properties::Grid>::HostGrid;

    // try to create a grid (from the given grid file or the input file)
    // GridManager<HostGrid> hostGridManager;
    // hostGridManager.init(std::to_string(DIM) + "D");

    ElementSelector<typename HostGrid::LeafGridView> elementSelector;

    Dumux::GridManager<Grid> subgridManager;
    subgridManager.init(elementSelector, std::to_string(DIM) + "D");

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = subgridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
    gridGeometry->update();

    // the problem (boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = leafGridView.size(0);
    const auto numDofsFace = leafGridView.size(1);
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOField = GetPropType<TypeTag, Properties::IOFields>;
    StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOField::initOutputModule(vtkWriter); //!< Add model specific output fields
    vtkWriter.write(0.0);

    // use the staggered FV assembler
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // linearize & solve
    Dune::Timer assembleAndSolveTimer;

    nonLinearSolver.solve(x);

    std::cout << "Assembly and solve took " << assembleAndSolveTimer.elapsed() << " seconds" << std::endl;
    std::cout << "numFaceDofs: " << numDofsFace << ", numCellCenterDofs: " << numDofsCellCenter << ", total: " << numDofsFace + numDofsCellCenter << std::endl;

    // write vtk output
    vtkWriter.write(1.0);

    FluxOverSurface<GridVariables,
                    SolutionVector,
                    GetPropType<TypeTag, Properties::ModelTraits>,
                    GetPropType<TypeTag, Properties::LocalResidual>> flux(*gridVariables, x);

    getFluxes(flux, *gridGeometry);

    const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
    std::cout << "Simulation took " << timer.elapsed() << " seconds on "
              << comm.size() << " processes.\n"
              << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
