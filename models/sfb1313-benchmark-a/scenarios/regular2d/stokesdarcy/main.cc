// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A test problem for the coupled Stokes/Darcy problem (1p).
 */

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>

#include <dune/grid/yaspgrid.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include "darcyproblem.hh"
#include "properties.hh"

#include <dumux/geometry/intersectspointgeometry.hh>
#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>
#include <dumux/common/math.hh>

namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::StokesTypeTag>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::DARCYTYPETAG>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::DARCYTYPETAG>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::StokesTypeTag, Properties::TTag::StokesTypeTag, TypeTag>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DARCYTYPETAG>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    static constexpr auto dim = 2;

public:
    // using HostGrid = Dune::YaspGrid<dim>;
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
    using type = Dune::SubGrid<dim, HostGrid>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    static constexpr auto dim = 2;

public:
    // using HostGrid = Dune::YaspGrid<dim>;
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
    using type = Dune::SubGrid<dim, HostGrid>;
};

} // end namespace Properties
} // end namespace Dumux

int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using StokesTypeTag = Properties::TTag::StokesTypeTag;
    using DarcyTypeTag = Properties::TTag::DARCYTYPETAG;

    using Grid = GetPropType<StokesTypeTag, Properties::Grid>;
    using HostGrid = typename GetProp<StokesTypeTag, Properties::Grid>::HostGrid;

    using Scalar = typename HostGrid::ctype;

    // change grid to account for interface location
    const Scalar lengthCavity = getParamFromGroup<Scalar>("Stokes", "Grid.CavityLength");
    const Scalar heightCavity = getParamFromGroup<Scalar>("Stokes", "Grid.CavityHeight");
    const Scalar inletLength = getParamFromGroup<Scalar>("Stokes", "Grid.InletLength");

    const auto positions0 = getParam<std::vector<Scalar>>("Grid.Positions0");
    const auto positions1 = getParam<std::vector<Scalar>>("Grid.Positions1");
    const auto cells1 = getParam<std::vector<int>>("Grid.Cells1");

    std::vector<Scalar> newPositions1;
    std::vector<int> newCells1;

    newPositions1.push_back(positions1[0]);

    auto yInterface = positions1[0] + heightCavity;


    if(positions1.size() != 4 || cells1.size() != 3)
        DUNE_THROW(Dune::InvalidStateException, "Variable interface does not work for the given grid parameters!");

    auto yCVPos1 = positions1[1]; 
    auto yCVPos2 = positions1[2];

    // We assume a constant dy
    Scalar dy = (positions1[2] - positions1[1]) / cells1[1];

    if(std::abs(yCVPos1 - yInterface) < dy)
        DUNE_THROW(Dune::InvalidStateException, "Wrong positions in Grid.Positions1, second entry too close at interface!");

    bool dropYPos2 = false;
    if(std::abs(yCVPos2 - yInterface) < 0.99*dy/2.0)
        dropYPos2 = true;

    newPositions1.push_back(yCVPos1);
    newCells1.push_back(cells1[0]);

    auto addIntervallData = [&] (const auto& y1, const auto& y2) 
                            {
                                newPositions1.push_back(y1);
                                newCells1.push_back(std::round(std::abs(y1-y2)/dy)); 
                            };

    if(yInterface < yCVPos2)
    {
        addIntervallData(yInterface, yCVPos1);

        if(!dropYPos2)
            addIntervallData(yCVPos2, yInterface);

        addIntervallData(positions1.back(), dropYPos2 ? yInterface : yCVPos2);
    }
    else
    {
        if(!dropYPos2)
        {
            addIntervallData(yCVPos2, yCVPos1);
            addIntervallData(yInterface, yCVPos2);
        }
        else
        {
            addIntervallData(yInterface, yCVPos1);
        }

        addIntervallData(positions1.back(), yInterface);
        
    }
    

    std::ostringstream newPositions1Str;
    std::copy(newPositions1.begin(), newPositions1.end()-1, std::ostream_iterator<Scalar>(newPositions1Str, " "));
    newPositions1Str << newPositions1.back();

    std::ostringstream newCells1Str;
    std::copy(newCells1.begin(), newCells1.end()-1, std::ostream_iterator<Scalar>(newCells1Str, " "));
    newCells1Str << newCells1.back();

    std::cout << "newPos: "<< newPositions1Str.str() << std::endl;
    std::cout << "newCells: "<< newCells1Str.str() << std::endl;

    Parameters::init([&](auto& params){ 
                                        params["Grid.Positions1"] = newPositions1Str.str(); 
                                        params["Grid.Cells1"] = newCells1Str.str();
                                     });

    // try to create a grid (from the given grid file or the input file)
    const auto paramGroup = std::to_string(2) + "D";

    // try to create a grid (from the given grid file or the input file)
    GridManager<HostGrid> hostGridManager;
    using Element = typename Grid::LeafGridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    hostGridManager.init();

    Dune::VTKWriter<typename HostGrid::LeafGridView> vtkWriter(hostGridManager.grid().leafGridView());
    vtkWriter.write("host");

    using Rectangle = Dune::AxisAlignedCubeGeometry<Scalar, 2, 2>;

    const auto lowerLeft = GlobalPosition{positions0[0], positions1[0]};

    const Rectangle belowInlet(lowerLeft, GlobalPosition{lowerLeft[0]+inletLength, heightCavity});
    const Rectangle belowOutlet(GlobalPosition{inletLength+lengthCavity+lowerLeft[0], lowerLeft[1]}, GlobalPosition{positions0.back(), positions1[0] + heightCavity});

    auto elementSelectorStokes = [&](const auto& element)
    {
        const auto& center = element.geometry().center();
        return center[1] > positions1[0]+heightCavity;
    };

    auto elementSelectorDarcy = [&](const auto& element)
    {
        const auto& center = element.geometry().center();
        if (center[1] > positions1[0]+heightCavity)
            return false;
        if (intersectsPointGeometry(center, belowInlet))
            return false;
        if (intersectsPointGeometry(center, belowOutlet))
            return false;
        return true;
    };

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using DarcyGridManager = Dumux::GridManager<GetPropType<DarcyTypeTag, Properties::Grid>>;
    DarcyGridManager darcyGridManager;
    darcyGridManager.init(hostGridManager.grid(), elementSelectorDarcy, "Darcy"); // pass parameter group

    using StokesGridManager = Dumux::GridManager<GetPropType<StokesTypeTag, Properties::Grid>>;
    StokesGridManager stokesGridManager;
    stokesGridManager.init(hostGridManager.grid(), elementSelectorStokes, "Stokes"); // pass parameter group

    // we compute on the leaf grid view
    const auto& darcyGridView = darcyGridManager.grid().leafGridView();
    const auto& stokesGridView = stokesGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using StokesFVGridGeometry = GetPropType<StokesTypeTag, Properties::GridGeometry>;
    auto stokesFvGridGeometry = std::make_shared<StokesFVGridGeometry>(stokesGridView);
    stokesFvGridGeometry->update();
    using DarcyFVGridGeometry = GetPropType<DarcyTypeTag, Properties::GridGeometry>;
    auto darcyFvGridGeometry = std::make_shared<DarcyFVGridGeometry>(darcyGridView);
    darcyFvGridGeometry->update();

    using Traits = StaggeredMultiDomainTraits<StokesTypeTag, StokesTypeTag, DarcyTypeTag>;

    // the coupling manager
    using CouplingManager = StokesDarcyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(stokesFvGridGeometry, darcyFvGridGeometry);

    // the indices
    constexpr auto freeFlowCellCenterIdx = CouplingManager::freeFlowCellCenterIdx;
    constexpr auto freeFlowFaceIdx = CouplingManager::freeFlowFaceIdx;
    constexpr auto porousMediumIdx = CouplingManager::porousMediumIdx;

    // the problem (initial and boundary conditions)
    using StokesProblem = GetPropType<StokesTypeTag, Properties::Problem>;
    auto stokesProblem = std::make_shared<StokesProblem>(stokesFvGridGeometry, couplingManager);
    using DarcyProblem = GetPropType<DarcyTypeTag, Properties::Problem>;
    auto darcyProblem = std::make_shared<DarcyProblem>(darcyFvGridGeometry, couplingManager);

    // the solution vector
    Traits::SolutionVector sol;
    sol[freeFlowCellCenterIdx].resize(stokesFvGridGeometry->numCellCenterDofs());
    sol[freeFlowFaceIdx].resize(stokesFvGridGeometry->numFaceDofs());
    sol[porousMediumIdx].resize(darcyFvGridGeometry->numDofs());

    // get a solution vector storing references to the two Stokes solution vectors
    auto stokesSol = partial(sol, freeFlowFaceIdx, freeFlowCellCenterIdx);

    couplingManager->init(stokesProblem, darcyProblem, sol);

    // the grid variables
    using StokesGridVariables = GetPropType<StokesTypeTag, Properties::GridVariables>;
    auto stokesGridVariables = std::make_shared<StokesGridVariables>(stokesProblem, stokesFvGridGeometry);
    stokesGridVariables->init(stokesSol);
    using DarcyGridVariables = GetPropType<DarcyTypeTag, Properties::GridVariables>;
    auto darcyGridVariables = std::make_shared<DarcyGridVariables>(darcyProblem, darcyFvGridGeometry);
    darcyGridVariables->init(sol[porousMediumIdx]);

    // intialize the vtk output module
    const auto stokesName = getParam<std::string>("Problem.Name") + "_" + stokesProblem->name();
    const auto darcyName = getParam<std::string>("Problem.Name") + "_" + darcyProblem->name();

    // intialize the vtk output module
    StaggeredVtkOutputModule<StokesGridVariables, decltype(stokesSol)> stokesVtkWriter(*stokesGridVariables, stokesSol, stokesName);
    GetPropType<StokesTypeTag, Properties::IOFields>::initOutputModule(stokesVtkWriter);
    std::vector<Scalar> stokesVolume(stokesGridView.size(0), 0.0);
    stokesVtkWriter.addField(stokesVolume, "volume");

    VtkOutputModule<DarcyGridVariables, GetPropType<DarcyTypeTag, Properties::SolutionVector>> darcyVtkWriter(*darcyGridVariables, sol[porousMediumIdx], darcyName);
    using DarcyVelocityOutput = GetPropType<DarcyTypeTag, Properties::VelocityOutput>;
    darcyVtkWriter.addVelocityOutput(std::make_shared<DarcyVelocityOutput>(*darcyGridVariables));
    GetPropType<DarcyTypeTag, Properties::IOFields>::initOutputModule(darcyVtkWriter);
    
    std::vector<Scalar> cellPressure(darcyGridView.size(0), 0.0);
    std::vector<Scalar> darcyVolume(darcyGridView.size(0), 0.0);
    std::vector<GlobalPosition> cellVelocity(darcyGridView.size(0), GlobalPosition(0.0));
    darcyVtkWriter.addField(cellPressure, "cellP");
    darcyVtkWriter.addField(darcyVolume, "volume");
    darcyVtkWriter.addField(cellVelocity, "cellVelocity");

    // the assembler for a stationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(stokesProblem, stokesProblem, darcyProblem),
                                                 std::make_tuple(stokesFvGridGeometry->faceFVGridGeometryPtr(),
                                                                 stokesFvGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 darcyFvGridGeometry),
                                                 std::make_tuple(stokesGridVariables->faceGridVariablesPtr(),
                                                                 stokesGridVariables->cellCenterGridVariablesPtr(),
                                                                 darcyGridVariables),
                                                 couplingManager);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    // solve the non-linear system
    nonLinearSolver.solve(sol);

    Scalar permeability = getParam<Scalar>("Darcy.SpatialParams.Permeability");
    for(const auto& element : elements(darcyGridView))
    {
        auto fvGeometry = localView(*darcyFvGridGeometry);
        fvGeometry.bind(element);

        auto elemVolVars = localView(darcyGridVariables->curGridVolVars());
        elemVolVars.bind(element, fvGeometry, sol[porousMediumIdx]);
        const auto eIdx = darcyFvGridGeometry->elementMapper().index(element);

        const auto geometry = element.geometry();

        const auto& localBasis = fvGeometry.feLocalBasis();
        using ShapeValue = typename Dune::FieldVector<double, 1>;
        std::vector<ShapeValue> shapeValues;
        // evaluate gradP - rho*g at integration point
        Dune::FieldVector<Scalar, DarcyFVGridGeometry::GridView::dimension> gradP(0.0);

        // evaluate shape functions and gradients at the integration point
        const auto ipLocal = geometry.local(geometry.center());
        localBasis.evaluateFunction(ipLocal, shapeValues);

        using FeCache = Dune::PQkLocalFiniteElementCache<DarcyFVGridGeometry::GridView::ctype, Scalar, DarcyFVGridGeometry::GridView::dimension, 1>;
        using FeLocalBasis = typename FeCache::FiniteElementType::Traits::LocalBasisType;
        using ShapeJacobian = typename FeLocalBasis::Traits::JacobianType;

        std::vector<ShapeJacobian> shapeJacobian;
        const auto jacInvT = geometry.jacobianInverseTransposed(ipLocal);
        localBasis.evaluateJacobian(ipLocal, shapeJacobian);

        darcyVolume[eIdx] = element.geometry().volume();

        for (const auto& scv : scvs(fvGeometry))
        {
            const auto& volVars = elemVolVars[scv];

            // the global shape function gradient
            GlobalPosition gradN(0.0);
            jacInvT.mv(shapeJacobian[scv.localDofIndex()][0], gradN);
            gradP.axpy(volVars.pressure()/volVars.viscosity(), gradN);
            cellPressure[eIdx] += shapeValues[scv.indexInElement()][0]*volVars.pressure();
        }
        cellVelocity[eIdx] = -permeability*gradP;
    }

    for(const auto& element : elements(stokesGridView))
    {
        const auto eIdx = stokesFvGridGeometry->elementMapper().index(element);
        stokesVolume[eIdx] = element.geometry().volume();
    }

    // write vtk output
    stokesVtkWriter.write(1.0);
    darcyVtkWriter.write(1.0);

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
