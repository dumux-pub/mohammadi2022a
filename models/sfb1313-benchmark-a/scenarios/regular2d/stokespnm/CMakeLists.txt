add_input_file_links()
dune_symlink_to_source_files(FILES grids)
dune_symlink_to_source_files(FILES "averaging.py")

dune_add_test(SOURCES micromodel_coupled.cc
              NAME stokes_pnm_regular_2d
              CMAKE_GUARD HAVE_UMFPACK dune-subgrid_FOUND)
