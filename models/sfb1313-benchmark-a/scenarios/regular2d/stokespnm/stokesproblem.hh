// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */
#ifndef DUMUX_CHANNEL_TEST_PROBLEM_HH
#define DUMUX_CHANNEL_TEST_PROBLEM_HH

#include <dumux/common/numeqvector.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

#include "../stokesproblem.hh"

namespace Dumux
{
template <class TypeTag>
class ChannelTestProblem;


/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the one-phase (Navier-) Stokes problem in a channel.
 * \todo doc me!
 */
template <class TypeTag>
class ChannelTestProblem : public StokesProblemBase<TypeTag>
{
    using ParentType = StokesProblemBase<TypeTag>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr int dimWorld = GridView::dimensionworld;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    ChannelTestProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry, "Stokes"), eps_(1e-6), couplingManager_(couplingManager)
    {
        beta_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Beta");
    }

   /*!
     * \name Problem parameters
     */
    // \{

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        const auto& globalPos = scvf.dofPosition();

        if (this->isOutlet_(globalPos))
            values.setDirichlet(Indices::pressureIdx);
        else if (couplingManager().isCoupledEntity(CouplingManager::bulkIdx, scvf))
        {
            values.setCouplingNeumann(Indices::momentumYBalanceIdx);
            values.setCouplingNeumann(Indices::conti0EqIdx);
            // values.setDirichlet(Indices::velocityXIdx);
            values.setBeaversJoseph(Indices::velocityXIdx);
        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        return values;
    }

    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        if (couplingManager().isCoupledEntity(CouplingManager::bulkIdx, scvf))
        {
            values[Indices::conti0EqIdx] = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
            values[scvf.directionIndex()] = couplingManager().couplingData().momentumCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
        }
        return values;
    }

    /*!
     * \brief Returns the velocity in the porous medium (which is 0 by default according to Saffmann).
     */
    Scalar velocityPorousMedium(const Element& element, const SubControlVolumeFace& scvf) const
    {
        if (couplingManager().isCoupledEntity(CouplingManager::bulkFaceIdx, scvf))
            return couplingManager().couplingData().boundaryVelocity(element, scvf)[0];
        else
         return 0.0;
    }

    /*!
     * \brief Returns the alpha value required as input parameter for the
              Beavers-Joseph-Saffman boundary condition.
     */
    Scalar betaBJ(const Element& element, const SubControlVolumeFace& scvf, const GlobalPosition& tangentialVector) const
    { return beta_; }


    // \}

    template<class FluxOverPlane>
    void setPlanes(FluxOverPlane& flux, const std::vector<Scalar>& auxiliaryPositions)
    {
        const Scalar xMax = this->gridGeometry().bBoxMax()[0];
        const Scalar yMin = this->gridGeometry().bBoxMin()[1];
        const Scalar yMax = this->gridGeometry().bBoxMax()[1];

        //set a plane at the outlet
        const GlobalPosition pBottom{xMax, yMin};
        const GlobalPosition pTop{xMax, yMax};
        flux.addSurface("outlet", pBottom, pTop);

        const Scalar horizontalPlaneY = getParam<Scalar>("FluxOverSurface.HorizontalPlaneY");

        for(auto i : auxiliaryPositions)
            std::cout << i << std::endl;

        for(int i = 0; i < auxiliaryPositions.size(); i+=2)
        {
            const GlobalPosition pLeft{auxiliaryPositions[i] , horizontalPlaneY};
            const GlobalPosition pRight{auxiliaryPositions[i+1] , horizontalPlaneY};
            flux.addSurface("throatsVerticalFlux", pLeft, pRight);
        }
    }

    template<class FluxOverPlane>
    void printFluxes(const FluxOverPlane& flux) const
    {
        std::cout << "mass flux at outlet is: " << flux.netFlux("outlet") << std::endl;

        const auto& values = flux.values("throatsVerticalFlux");

        for(int i = 0; i < values.size(); ++i)
            std::cout << "throat " << i << " : " << values[i] << std::endl;

        std::cout << "\nvertical net flux (should be zero): "  << flux.netFlux("throatsVerticalFlux") << std::endl;
        std::cout << "\n##################################\n" << std::endl;
    }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

   /*!
     * \name Volume terms
     */
    // \{


private:
    Scalar eps_;
    Scalar beta_;
    Scalar vyMaxTop_;
    Scalar topInletLeft_;
    Scalar topInletRight_;

    std::shared_ptr<CouplingManager> couplingManager_;
};
} //end namespace

#endif
