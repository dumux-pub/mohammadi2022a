// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Base problem for the Stokes model
 */
#ifndef DUMUX_STOKES_PROBLEM_BASE_HH
#define DUMUX_STOKES_PROBLEM_BASE_HH

#ifndef USE_DEPRECATED_TEMP
#define USE_DEPRECATED_TEMP 0
#endif

#include <dumux/common/numeqvector.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

namespace Dumux {

/*!
 * \brief  Test problem for the one-phase model:
   \todo doc me!
 */
template <class TypeTag>
class StokesProblemBase : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;

    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using Element = typename GridView::template Codim<0>::Entity;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

public:
    StokesProblemBase(std::shared_ptr<const GridGeometry> gridGeometry, const std::string& paramGroup = "")
    : ParentType(gridGeometry, paramGroup), eps_(1e-10)
    {
        vInflowMax_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.VInflowMax");
        inletLeft_ = getParamFromGroup<GlobalPosition>(this->paramGroup(), "Problem.InletLeft");
        inletRight_ = getParamFromGroup<GlobalPosition>(this->paramGroup(), "Problem.InletRight");
        outflowLength_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.OutflowLength");
    }
    /*!
     * \name Problem parameters
     */
    // \{

#if USE_DEPRECATED_TEMP
    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}
#endif

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set a fixed pressure at the outlet
        if (isOutlet_(globalPos))
            values.setDirichlet(Indices::pressureIdx);
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);

        if (isInlet_(globalPos))
        {
            auto vel = parabolicProfile_(globalPos);
            values[Indices::velocityXIdx] = vel[0];
            values[Indices::velocityYIdx] = vel[1];
        }

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        return PrimaryVariables(0.0);
    }

    // \}

protected:

    bool isOutlet_(const GlobalPosition& globalPos) const
    {
        return (globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_) &&
               (globalPos[1] > (this->gridGeometry().bBoxMax()[1] - outflowLength_ - eps_));
    }

    bool isInlet_(const GlobalPosition& globalPos) const
    {
        if(isTopBoundary_(globalPos))
            return (globalPos[0] > inletLeft_[0] - eps_) && (globalPos[0] < inletRight_[0] + eps_);
        else if(isLeftBoundary_(globalPos))
            return (globalPos[1] > inletLeft_[1] - eps_) && (globalPos[1] < inletRight_[1] + eps_);

        return false;
    }

    bool isTopBoundary_(const GlobalPosition& globalPos) const
    {
        return globalPos[dimWorld-1] > this->gridGeometry().bBoxMax()[dimWorld-1] - eps_;
    }

    bool isLeftBoundary_(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    int bcConditionDirIdx_() const
    {
        if(isTopBoundary_(inletLeft_) && isTopBoundary_(inletRight_))
            return dimWorld-1;
        else if(isLeftBoundary_(inletLeft_) && isLeftBoundary_(inletRight_))
            return 0;
        else
            DUNE_THROW(Dune::InvalidStateException, "Unkown inlet coordinates for BC!");
    }

    GlobalPosition parabolicProfile_(const GlobalPosition x) const
    {
        GlobalPosition velocity(0.0);
        const auto xMin = inletLeft_;
        const auto xMax = inletRight_;
        const Scalar vMax = vInflowMax_;
        velocity[bcConditionDirIdx_()] = vMax * (x - xMin)*(xMax - x) / (0.25*(xMax - xMin)*(xMax - xMin));
        return velocity;
    }

    Scalar eps_;
    Scalar vInflowMax_;
    GlobalPosition inletLeft_;
    GlobalPosition inletRight_;
    Scalar outflowLength_;
};
} //end namespace

#endif
