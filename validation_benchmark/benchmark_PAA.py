#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Thu Feb 6 2020

"""

import sys
import os
import numpy as np
from scipy import stats
import seaborn as sns
from matplotlib.patches import Patch
import matplotlib.patches as patches
import matplotlib
import matplotlib.pylab as plt

# Import the scripts
import ffpm_validation_stokesdarcy as stokesdarcy
import ffpm_validation_stokespnm as stokespnm

matplotlib.use("agg")
## Add BayesValidRox path
#sys.path.append("../src/bayesvalidrox/")


def kde_plot_BME(dof, BME, model_names, plot_name):
    # mkdir for plots
    directory = "logBME_vs_BME/"
    os.makedirs(out_dir + directory, exist_ok=True)

    # Compute log_BME for theoretically optimal model (TOM)
    log_TOM_BME = stats.chi2.rvs(dof, size=BME.shape[1])

    # Plots
    fig, ax = plt.subplots()

    legend_elements = [Patch(facecolor="k", edgecolor="k", label="TOM")]

    Colors = ["blue", "green", "gray"]

    # plot TOM
    sns.kdeplot(log_TOM_BME, ax=ax, color="k", shade=True, lw=3.5, alpha=0.3)

    # Plot BMEs for all models
    for idx in range(BME.shape[0]):
        sns.kdeplot(BME[idx], ax=ax, color=Colors[idx], shade=True, lw=4)
        legend_elements.append(
            Patch(facecolor=Colors[idx], edgecolor=Colors[idx],
                  label=model_names[idx])
        )

    ax.set_xlabel("log$_{10}$(BME)", fontsize=40)
    ax.set_ylabel("Probability density", fontsize=40)

    ax.legend(handles=legend_elements, fontsize=40)

    # Set size of the ticks
    for t in ax.get_xticklabels():
        t.set_fontsize(40)
    for t in ax.get_yticklabels():
        t.set_fontsize(40)

    plt.savefig(
        f"./{out_dir}{directory}BME_TOM_{plot_name}.svg",
        bbox_inches="tight",
    )
    plt.savefig(
        f"./{out_dir}{directory}BME_TOM_{plot_name}.pdf",
        bbox_inches="tight",
    )

    plt.close()

    return log_TOM_BME


def cal_model_weight(BME_dict):
    """
    Normalizes the BME (Asumption: Model Prior weights are equal for models)

    Parameters
    ----------
    BME_dict : dict
        .

    Returns
    -------
    all_ModelWeights : array
        Model weights for all models.
    ModelWeights : array
        Model weights for all models except the non-averaged PNM Model.

    """
    all_BMEs = np.empty((0, n_bootstrap_itr))
    for key in BME_dict.keys():
        all_BMEs = np.vstack((all_BMEs, np.log(BME_dict[key])))

    all_ModelWeights = np.divide(
        np.exp(all_BMEs), np.nansum(np.exp(all_BMEs), axis=0)
        )
    model_weights = np.divide(
        np.exp(all_BMEs[:3]), np.nansum(np.exp(all_BMEs[:3]), axis=0)
        )

    # Compute statistics
    quantile_25_BMEs = np.quantile(all_BMEs[:3], 0.25, axis=1)
    quantile_50_BMEs = np.quantile(all_BMEs[:3], 0.5, axis=1)
    quantile_75_BMEs = np.quantile(all_BMEs[:3], 0.75, axis=1)

    quantile_25_model_weights = np.quantile(model_weights, 0.25, axis=1)
    quantile_50_model_weights = np.quantile(model_weights, 0.5, axis=1)
    quantile_75_model_weights = np.quantile(model_weights, 0.75, axis=1)

    # Print statistics
    print("-" * 10 + "Statistical summary of BME" + "-" * 10)
    print(quantile_25_BMEs - quantile_50_BMEs)
    print(quantile_50_BMEs)
    print(quantile_75_BMEs - quantile_50_BMEs)

    print("\n" + "-" * 10 + "Statistical summary of Model Weights" + "-" * 10)
    print(quantile_25_model_weights - quantile_50_model_weights)
    print(quantile_50_model_weights)
    print(quantile_75_model_weights - quantile_50_model_weights)

    return all_ModelWeights, model_weights


def kde_plot_bayes_factor(BME_dict, plot_name):
    """
    Plots bayes factors.

    Parameters
    ----------
    BME_dict : dict
        A dictionary containing the BME values of all models.
    plot_name : string
        Plot name.

    Returns
    -------
    None
    """

    # mkdir for plots
    directory = "BayesFactor/"
    os.makedirs(out_dir + directory, exist_ok=True)

    Colors = ["blue", "green", "gray", "brown"]

    model_names = list(BME_dict.keys())
    n_models = len(model_names)

    # Plots
    fig, axes = plt.subplots(
        nrows=n_models, ncols=n_models, sharex=True, sharey=True
        )
    f_size = 40

    for i, key_i in enumerate(model_names):

        for j, key_j in enumerate(model_names):
            ax = axes[i, j]
            # Set size of the ticks
            for t in ax.get_xticklabels():
                t.set_fontsize(f_size)
            for t in ax.get_yticklabels():
                t.set_fontsize(f_size)

            if j != i:

                # Null hypothesis: key_j is the better model
                BayesFactor = np.log10(
                    np.divide(BME_dict[key_i], BME_dict[key_j])
                    )

                # taken from seaborn's source code
                # (utils.py and distributions.py)
                def seaborn_kde_support(data, bw, gridsize, cut, clip):
                    if clip is None:
                        clip = (-np.inf, np.inf)
                    support_min = max(data.min() - bw * cut, clip[0])
                    support_max = min(data.max() + bw * cut, clip[1])
                    return np.linspace(support_min, support_max, gridsize)

                kde_estim = stats.gaussian_kde(BayesFactor, bw_method="scott")

                # or better: mimic seaborn's internal stuff
                bw = kde_estim.scotts_factor() * np.std(BayesFactor)
                linearized = seaborn_kde_support(BayesFactor, bw, 100, 3, None)

                # computes values of the estimated function on the estimated
                # linearized inputs
                Z = kde_estim.evaluate(linearized)

                def normalize(x):
                    return (x - x.min(0)) / x.ptp(0)

                # normalize so it is between 0;1
                Z2 = normalize(Z)
                ax.plot(linearized, Z2, "-", color=Colors[i], linewidth=2)
                ax.fill_between(linearized, 0, Z2, color=Colors[i], alpha=0.25)

                # ax.set_ylim([0,0.75])
                # Draw BF significant levels according to Jeffreys 1961
                ax.axvline(
                    x=np.log10(3), ymin=0, linewidth=4, color="dimgrey"
                )  # Strong evidence for both models
                ax.axvline(
                    x=np.log10(10), ymin=0, linewidth=4, color="orange"
                )  # Strong evidence for one model
                ax.axvline(
                    x=np.log10(100), ymin=0, linewidth=4, color="r"
                )  # Decisive evidence for one model

                # legend
                BF_label = key_i + "/" + key_j
                legend_elements = [
                    Patch(
                        facecolor=Colors[i],
                        edgecolor=Colors[i],
                        label="BF(" + BF_label + ")",
                    )
                ]
                ax.legend(
                    loc="upper left",
                    handles=legend_elements,
                    fontsize=f_size - (n_models + 1) * 5,
                )

            elif j == i:
                # build a rectangle in axes coords
                left, width = 0, 1
                bottom, height = 0, 1

                # axes coordinates: 0,0 bottom left and 1,1 upper right
                p = patches.Rectangle(
                    (left, bottom),
                    width,
                    height,
                    color="white",
                    fill=True,
                    transform=ax.transAxes,
                    clip_on=False,
                )
                ax.grid(False)
                ax.add_patch(p)
                # ax.text(0.5*(left+right), 0.5*(bottom+top), key_i,
                fsize = f_size + 20 if n_models < 4 else f_size
                ax.text(
                    0.5,
                    0.5,
                    key_i,
                    horizontalalignment="center",
                    verticalalignment="center",
                    fontsize=fsize,
                    color=Colors[i],
                    transform=ax.transAxes,
                )

    # Defining custom 'ylim' values.
    custom_ylim = (0, 1.05)

    # Setting the values for all axes.
    plt.setp(axes, ylim=custom_ylim)

    # set labels
    for i in range(n_models):
        axes[-1, i].set_xlabel("log$_{10}$(BF)", fontsize=f_size)
        axes[i, 0].set_ylabel("Probability", fontsize=f_size)

    # Adjust subplots
    plt.subplots_adjust(wspace=0.2, hspace=0.1)

    plt.savefig(
        f"./{out_dir}{directory}BayesFactor{plot_name}.svg",
        bbox_inches="tight",
    )
    plt.savefig(
        f"./{out_dir}{directory}BayesFactor{plot_name}.pdf",
        bbox_inches="tight",
    )

    plt.close()


def box_plot_model_weights(model_weights, plot_name):
    """
    Box plot for the posterior model weights.

    Parameters
    ----------
    model_weights : array
        Posterior model weights.
    plot_name : string
        Plot name.

    Returns
    -------
    None.

    """

    # mkdir for plots
    directory = "ModelWeights/"
    os.makedirs(out_dir + directory, exist_ok=True)

    # Create figure
    fig, ax = plt.subplots()
    f_size = 40

    # Filter data using np.isnan
    mask = ~np.isnan(model_weights.T)
    filtered_data = [d[m] for d, m in zip(model_weights, mask.T)]

    # Create the boxplot
    bp = ax.boxplot(filtered_data, patch_artist=True, showfliers=False)

    # change outline color, fill color and linewidth of the boxes
    for box in bp["boxes"]:
        # change outline color
        box.set(color="#7570b3", linewidth=4)
        # change fill color
        box.set(facecolor="#1b9e77")

    # change color and linewidth of the whiskers
    for whisker in bp["whiskers"]:
        whisker.set(color="#7570b3", linewidth=2)

    # change color and linewidth of the caps
    for cap in bp["caps"]:
        cap.set(color="#7570b3", linewidth=2)

    # change color and linewidth of the medians
    for median in bp["medians"]:
        median.set(color="#b2df8a", linewidth=2)

    # change the style of fliers and their fill
    # for flier in bp['fliers']:
    #     flier.set(marker='o', color='#e7298a', alpha=0.75)

    # Custom x-axis labels
    ax.set_xticklabels(list(BME_dict.keys())[: len(model_weights)])

    ax.set_ylabel("Weight", fontsize=f_size)

    # Title
    plt.title("Posterior Model Weights")

    # Set y lim
    ax.set_ylim((-0.05, 1.05))

    # Set size of the ticks
    for t in ax.get_xticklabels():
        t.set_fontsize(f_size)
    for t in ax.get_yticklabels():
        t.set_fontsize(f_size)

    # Save the figure
    fig.savefig(
        f"./{out_dir}{directory}ModelWeights{plot_name}.pdf",
        bbox_inches="tight",
    )

    plt.close()


def perturb_data(data, out_names, n_itr, noise_level):
    """
    Perturbs the data set with the given noise level.

    Parameters
    ----------
    data : dict
        Observation data for all model outputs.
    out_names : list
        List of the output names.
    n_itr : int
        Number of perturbtion.
    noise_level : float
        Multiplier for the standard deviation to compute the additive noise.

    Returns
    -------
    perturbed_data : array of shape (n_itr, n_total_measurement)
        Noisy (perturbed) data set.

    """

    obs_data = data[out_names].values
    n_measurement, n_outputs = obs_data.shape
    n_total_measurement = obs_data[~np.isnan(obs_data)].shape[0]
    perturbed_data = np.zeros((n_itr, n_total_measurement))
    perturbed_data[0] = obs_data.T[~np.isnan(obs_data.T)]

    for itr_idx in range(1, n_itr):
        data = np.zeros((n_measurement, n_outputs))
        for idx in range(len(out_names)):
            std = np.nanstd(obs_data[:, idx])
            if std == 0:
                std = 0.001
            noise = std * noise_level
            data[:, idx] = np.add(
                obs_data[:, idx],
                np.random.normal(0, 1, obs_data.shape[0]) * noise,
            )

        perturbed_data[itr_idx] = data.T[~np.isnan(data.T)]

    return perturbed_data


if __name__ == "__main__":

    # =========================================================================
    # =======================   Set Parameters   ==============================
    # =========================================================================
    # ---------- set scenario params ----------
    inclusion = "squared"  # squared or circular
    inletLoc = "top"  # left or top
    outputs = ["velocity [m/s]", "p"]

    # ---------- set BayesValidRox params ----------
    n_samples = 300  # No. of orig. Model runs for surrogate training
    n_bootstrap_itr = 1000  # No. of bootstraping iterations
    noise_level = 0.00005  # Noise level for bootstraping

    # Perturbe averaged data
    # data = pd.read_csv(
    #     f'data/stokesDataValid_{inclusion}_inclusion_{inletLoc}Inflow.csv')
    # perturbedDataAvg = perturb_data(
    #     data, outputs, n_bootstrap_itr, noise_level)
    # np.savetxt(
    #     f'./data/perturbedValidDataAvg_{inclusion}_inclusion_{inletLoc}Inflow.csv',
    #     perturbedDataAvg, delimiter=',')

    # # Perturbe non-averaged data for PNM
    # data = pd.read_csv(
    #     f'data/stokesDataValid_squared_inclusion_{inletLoc}Inflow_without_averaging.csv')
    # perturbedData = perturb_data(
    #     data, outputs, n_bootstrap_itr, noise_level)
    # np.savetxt(
    #     f'./data/perturbedValidData_squared_inclusion_{inletLoc}Inflow.csv',
    #     perturbedData, delimiter=',')

    # Perturbe averaged data
    perturbed_data_avg = np.loadtxt(
        f"./data/perturbedValidDataAvg_{inclusion}_inclusion_{inletLoc}Inflow.csv",
        delimiter=",")
    params_avg = (n_samples, n_bootstrap_itr, perturbed_data_avg)

    # Perturbe non-averaged data for PNM
    perturbed_data = np.loadtxt(
        f"./data/perturbedValidData_squared_inclusion_{inletLoc}Inflow.csv",
        delimiter=",")
    params = (n_samples, n_bootstrap_itr, perturbed_data)

    # =========================================================================
    # ====================   Run main scripts for PA-B   ======================
    # =========================================================================
    # Set dir name for plots
    out_dir = f"Outputs_BayesAnalysis_{inclusion}_inclusion_{inletLoc}Inflow/"
    result_folder = "./"  # './' or './Results_05_10_2021'

    # ------------- Run scripts ---------
    if inclusion == 'squared':
        # Stokes-PN model with the averaged data
        _, _, BayesValid_PNM = stokespnm.run(params_avg, inletLoc=inletLoc)

        # Stokes-PN model without the averaged data
        _, _, BayesValid_PNM_NA = stokespnm.run(params, averaging=False,
                                                inletLoc=inletLoc)
        # StokesDarcy with Classical IC (Beaver-Joseph)
        _, _, BayesValid_BJ = stokesdarcy.run(params_avg, couplingcond='BJ',
                                              inletLoc=inletLoc,
                                              inclusion=inclusion)

    # StokesDarcy with Generalized (ER) IC
    _, _, BayesValid_ER = stokesdarcy.run(params_avg, couplingcond='ER',
                                          inletLoc=inletLoc,
                                          inclusion=inclusion)

    # ------------- Load the objects ---------
    # Stokes-PN model with the averaged data
    BayesValid_PNM_logBME = np.loadtxt(
        result_folder
        + f"/outputs_ffpm-stokespnm_{inclusion}_inclusion_{inletLoc}Inflow/"
        + f"logBME_ffpm-stokespnm_{inclusion}_inclusion_{inletLoc}Inflow-valid.csv"
    )

    # Stokes-PN model without the averaged data
    BayesValid_PNM_NA_logBME = np.loadtxt(
        result_folder
        + f"/outputs_ffpm-stokespnmNA_{inclusion}_inclusion_{inletLoc}Inflow/"
        + f"logBME_ffpm-stokespnmNA_{inclusion}_inclusion_{inletLoc}Inflow-valid.csv"
    )

    # StokesDarcy with Classical IC (Beaver-Joseph)
    BayesValid_BJ_logBME = np.loadtxt(
        result_folder
        + f"/outputs_ffpm-stokesdarcyBJ_{inclusion}_inclusion_{inletLoc}Inflow/"
        + f"logBME_ffpm-stokesdarcyBJ_{inclusion}_inclusion_{inletLoc}Inflow-valid.csv"
    )

    # StokesDarcy with Generalized (ER) IC
    BayesValid_ER_logBME = np.loadtxt(
        result_folder
        + f"/outputs_ffpm-stokesdarcyER_{inclusion}_inclusion_{inletLoc}Inflow/"
        + f"logBME_ffpm-stokesdarcyER_{inclusion}_inclusion_{inletLoc}Inflow-valid.csv"
    )

    # =========================================================================
    # =============   Bayes Factor Computation (only models)   ================
    # =========================================================================
    # mkdir for plots
    os.makedirs(out_dir, exist_ok=True)

    # ---------------- Validation ----------------
    # Change ln scale to normal scale
    BME_dict = dict()
    BME_dict["Classical IC"] = np.exp(BayesValid_BJ_logBME)
    BME_dict["Generalized IC"] = np.exp(BayesValid_ER_logBME)
    BME_dict["Pore Network"] = np.exp(BayesValid_PNM_logBME)

    # =========================================================================
    # ====================   Bayes Factor plot  ===============================
    # =========================================================================
    # kdePlot only models
    kde_plot_bayes_factor(BME_dict, plot_name="Valid")

    # Plot with all models
    BME_dict["Pore Network SA"] = np.exp(BayesValid_PNM_NA_logBME)
    kde_plot_bayes_factor(BME_dict, plot_name="Valid_with_PNM_SA")

    # =========================================================================
    # ====================   Model weight Computation   =======================
    # =========================================================================

    all_model_weights, model_weights = cal_model_weight(BME_dict)
    # Box Plot
    # box_plot_model_weights(all_ModelWeights, plot_name='BME_Valid_all')
    box_plot_model_weights(model_weights, plot_name="BME_Valid_BJ_ER_PNM")
