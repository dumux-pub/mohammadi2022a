#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 16:55:09 2021

@author: farid
"""

from scipy.stats import lognorm
import matplotlib.pylab as plt
import numpy as np
import chaospy

cnt_of_samples = 1000
ln_sigma = 0.4 #0.28
ln_loc = -np.exp(-18.65)
lb , rb = [1e-10, 1e-7]
int_scale = rb -lb
samples = lognorm(ln_sigma, ln_loc, int_scale/2).rvs(size=cnt_of_samples)
print(np.min(samples),np.max(samples))

# Plot
fig, ax = plt.subplots()
ax.hist(x=samples, bins='auto', color='#0504aa', alpha=0.7, rwidth=0.85);
plt.show()
