#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Author: Farid Mohammadi, M.Sc.
E-Mail: farid.mohammadi@iws.uni-stuttgart.de
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS), University
of Stuttgart, www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart

Created on Thu Aug 13 2020
"""
import os
import joblib
import numpy as np
import pandas as pd
import shutil
import matplotlib
from util.post_plot_PAPER import postPredictiveplot
from bayesvalidrox import PyLinkForwardModel
from bayesvalidrox import Input
from bayesvalidrox import MetaModel
from bayesvalidrox import PostProcessing
from bayesvalidrox import BayesInference
from bayesvalidrox import Discrepancy
matplotlib.use("agg")


def check_ranges(samples, BayesDF):
    """
    This function checks if theta lies in the given ranges

    Parameters
    ----------
    theta : numpy array
        Proposed parameter sets.
    BayesDF : Pandas DataFrame
        DESCRIPTION.

    Returns
    -------
    index : list
        List of indices within the range

    """
    ranges = BayesDF.apply(lambda x: pd.Series([x.min(), x.max()])).T.values.tolist()
    nofpa = samples.shape[1]
    index = []
    # traverse in the list1
    for i, theta in enumerate(samples):
        c = True
        for j, bounds in enumerate(ranges[:nofpa]):
            x = theta[j]
            # condition check
            if x < bounds[0] or x > bounds[1]:
                c = False
        if c:
            index.append(i)
    return index


def run(params, couplingcond="BJ", inclusion="squared", inletLoc="top"):

    print("\n" + "=" * 75)
    print("Stochastic calibration and validation of ffpm-stokesdarcy"
          f"{couplingcond}.")
    print("=" * 75 + "\n")

    n_samples, n_bootstrap_itrs, perturbed_data = params

    # =====================================================
    # ============   COMPUTATIONAL MODEL   ================
    # =====================================================
    Model = PyLinkForwardModel()
    Model.n_cpus = 4  # 8
    Model.type = "PyLink"

    Model.name = f"ffpm-stokesdarcy{couplingcond}_{inclusion}_inclusion_"\
        f"{inletLoc}Inflow"

    model_dir = "./models/stokesdarcy/"
    Model.input_file = f"{model_dir}params_{couplingcond}_{inclusion}_"\
        f"inclusion_{inletLoc}Inflow.input"
    Model.input_template = f"{model_dir}params_{couplingcond}_{inclusion}_"\
        f"inclusion_{inletLoc}Inflow.tpl.input"
    Model.shell_command = f"model_exe.py stokesdarcy -o {inclusion} "\
        f"{Model.input_file}"

    Model.Output.parser = "read_ffpm"
    Model.Output.names = ["velocity [m/s]", "p"]
    Model.Output.file_names = ["ffpm_stokesdarcy_velocity_final.csv",
                               "ffpm_stokesdarcy_p_final.csv"]

    # For Bayesian inversion
    Model.meas_file = f"data/stokesDataCalib_{inclusion}_inclusion"\
        f"_{inletLoc}Inflow.csv"

    # Include the validation observation data
    Model.meas_file_valid = f"data/stokesDataValid_{inclusion}_inclusion"\
        f"_{inletLoc}Inflow.csv"

    # =====================================================
    # =========   PROBABILISTIC INPUT MODEL  ==============
    # =====================================================
    Inputs = Input()

    # VyMaxTop
    Inputs.add_marginals()
    Inputs.Marginals[0].name = "$V^{top}$"
    Inputs.Marginals[0].dist_type = 'uniform'
    Inputs.Marginals[0].parameters = [5e-4, 1.5e-3]

    # CavityHeight
    Inputs.add_marginals()
    Inputs.Marginals[1].name = "$\\Gamma$"
    if couplingcond == "BJ":
        params = [4.9 * 1e-3, 5.1 * 1e-3]
    else:
        params = [5.0 * 1e-3, 5.1 * 1e-3]
    Inputs.Marginals[1].dist_type = 'uniform'
    Inputs.Marginals[1].parameters = params

    # Permeability
    Inputs.add_marginals()  # Permeability
    Inputs.Marginals[2].name = r"$\mathsf{K}$"
    Inputs.Marginals[2].dist_type = 'uniform'
    Inputs.Marginals[2].parameters = [1e-10, 1e-8]
    # params = (1.5e-09, 1.0e-9)
    # Inputs.Marginals[2].DistType = 'lognorm'
    # Inputs.Marginals[2].Parameters = params

    if couplingcond == "BJ":
        # AlphaBeaversJoseph
        Inputs.add_marginals()
        Inputs.Marginals[3].name = "$\\alpha_{BJ}$"
        Inputs.Marginals[3].dist_type = 'unif'
        Inputs.Marginals[3].parameters = [0.1, 4.0]

    # =====================================================
    # ==========  DEFINITION OF THE METAMODEL  ============
    # =====================================================
    MetaModelOpts = MetaModel(Inputs)

    # Select if you want to preserve the spatial/temporal depencencies
    # MetaModelOpts.DimRedMethod = "PCA"
    # MetaModelOpts.varPCAThreshold = 99.99

    # Select your metamodel method
    # 1) PCE (Polynomial Chaos Expansion) 2) aPCE (arbitrary PCE)
    # 3) GPE (Gaussian Process Emulator)
    MetaModelOpts.meta_model_type = "aPCE"

    # ------------------------------------------------
    # ------------- PCE Specification ----------------
    # ------------------------------------------------
    # Select the sparse least-square minimization method for
    # the PCE coefficients calculation:
    # 1)OLS: Ordinary Least Square  2)BRR: Bayesian Ridge Regression
    # 3)LARS: Least angle regression  4)ARD: Bayesian ARD Regression
    # 5)FastARD: Fast Bayesian ARD Regression
    # 6)VBL: Variational Bayesian Learning
    # 7)EBL: Emperical Bayesian Learning
    MetaModelOpts.pce_reg_method = "FastARD"

    # Specify the max degree to be compared by the adaptive algorithm:
    # The degree with the lowest Leave-One-Out cross-validation (LOO)
    # error (or the highest score=1-LOO) estimator is chosen.
    # pce_deg accepts degree as a scalar or a range.
    MetaModelOpts.pce_deg = np.arange(12)

    # q-quasi-norm 0<q<1 (default=1)
    # MetaModelOpts.q = 0.85

    # ------------------------------------------------
    # ------ Experimental Design Configuration -------
    # ------------------------------------------------
    # Generate an experimental design
    MetaModelOpts.add_ExpDesign()

    # One-shot (normal) or Sequential Adaptive (sequential) Design
    MetaModelOpts.ExpDesign.method = "normal"
    MetaModelOpts.ExpDesign.n_init_samples = n_samples

    # Sampling methods
    # 1) random 2) latin_hypercube 3) sobol 4) halton 5) hammersley
    # 6) chebyshev(FT) 7) korobov 8) grid(FT) 9) nested_grid(FT) 10)user
    MetaModelOpts.ExpDesign.sampling_method = "latin_hypercube"

    # Provide the experimental design object with a hdf5 file
    # MetaModelOpts.ExpDesign.hdf5_file = f"ExpDesign_{Model.name}.hdf5"

    # For calculation of validation error
    import h5py
    HDF5File = f"ExpDesign_{Model.name}-testset_Calibration.hdf5"
    hdf5_set = h5py.File(f"./data/ValidationSets/{HDF5File}", "r+")
    valid_samples = np.array(hdf5_set["EDX/init_"])
    valid_sets = dict()

    # Extract x values
    valid_sets["x_values"] = dict()
    for varIdx, var in enumerate(Model.Output.names):
        valid_sets["x_values"][var] = np.array(hdf5_set[f"x_values/{var}"])

    for varIdx, var in enumerate(Model.Output.names):
        valid_sets[var] = np.array(hdf5_set[f"EDY/{var}/init_"])
    hdf5_set.close()

    # >>>>>>>>>>>>>>>>>>>>>> Build Surrogate <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # Adaptive sparse arbitrary polynomial chaos expansion
    print("\n" + "-" * 40)
    print("PCE training for calibration.")
    print("-" * 40 + "\n")
    PCEModel = MetaModelOpts.create_metamodel(Model)

    # Remove zip file
    if os.path.isfile(Model.name + ".zip"):
        os.remove(Model.name + ".zip")

    # Save PCE models
    with open(f"PCEModel_{Model.name}.pkl", "wb") as output:
        joblib.dump(PCEModel, output, 2)

    # Load the objects
    # with open(f"PCEModel_{Model.name}.pkl", "rb") as input:
    #     PCEModel = joblib.load(input)

    # =====================================================
    # =========  POST PROCESSING OF METAMODELS  ===========
    # =====================================================
    PostPCE = PostProcessing(PCEModel)

    # Compute the moments and compare with the Monte-Carlo reference
    PostPCE.plot_moments(xlabel="Point ID", plot_type="bar")

    # Plot the sobol indices
    PostPCE.sobol_indices(xlabel="Point ID", plot_type="bar")

    # Compute and print RMSE error
    PostPCE.check_accuracy(samples=valid_samples, outputs=valid_sets)

    # =====================================================
    # =========  Bayesian inference (Calibration)  ========
    # =====================================================
    BayesOptsCalib = BayesInference(PCEModel)

    BayesOptsCalib.name = "Calib"
    BayesOptsCalib.emulator = True

    # Select the inference method
    BayesOptsCalib.inference_method = "MCMC"
    # Set the MCMC parameters passed to self.mcmc_params
    BayesOptsCalib.mcmc_params = {
        'n_steps': 1e5,
        'n_walkers': 30,
        'multiprocessing': False,
        'verbose': False
        }

    BayesOptsCalib.max_a_posteriori = "mean"
    BayesOptsCalib.corner_title_fmt = ".2e"
    BayesOptsCalib.plot_post_pred = False
    BayesOptsCalib.plot_map_pred = False

    # ----- Define the discrepancy model -------
    calibNumErr = pd.read_csv("data/NumErrorCalib_"
                              f"{Model.name.split('ffpm-')[1]}.csv")
    DiscrepancyOpts = Discrepancy("")
    DiscrepancyOpts.type = "Gaussian"
    DiscrepancyOpts.parameters = calibNumErr**2

    # -- (Option C) --
    DiscOutputOpts = Input()
    # OutputName = 'velocity'
    DiscOutputOpts.add_marginals()
    DiscOutputOpts.Marginals[0].name = "$\\sigma^2_{vel}$"
    DiscOutputOpts.Marginals[0].dist_type = "unif"
    DiscOutputOpts.Marginals[0].parameters = [0.0, 1e-7]

    # OutputName = 'pressure'
    DiscOutputOpts.add_marginals()
    DiscOutputOpts.Marginals[1].name = "$\\sigma^2_{p}$"
    DiscOutputOpts.Marginals[1].dist_type = "unif"
    DiscOutputOpts.Marginals[1].parameters = [0.0, 1e-2]

    BayesOptsCalib.Discrepancy = {
        "known": DiscrepancyOpts,
        "infer": Discrepancy(DiscOutputOpts),
    }

    # ----- Strat Bayesian inference -------
    BayesCalib = BayesOptsCalib.create_inference()

    # Save class objects
    with open(f"PA_A_Bayes_{Model.name}.pkl", "wb") as output:
        joblib.dump(BayesCalib, output, 2)

    # Load the objects
    # with open(f"PA_A_Bayes_{Model.name}.pkl", "rb") as input:
    #     BayesCalib = joblib.load(input)
    # =====================================================
    # =================  Visualization  ===================
    # =====================================================
    # Plot posterior predictive
    postPredictiveplot(
        PCEModel.ModelObj.name, case="Calib", inletLoc=inletLoc,
        bins=20
        )
    # =====================================================
    # ==================  VALIDATION  =====================
    # =====================================================
    ValidInputs = Input()

    ValidInputs.add_marginals()  # VyMaxTop
    ValidInputs.Marginals[0].name = "$V^{top}$"
    ValidInputs.Marginals[0].input_data = BayesCalib.posterior_df["$V^{top}$"]

    ValidInputs.add_marginals()  # CavityHeight
    ValidInputs.Marginals[1].name = "$\\Gamma$"
    ValidInputs.Marginals[1].input_data = BayesCalib.posterior_df["$\\Gamma$"]

    ValidInputs.add_marginals()  # Permeability
    ValidInputs.Marginals[2].name = "$K$"
    ValidInputs.Marginals[2].input_data = BayesCalib.posterior_df[r"$\mathsf{K}$"]

    if couplingcond == "BJ":
        ValidInputs.add_marginals()  # AlphaBeaversJoseph
        ValidInputs.Marginals[3].name = "$\\alpha_{BJ}$"
        ValidInputs.Marginals[3].input_data = BayesCalib.posterior_df["$\\alpha_{BJ}$"]

    # -----------------------------------------------------------------
    print("\n" + "-" * 40)
    print("PCE training for validation.")
    print("-" * 40 + "\n")
    import copy

    ValidModel = copy.deepcopy(Model)
    ValidModel.name = "{}-valid".format(Model.name)
    ValidMetaModelOpts = copy.deepcopy(MetaModelOpts)
    ValidMetaModelOpts.input_obj = ValidInputs
    ValidMetaModelOpts.ExpDesign.method = "normal"
    ValidMetaModelOpts.ExpDesign.InputObj = ValidInputs
    ValidMetaModelOpts.ExpDesign.Y = None
    ValidMetaModelOpts.ExpDesign.sampling_method = "random"  # 'random' 'user'
    ValidMetaModelOpts.ExpDesign.hdf5_file = None
    # f"ExpDesign_{Model.name}-valid.hdf5"

    import h5py

    hdf5_file = f"ExpDesign_{Model.name}-testset_Validation.hdf5"
    ValidSets = h5py.File(f"./data/ValidationSets/{hdf5_file}", "r+")
    par_names = list(BayesCalib.posterior_df.keys())[: PCEModel.n_params]
    indices = check_ranges(
        np.array(ValidSets["EDX/init_"]), BayesCalib.posterior_df[par_names]
    )
    valid_samples = np.array(ValidSets["EDX/init_"])[indices]
    valid_sets = dict()

    # Extract x values
    valid_sets["x_values"] = dict()
    for varIdx, var in enumerate(Model.Output.names):
        valid_sets["x_values"][var] = np.array(ValidSets["x_values/" + var])

    for varIdx, var in enumerate(Model.Output.names):
        valid_sets[var] = np.array(ValidSets["EDY/" + var + "/init_"])[indices]
    ValidSets.close()

    # >>>>>>>>>>>>>>>>>>>>>> Build Surrogate <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    # Train the meta model
    ValidPCEModel = ValidMetaModelOpts.create_metamodel(ValidModel)

    # Remove zip file
    if os.path.isfile(ValidModel.name + ".zip"):
        os.remove(ValidModel.name + ".zip")

    # Save PCE models
    with open(f"PCEModel_{ValidModel.name}.pkl", "wb") as output:
        joblib.dump(ValidPCEModel, output, 2)

    # Load the objects
    # with open(f"PCEModel_{ValidModel.name}.pkl", "rb") as input:
    #     ValidPCEModel = joblib.load(input)

    # =====================================================
    # =========  POST PROCESSING OF METAMODELS  ===========
    # =====================================================
    ValidPostPCE = PostProcessing(ValidPCEModel, name="valid")

    # Compute the moments and compare with the Monte-Carlo reference
    ValidPostPCE.plot_moments(xlabel="Point ID", plot_type="bar")

    # Plot the sobol indices
    ValidPostPCE.sobol_indices(xlabel="Point ID", plot_type="bar")

    # Compute and print RMSE error
    try:
        ValidPostPCE.check_accuracy(
            Samples=valid_samples, validOutputsDict=valid_sets
        )
    except:
        # Include the prediction errors in the likelihood
        ValidPCEModel.rmse = None

    # =====================================================
    # =========  Bayesian inference (Validation)  =========
    # =====================================================
    BayesOptsValid = BayesInference(ValidPCEModel)

    BayesOptsValid.name = "Valid"
    BayesOptsValid.emulator = True

    # Bootstrap for BME calulations
    BayesOptsValid.bootstrap = True
    BayesOptsValid.n_bootstrap_itrs = n_bootstrap_itrs
    BayesOptsValid.perturbed_data = perturbed_data

    BayesOptsValid.max_a_posteriori = "mean"
    BayesOptsValid.corner_title_fmt = ".2e"
    BayesOptsValid.plot_post_pred = False
    BayesOptsValid.plot_map_pred = False

    # ----- Define the discrepancy model -------
    DiscrepancyOpts = Discrepancy("")
    DiscrepancyOpts.type = "Gaussian"
    validNumErr = pd.read_csv("data/NumErrorValid_"
                              f"{Model.name.split('ffpm-')[1]}.csv")
    DiscrepancyOpts.parameters = {
        "velocity [m/s]": np.mean(
            BayesCalib.posterior_df["$\\sigma^2_{vel}$"].values)
        + validNumErr["velocity [m/s]"].values**2,
        "p": np.mean(BayesCalib.posterior_df["$\\sigma^2_{p}$"].values)
        + validNumErr["p"].values**2
    }
    BayesOptsValid.Discrepancy = DiscrepancyOpts

    # ----- Strat Bayesian inference -------
    BayesValid = BayesOptsValid.create_inference()

    #  Save class objects
    with open(f"PA_A_Bayes_{ValidModel.name}.pkl", "wb") as output:
        joblib.dump(BayesValid, output, 2)

    np.savetxt(f"logBME_{ValidModel.name}.csv", BayesValid.log_BME)
    # =====================================================
    # =================  Visualization  ===================
    # =====================================================
    # Plot posterior predictive
    postPredictiveplot(
        ValidPCEModel.ModelObj.name, case="Valid", inletLoc=inletLoc, bins=20
    )

    # =====================================================
    # ========  Moving folders to a new folder  ===========
    # =====================================================
    # Create a output directory
    outdir = f"outputs_{Model.name}"
    os.makedirs(outdir, exist_ok=True)

    # Move files to a main folder
    for case in ["Calib", "Valid"]:
        metaModel = PCEModel if case == "Calib" else ValidPCEModel
        model_name = metaModel.ModelObj.name
        files = [
            f"ExpDesign_{model_name}.hdf5",
            f"PCEModel_{model_name}.pkl",
            f"PA_A_Bayes_{model_name}.pkl",
        ]

        if case == "Valid":
            files.append(f"logBME_{ValidModel.name}.csv")

        for file in files:
            shutil.move(f"./{file}", f"{outdir}/{file}")

        # Move folders to a main forlder
        folders = [
            f"Outputs_PostProcessing_{case.lower()}",
            f"Outputs_Bayes_{model_name}_{case}",
            f"postPred_{model_name}_{case}",
        ]

        for folder in folders:
            try:
                shutil.move(f"./{folder}", outdir)
            except FileNotFoundError:
                pass

    return PCEModel, BayesCalib, BayesValid
