#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 14:15:21 2020
 This script computes p-order Richardson extrapolation scheme:
     $e_h = f_h - \widetilde{f} = g_p h^p + g_{p+1} h^g_{p+1} + O(h^g_{p+2})$

    $f_h$: exact solution to the discrete solution
   $\widetilde{f}$: exact solution to the original PDE (unknown)
   $p$: order of accuracy
   $g_p$: error term cofficient of p-order
@author: farid
"""
import numpy as np
import os
import pandas as pd
from scipy import stats
from scipy import optimize
import matplotlib
import warnings
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 20})
plt.rc('figure', figsize=(24, 16))
plt.rc('font', family='serif', serif='Arial')
plt.rc('axes', grid=True)
plt.rc('text', usetex=False)
plt.rc('xtick', labelsize=20)
plt.rc('ytick', labelsize=20)
plt.rc('axes', labelsize=20)
matplotlib.use('agg')
warnings.filterwarnings("ignore")


def list_files(path):
    r = []
    subdirs = [x[0] for x in os.walk(path)]
    for subdir in subdirs:
        files = os.walk(subdir).__next__()[2]
        if len(files) > 0:
            for file in files:
                if file.endswith('.csv'):
                    r.append(f"{subdir}/{file}")
    return r


def read_data(list_of_files, cases):
    S_k_calib = {}

    for idx, file in enumerate(list_of_files):
        df = pd.read_csv(file)
        S_k_calib[cases[idx]] = df.to_numpy()

    return S_k_calib


def plotFits(h_k, S_k, params, method, Case, item):

    nPrSensor = S_k.shape[0]
    global p_f, pointIDs, modelName

    OutputDir = (r'Plots_'+modelName+'_'+Case)
    if not os.path.exists(OutputDir):
        os.makedirs(OutputDir)

    for idx, PID in enumerate(pointIDs[Case]):
        fig, ax = plt.subplots()

        x = np.linspace(np.min(h_k), np.max(h_k), 100)
        estimate = errorEstimate(x, params[idx], p_f, method)

        # Fit
        plt.plot(x, estimate, marker='o', markevery=5)
        # Data
        plt.scatter(h_k, S_k[idx], marker='x', s=150, c='k')

        if method == 'mixedorder':
            txt = r'$\epsilon (h) = %.4e\times h + {%.4e} \times h^2 $' % (
                params[idx, 1], params[idx, 2])
        else:
            txt = r'$\epsilon (h) = %.4e\times h^{%.4e}$' % (params[idx, 1],
                                                             params[idx, 2])

        plt.text(0.65, 0.65, txt, ha='center', va='bottom',
                 transform=fig.transFigure, usetex=True)
        plt.title(f'Point ID {item} {PID}', usetex=True)
        plt.xlabel('Mesh size $h$ [m]', usetex=True)
        if case == 'velocity':
            plt.ylabel('Velocity [m/s]', usetex=True)
        else:
            plt.ylabel('Pressure [bar]', usetex=True)

        fig.savefig(f'./{OutputDir}/fit_{idx+1}.svg', bbox_inches='tight')
        plt.close()


def errorEstimate(h, params, p_f, method):
    """
    Power-law fitting is best done by first converting
    to a linear equation and then fitting to a straight line.
      S_k = S_0 - C * h_k ^ a

    define our (line) fitting function
    p[0]=S_0 p[1]=C p[2] = a

    Parameters
    ----------
    h : TYPE
        DESCRIPTION.
    params : TYPE
        DESCRIPTION.
    p_f : TYPE
        DESCRIPTION.
    method : TYPE
        DESCRIPTION.

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    if method == 'mixedorder':
        return params[0] + params[1] * h + params[2] * (h**2)
    else:
        f = params[0] + params[1] * (h**params[2])
        if p_f == 1:
            return f
        else:
            for pIdx in range(1, int(p_f)):
                f += params[pIdx+2] * (h**(params[2]+pIdx))
            return f


def mixed1st2ndorderMethod(h_k, S_k_All, Case, h, verbose=True):

    global p_f, reqMethod
    nPrSensor = S_k_All.shape[0]
    nUnknowns = 3
    unknowns = np.zeros((nPrSensor, nUnknowns))

    ################################################################
    # Fitting the data -- mixed1st2ndorder Method Oberkampf book P. 335
    ################################################################
    if verbose:
        print('='*50)
        print('mixed1st2ndorder method, case:', Case)
        print('='*50)

    g_1 = np.zeros((nPrSensor))
    g_2 = np.zeros((nPrSensor))
    f_bar = np.zeros((nPrSensor))

    # Constant refinement
    possible_indices = [[0, 1, 2]]

    for selected_indices in possible_indices:
        selctedh_k = h_k[selected_indices]
        selctedS_k = S_k_All[:, selected_indices]

        # Compute grid factor
        r = selctedh_k[1] / selctedh_k[0]

        for i in range(nPrSensor):
            df_21 = selctedS_k[i, 1] - selctedS_k[i, 0]
            df_32 = selctedS_k[i, 2] - selctedS_k[i, 1]

            g_1[i] = (df_21 * (r**2) - df_32) / (r * (r-1)**2)
            g_2[i] = (df_32 - r*df_21) / (r*(r+1)*(r-1)**2)
            f_bar[i] = selctedS_k[i, 0] + ((df_32-df_21*(1+r+r**2)) /
                                           ((r+1)*(r-1)**2))

            if verbose:
                print('-'*30)
                print("Sensor idx:{0:d} : f = {1:.3f} + {2:.3e} * h "
                      "+ {3:.3e}* h^2".format(i, f_bar[i], g_1[i], g_2[i]))
                estimate = errorEstimate(h_k, (f_bar[i], g_1[i], g_2[i]), p_f,
                                         reqMethod)
                mse = mean_squared_error(selctedS_k[i], estimate)
                print("RMSE: %.3e" % np.sqrt(mse))

    for i in range(nPrSensor):
        unknowns[i] = np.array([f_bar[i], g_1[i], g_2[i]])

    return unknowns


def porderMethod(h_k, S_k_All, Case, h, verbose=True):

    nPrSensor = S_k_All.shape[0]
    nUnknowns = 3
    unknowns = np.zeros((nPrSensor, nUnknowns))

    ################################################################
    # Fitting the data -- Analytical Method Oberkampf book P. 319
    ################################################################
    if verbose:
        print('='*50)
        print('Analytical method, case:', Case)
        print('='*50)

    p_hat = np.zeros((nPrSensor))
    f_bar = np.zeros((nPrSensor))
    g_p = np.zeros((nPrSensor))

    # Constant refinement
    possible_indices = [[0, 1, 2]]

    for selected_indices in possible_indices:
        selctedh_k = h_k[selected_indices]
        selctedS_k = S_k_All[:, selected_indices]

        # Compute grid factor r>1
        r = selctedh_k[1] / selctedh_k[0]

        for i in range(nPrSensor):
            numerator = (selctedS_k[i, 2]-selctedS_k[i, 1]) / \
                (selctedS_k[i, 1]-selctedS_k[i, 0])
            if numerator < 0:
                print("Swaping the values for fine and normal grids")
                selctedS_k[i, 0], selctedS_k[i, 1] = selctedS_k[i, 1], selctedS_k[i, 0]
                numerator = (selctedS_k[i, 2]-selctedS_k[i, 1]) / \
                    (selctedS_k[i, 1]-selctedS_k[i, 0])

            if np.isnan(numerator):
                f_bar[i] = selctedS_k[i, 1]
                p_hat[i], g_p[i] = 0, 0
            else:
                p_hat[i] = np.log(numerator) / np.log(r)
                f_bar[i] = selctedS_k[i, 0]
                f_bar[i] += (selctedS_k[i, 0]-selctedS_k[i, 1])/(r**p_hat[i]-1)
                g_p[i] = (selctedS_k[i, 0] - f_bar[i])
                g_p[i] /= (selctedh_k[0]**p_hat[i])

            if verbose:
                print('-'*30)
                print("\nPoint idx:{0:d} : f = {1:.3f} + {2:.3e} "
                      "* h**{3:.3e}".format(i+1, f_bar[i], g_p[i], p_hat[i]))

    for i in range(nPrSensor):
        unknowns[i] = np.array([f_bar[i], g_p[i], p_hat[i]])

    return unknowns


def lstsqMethod(h_k, S_k_All, Case, h, method='power', normOrd=None,
                verbose=False):
    global item, p_f
    # Define function for calculating a power law
    nPrSensor = S_k_All.shape[0]
    nUnknowns = 2 + int(p_f)

    unknowns = np.zeros((nPrSensor, nUnknowns))

    ##################################################
    # Fitting the data -- Least Squares Method
    ##################################################
    if verbose:
        print('='*50)
        print('Least Squares Method, item, case:', item, Case)
        print('='*50)

    def fit_func(h_k, params, p_f, method):
        return errorEstimate(h_k, params, p_f, method)

    if normOrd is None:
        def errfunc(params, x, y, p_f, method):
            return y - fit_func(h_k, params, p_f, method)
    else:
        def errfunc(params, h_k, y, p_f, method):
            diff = np.vstack(y - fit_func(h_k, params, p_f, method))
            return np.linalg.norm(diff, ord=normOrd, axis=0)

    for sensor_idx, S_k in enumerate(S_k_All):

        # Unknown boundaries
        upBounds = [max(0, np.max(S_k)-10), -1*(np.mean(S_k)-np.std(S_k)), 0.5]
        lowBounds = [np.max(S_k)+10, np.mean(S_k)+np.std(S_k), p_f]

        # Initial guess (must be in the bound)
        pinit = np.array([np.max(S_k), -1*np.std(S_k), 0.6])

        if p_f > 1:
            for i in range(1, int(p_f)):
                upBounds.append(-0.1*(np.mean(S_k)-np.std(S_k)))
                lowBounds.append(0.1*np.mean(S_k)+np.std(S_k))
                pinit = np.hstack((pinit, 0.1*np.mean(S_k)+0.1*np.std(S_k)))

        Bounds = (upBounds, lowBounds)

        out = optimize.least_squares(errfunc, pinit, bounds=Bounds,
                                     args=(h_k, S_k, p_f, method,))

        # Bounds = [(np.min(S_k)-5, np.max(S_k)+5),
        # (max(0,np.max(S_k)-1*np.std(S_k)), np.max(S_k)+1*np.std(S_k)),
        # (np.max(S_k)-200, np.max(S_k)+200)
        #         (-10 , 10), #(-1*abs(np.min(S_k)-200) , 2000)
        #         (0, 10)] #-20,10
        # out = optimize.dual_annealing(errfunc, bounds=Bounds,
        #                         args=(h_k, S_k,))

        # Extract results
        pfinal = out.x
        unknowns[sensor_idx] = pfinal

        if verbose:
            print('-'*30)
            print("Point idx:{0:d} : f = {1:.3f} + {2:.3e} * h**{3:.3e}"
                  .format(sensor_idx+1, pfinal[0], pfinal[1], pfinal[2]))
            estimate = fit_func(h_k, unknowns[sensor_idx], p_f, method)
            mse = mean_squared_error(S_k, estimate)
            print("RMSE: %.3e" % np.sqrt(mse))

    return unknowns


def RobustVerificationMethod(h_k, S_k_All, Case, h, normOrd=None,
                             verbose=True):
    global p_f, reqMethod
    nPrSensor = S_k_All.shape[0]
    nUnknowns = 3
    unknowns = np.zeros((len(normOrd), nPrSensor, nUnknowns))
    failed_idx = []

    for idx, Ord in enumerate(normOrd):

        try:
            unknowns[idx] = lstsqMethod(h_k, S_k_All, Case, h, normOrd=Ord,
                                        verbose=False)
        except:
            failed_idx.append(idx)

    unknowns = np.delete(unknowns, failed_idx, axis=0)
    # Compute median
    medunknowns = np.median(unknowns, axis=0)
    if verbose:
        print("Median:\n", medunknowns)

    # median_absolute_deviation
    MAD = stats.median_absolute_deviation(unknowns, axis=0)
    if verbose:
        print("MAD:\n", MAD)

    if verbose:
        for idx in range(nPrSensor):
            print('-'*30)
            print("Time idx:{0:d} : f = {1:.3f}+/-{2:.3f} + {3:.3e} * h**"
                  "({4:.3e}+/-{5:.3f})".format(idx, medunknowns[idx, 0],
                                               MAD[idx, 0],
                                               medunknowns[idx, 1],
                                               medunknowns[idx, 2],
                                               MAD[idx, 2]))
            estimate = errorEstimate(h_k, medunknowns[idx], p_f, reqMethod)
            mse = mean_squared_error(S_k_All[idx], estimate)
            print("RMSE: %.3e" % np.sqrt(mse))

    return medunknowns


def NumErrCalculator(params, Case, h, method='power', verbose=True):
    """
    Compute Numerical Error for certain mesh generating factor

    Parameters
    ----------
    params : 2d-array (n_sensors, n_unknowns)
        An array containing the unknowns of the discretization error model.
    Case : string
        Connected or disconnected.
    h : float
        Mesh generating factor.

    method : string; default:power
        Numerical error model type - power or mixed order

    Returns
    -------
    NumErr: 1d-array (n_sensors)
        Numerical error estimations

    """

    n_sensors, n_unknowns = params.shape

    NumErr = np.zeros((n_sensors))

    print('\n'*3)
    print('='*50)
    print('Case:', Case)

    for idx, param in enumerate(params):

        if method == 'power':
            NumErr[idx] = param[1] * (h**param[2])

            if p_f != 1:
                for pIdx in range(1, int(p_f)):
                    NumErr[idx] += param[pIdx+2] * (h**(param[2]+pIdx))

        elif method == 'mixed order':
            f_bar, g_1, g_2 = param
            NumErr[idx] = g_1 * h + g_2 * (h**2)

        print("\nPoint idx:{0:d} NumErr = {1:.3e} ".format(idx+1, NumErr[idx]))
        print('-'*30)

    return NumErr


if __name__ == "__main__":

    AllParams = {}
    Errors = {}
    pointIDs = {}
    AllErrors = {}
    verbose = True  # False, True
    step = 100  # Step to plot

    ######################################################
    #   Define h_k and the solutions
    ######################################################
    Case = ['velocity', 'p']
    labels = ['1312x768', '656x384', '328x192']
    Items = ['Calib']
    # stokesdarcy_ER, stokesdarcy_BJ, stokespnm, stokespnmNA
    modelName = 'stokespnmNA'

    list_of_files = [f'./{modelName}/ffpm_{modelName}_{c}.csv' for c in Case]

    # Actual normal mesh spacing
    if 'darcy' in modelName:
        h = 1.5625e-05
        h_k = np.array([7.8125e-06, 1.5625e-05, 3.125e-05])

    elif 'pnm' in modelName:
        h = 6.103515625e-11
        # h_k = np.array([6.25e-10, 2.5e-09, 1.0e-08]) #1.25e-08
        h_k = np.array([1.52587890625e-11, 6.103515625e-11, 2.44140625e-10])

    # Read data
    S_k_calib = read_data(list_of_files, Case)

    # Extract the Point Ids
    velocity_points = pd.read_csv('../velocity_points.csv')
    pointIDs['velocity'] = velocity_points['vtkOriginalPointIds'].values
    pressure_points = pd.read_csv('../pressure_points.csv')
    pointIDs['p'] = pressure_points['vtkOriginalPointIds'].values

    ######################################################
    #   Define the Richardson extrapolation scheme
    ######################################################
    # Methods
    # 'RobustVerification', 'lstsq', 'porder', 'mixedorder'
    reqMethod = 'porder'

    # Set the order for the numerical scheme
    p_f = 1

    # Required only for RobustVerification method
    normOrd = [None, 1, -1, 2, -2, 'inf', '-inf', 'fro', 'nuc']

    ######################################################
    #   Run the discretization error model fitting
    ######################################################
    for item in Items:
        Params = {}
        for case in Case:
            S_k_All = S_k_calib[case]

            if reqMethod == 'RobustVerification':
                Params[case] = RobustVerificationMethod(h_k, S_k_All, case,
                                                        h, normOrd, verbose)

            elif reqMethod == 'lstsq':
                Params[case] = lstsqMethod(h_k, S_k_All, case, h,
                                           method='power', normOrd=None,
                                           verbose=verbose)

            elif reqMethod == 'porder':
                Params[case] = porderMethod(h_k, S_k_All, case, h, verbose)

            elif reqMethod == 'mixedorder':
                Params[case] = mixed1st2ndorderMethod(h_k, S_k_All, case, h,
                                                      verbose=verbose)

        AllParams[item] = Params

    ######################################################
    # Plot the fitted error model
    ######################################################
    for item in Items:
        for case in Case:
            S_k_All = S_k_calib[case]

            plotFits(h_k, S_k_All, AllParams[item][case],
                     reqMethod, case, item)

    ######################################################
    # Compute Numerical Error for mesh generating factor h
    ######################################################
    for item in Items:
        # Extract point ids
        Errors['Point ID'] = pointIDs['velocity']

        for case in Case:

            method = 'mixed order' if reqMethod == 'mixedorder' else 'power'
            Errors[case] = NumErrCalculator(AllParams[item][case], case, h,
                                            method, verbose=verbose)

            if case == 'p':
                Errors['p'] = np.hstack((Errors['p'], np.nan*np.zeros(15)))

        df = pd.DataFrame(Errors)
        df.columns = ['Point ID', 'velocity [m/s]', 'p']
        df.to_csv(f'./NumError_{modelName}.csv', index=False)
