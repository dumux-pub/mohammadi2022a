#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 16:48:59 2019

@author: farid
"""
import numpy as np
import os, sys
import zipfile
import argparse

# parse arguments
parser = argparse.ArgumentParser(
  prog='\033[1m\033[94m' + 'python3.7' + '\033[0m' + ' ' + sys.argv[0],
  description='Run Stokes-Darcy model and extract outputs.'
)
parser.add_argument('file', help="input file")
parser.add_argument('-a', '--averaging', default=True, help='Uses averaging scripts to average quantities in the porous medium. True = averaging. False = no averaging')
args = vars(parser.parse_args())

# Specification
model = 'stokes'
inputFile = args['file']
averaging = args['averaging']

velocity_points = "../../models/velocity_points.csv"
pressure_points = "../../models/pressure_points.csv"

# Run the model
model_exe = 'stokes_regular_2d'
pvdfile1 = 'regular_2D_stokes_250_micron'
vtufile = 'regular_2D_stokes_250_micron-00000.vtu'


NewCommand = model_exe + ' ' + inputFile

Process1 = os.system('timeout 1000 ./%s > log.out 2>&1' %NewCommand)

if Process1 != 0:
    print('\nMessage 1:')
    print('\tIf value of \'%d\' is a non-zero value, then compilation problems \n' % Process1)

Command_avg = "./averaging.py -of ffpm_{}_velocity_final ffpm_{}_p_final \
    -f {} -p ".format(model,model,vtufile)

Process4 = os.system(Command_avg + velocity_points + ' ' + pressure_points)
if Process4 != 0:
    print('\nMessage 4:')
    print('\tIf value of \'%d\' is a non-zero value, then compilation problems \n' % Process3)

# Zip auxillary files
keys = ['vtu', 'vtp', 'pvd', '.csv']
filePaths = [path for path in os.listdir('.') for key in keys if key in path]

zip_file = zipfile.ZipFile('outFiles.zip', 'w')
with zip_file:
    # writing each file one by one
    for path in filePaths:
        if 'final' not in path:
            zip_file.write(path)

for path in filePaths:
    if 'final' not in path:
        os.remove(path)
