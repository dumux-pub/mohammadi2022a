#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 09:40:20 2021

@author: farid
"""

import os
import numpy as np
import matplotlib.pyplot as plt
SIZE = 50
plt.rc('figure', figsize = (24, 16))
plt.rc('font', family='serif', serif='Arial')
plt.rc('font', size=SIZE)
plt.rc('axes', grid = True)
plt.rc('text', usetex=True)
plt.rc('axes', linewidth=5)
plt.rc('lines', linewidth=3)
plt.rc('axes', grid=True)
plt.rc('grid', linestyle="-")
plt.rc('axes', titlesize=SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SIZE)    # legend fontsize
plt.rc('figure', titlesize=SIZE)  # fontsize of the figure title
from matplotlib.backends.backend_pdf import PdfPages


outDir = 'plots/'
os.makedirs(outDir,exist_ok=True)

def run(x,y_, ICtype, mesh_spacing=6.25e-5):
    # Generate points
    npoints = len(y_)
    p_id = np.arange(1,npoints+1)
    x_ = np.repeat(x,npoints)
    z_ = np.zeros(npoints)
    vtk = ['calibration']*npoints
    region = ['pm']*npoints
    vel_points = np.stack((p_id,x_,y_,z_,vtk,region),axis=1)
    # Save points
    np.savetxt('velocity_points.csv', vel_points, delimiter=',', comments='', fmt='%s',
               header='vtkOriginalPointIds,Points:0,Points:1,Points:2,__vtkIsSelected__	,Region')


    vel_csv = 'velocity_points.csv' #'../velocity_points.csv'
    p_csv = '../pressure_points.csv'
    
    # Run averaging script
    Command = "./averaging.py -f regular_2D_stokes_250_micron-00001.vtu \
        -p {0} {1} -o {2} -of averagedVelocityValues_{3} averagedPressureValues_{4}".format(vel_csv
        ,p_csv, outDir, int(x*1e3), int(x*1e3))
    
    Process = os.system(Command)
    if Process != 0:
        print('\nMessage 1:')
        print('\tIf value of \'%d\' is a non-zero value, then compilation problems \n' % Process)
    
    # Run extractoverline with pvpython script
    pvdfile1 = '../stokesdarcy/regular_2D_stokesdarcy_250_micron_{}_stokes'.format(ICtype)
    pvdfile2 = '../stokesdarcy/regular_2D_stokesdarcy_250_micron_{}_darcy'.format(ICtype)
    Command = "pvpython ../../extractpointdataovertime.py \
        -f "+pvdfile1+".pvd "+pvdfile2+".pvd\
        -p " + vel_csv + " -o " + outDir[:-1]
    Process = os.system(Command)
    if Process != 0:
        print('\nMessage 1:')
        print('\tIf value of \'%d\' is a non-zero value, then compilation problems \n' % Process)
    
    return

ICtype = 'ER' # ER or BJ

X = [0.002, 0.004, 0.006, 0.008]
Y = [0.001375, 0.001875, 0.002375, 0.002875, 0.003375, 0.003875,0.004375, 0.004875]
# Y = np.arange(0.0, 0.005+np.finfo(np.float32).eps, mesh_spacing)

pdf_x = PdfPages(outDir+'vel_x.pdf')
pdf_y = PdfPages(outDir+'vel_y.pdf')
for x in X:

    # Run the averaging and postprocessing
    run(x,Y,ICtype)
    
    # Read the outputs
    rev_vel = np.loadtxt(outDir+'ffpm_stokesdarcy_velocity.csv', delimiter=',')
    avg_vel = np.loadtxt(outDir+'averagedVelocityValues_{}.csv'.format(int(x*1e3)), delimiter=',')
    
    # Plot over line
    # velocity_x
    plt.plot(Y, rev_vel[:,0], marker='o', label=r'$V^{REV}_x$')
    plt.plot(Y, avg_vel[:,0], marker='o', label=r'$\bar{V}_x$')
    for y in Y:
        plt.axvline(x=y, linewidth=1, ls='--', color='k')
    plt.legend(loc='best')
    plt.title('Horizontal Velocity Profile at {} mm'.format(x*1e3))
    plt.xlabel('Y [m]')
    plt.ylabel('Velocity [m/s]')
    plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    fig_x = plt.gcf()
    plt.draw()
    pdf_x.savefig(fig_x, bbox_inches='tight')
    # plt.show()
    plt.clf()

    # velocity_y
    plt.plot(Y, rev_vel[:,1], marker='o', label=r'$V^{REV}_y$')
    plt.plot(Y, avg_vel[:,1], marker='o', label=r'$\bar{V}_y$')
    for y in Y:
        plt.axvline(x=y, linewidth=1, ls='--', color='k')
    plt.legend(loc='best')
    plt.title('Vertical Velocity Profile at {} mm'.format(x*1e3))
    plt.xlabel('Y [m]')
    plt.ylabel('Velocity [m/s]')
    plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    fig_y = plt.gcf()
    plt.draw()
    pdf_y.savefig(fig_y, bbox_inches='tight')
    # plt.show()
    plt.clf()

    
    
pdf_x.close()
pdf_y.close()
