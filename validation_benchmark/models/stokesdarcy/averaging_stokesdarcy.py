#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Idea:
- Get grid data from vtu file
- Get velocity and pressure data from vtu file
- averaging: loop over all grid cells
    - determine to which CV the cell belongs
    - add variable (p or vel) value to the local mean value
    - calculate expected number of grid cells in each cv, cells occupied by
    the inclusions are not included in the grid => account for this when calculating mean

Assumptions:
 grid unfirom => determine mesh width from first cell in grid
 all control volumes (CVs) have the same shape and size => function midpointInCV very simple
 CVs dont overlap => allows early loop exit, see code
 midpoints of CVs are inside grid cells => unused

 author: Lars Kaiser / adapted to Stokes-Darcy models by Martin Schneider
 email: lars.kaiser@mathematik.uni-stuttgart.de
"""
from enum import IntEnum
import argparse
import sys
import numpy as np
import meshio

# parse arguments
parser = argparse.ArgumentParser(
  prog='\033[1m\033[94m' + 'python' + '\033[0m' + ' ' + sys.argv[0],
  description='Averaging the outputs using control volumes.'
)
parser.add_argument('-fd', '--fileDarcy', nargs='+', required=True, help="darcy vtu file to be processed")
parser.add_argument('-fs', '--fileStokes', nargs='+', required=True, help="stokes vtu file to be processed")
parser.add_argument('-o', '--outputDirectory', default='', help="Directory to which the .csv files are written")
parser.add_argument('-of', '--outFile', nargs="+", default=['ffpm_stokesdarcy_velocity_final','ffpm_stokesdarcy_p_final'], help="Basename of the written csv file")
parser.add_argument('-p', '--points', type=str, nargs=2, required=True, help=' List of coordinates of the probed point (in 3D)')
parser.add_argument('-v', '--verbosity', default=False, help='Verbosity of the output. True = print progress. False = print data columns')
args = vars(parser.parse_args())

class CVType(IntEnum):
    PRESSURE = 0
    VELOCITY = 2

class ControlVolume:
    def __init__(self, midpoint, type, size, dim=2):
        self.midpoint = midpoint
        self.type = type
        self.size = size
        self.dim = dim
        if type==CVType.PRESSURE:
            self.sum = 0.0
        else:
            self.sum = np.array([0.,0.,0.])

        self.cellIndexList = []
        self.cellCounter = 0
        self.sumCellVolume = 0
        self.volume = np.prod(size)

    def addCell(self, cellIndex, volume, value):
        self.cellIndexList.append(cellIndex)
        self.cellCounter += 1
        self.sumCellVolume += volume
        self.sum += volume*value

    def mean(self):
        return self.sum/self.sumCellVolume

    def pointIsInside(self, point):
        inside = np.abs(self.midpoint-point)<0.5*self.size
        return all(inside)

    def getCVTypeArray(self):
        return np.array([self.type.value]*self.cellCounter)

    def getCVMeanArray(self,type):
        if type==self.type:
            return np.array([self.mean()]*self.cellCounter)
        else:
            return np.array( [(np.array([-1,-1,-1]) if type==CVType.VELOCITY else [-1])] *self.cellCounter)

    def getCellIndices(self):
        return self.cellIndexList

## User Parameters
vtuFilenameDarcy=args['fileDarcy'][0]
vtuFilenameStokes=args['fileStokes'][0]
verbose = args['verbosity']
#yif = float(args['yinterface'][0])
dim=2
#def isValidCV(controlVolume):# funCellTypesion to filter for valid control volumes
#    return controlVolume.midpoint[1]<yif # ignore points with y>0.005

## Implementation

# Grid
meshDarcy = meshio.read(vtuFilenameDarcy)
pointsDarcy = meshDarcy.points
cellsDarcy = meshDarcy.cells[0][1]
pDarcy = meshDarcy.cell_data["cellP"][0]
velocityDarcy = meshDarcy.cell_data["cellVelocity"][0]
volumeDarcy = meshDarcy.cell_data["volume"][0]

meshStokes = meshio.read(vtuFilenameStokes)
pointsStokes = meshStokes.points
cellsStokes = meshStokes.cells[0][1]
cellsStokes += len(pointsDarcy)
pStokes = meshStokes.cell_data["p"][0]
velocityStokes = meshStokes.cell_data["velocity_liq (m/s)"][0]
volumeStokes = meshStokes.cell_data["volume"][0]

points = np.concatenate([pointsDarcy,pointsStokes])
cells = np.concatenate([cellsDarcy,cellsStokes])
p = np.concatenate([pDarcy,pStokes])
velocity = np.concatenate([velocityDarcy,velocityStokes])
volume = np.concatenate([volumeDarcy,volumeStokes])

# Mesh width
representationCell = cells[0]
distances=np.diff(points[representationCell,:], axis=0)#calc 3 of the 4 distances along the "axis" in the first cell
meshWidth=np.amax(np.abs(distances),axis=0)
if dim==2: meshWidth[2]=1.0

# Print grid and data information
if verbose:
    griddescription = "number of grid points: {}, number of cells: {}, cell size: {}x{}x{}".format(len(points), len(cells), meshWidth[0],meshWidth[1],meshWidth[2])
    print("grid description: ", griddescription)
    datadescription = "pressure data points: {}, velocity data points {}".format(len(p), len(velocity))
    print("data description: ", datadescription)
    print()

# Init ControlVolumes (cvs)
pressurePoints = np.genfromtxt(args['points'][1], delimiter=',',skip_header=1, usecols=[1,2,3],dtype=float)
velocityPoints = np.genfromtxt(args['points'][0], delimiter=',',skip_header=1, usecols=[1,2,3],dtype=float)
pressureCVs = np.genfromtxt(args['points'][1], delimiter=',',skip_header=1, usecols=[4,5,6],dtype=float)
velocityCVs = np.genfromtxt(args['points'][0], delimiter=',',skip_header=1, usecols=[4,5,6],dtype=float)
ControlVolumes = []
for idx in range(0,len(pressurePoints)):
    ControlVolumes.append(ControlVolume(pressurePoints[idx],CVType.PRESSURE,pressureCVs[idx],dim))
for idx in range(0,len(velocityPoints)):
    ControlVolumes.append(ControlVolume(velocityPoints[idx],CVType.VELOCITY,velocityCVs[idx], dim))

#ControlVolumes = [cv for cv in ControlVolumes if isValidCV(cv)]# filter cvs

# Averaging: Go through all cells in the grid and calculate average
def midpoint(cell):
    return np.mean(points[cell],axis=0)
def applyToControlVolume(cellIndex):
    cellMidpoint = midpoint(cells[cellIndex])
    for cv in ControlVolumes:
        if(cv.pointIsInside(cellMidpoint)):
            cv.addCell(cellIndex, volume[cellIndex], p[cellIndex] if cv.type==CVType.PRESSURE else velocity[cellIndex])
            return #Assumption: each grid cell only contained in one control volume (cv)

for cellIndex in range(0,len(cells)):
    applyToControlVolume(cellIndex)

# export to vtu
velocityAverages=np.array([cv.mean() for cv in ControlVolumes if cv.type==CVType.VELOCITY])
pressureAverages=np.array([cv.mean() for cv in ControlVolumes if cv.type==CVType.PRESSURE])

# export
outDir = args['outputDirectory']
np.savetxt(outDir+args['outFile'][0]+".csv", velocityAverages, delimiter=",")
np.savetxt(outDir+args['outFile'][1]+".csv", pressureAverages, delimiter=",")

if verbose:
    allCellsIndexList = []
    allCellTypes = []
    allVelocities = []
    allPressures = []
    for cv in ControlVolumes:
        allCellsIndexList.extend(cv.cellIndexList)
        allCellTypes.extend(cv.getCVTypeArray())
        allVelocities.extend(velocity[cv.getCellIndices()])
        allPressures.extend(p[cv.getCellIndices()])

    mesh = meshio.Mesh(
        points,
        [("quad", cells[allCellsIndexList,:])],
        # Each item in cell data must match the cells array
        cell_data={"type": [allCellTypes], "pressure": [allPressures], "velocity": [allVelocities]}
    )
    mesh.write(
        "ControlVolumes.vtu",
        file_format="vtu",
        binary=False #Change to True for smaller/ faster files/processing
    )
