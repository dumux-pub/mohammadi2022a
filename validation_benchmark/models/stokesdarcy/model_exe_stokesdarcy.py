#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 16:48:59 2019

@author: farid
"""
import numpy as np
import os, sys
import zipfile
import argparse
from update_ER_interface_params import replace_ER_interface_params

# parse arguments
parser = argparse.ArgumentParser(
  prog='\033[1m\033[94m' + 'python3.7' + '\033[0m' + ' ' + sys.argv[0],
  description='Run Stokes-Darcy model and extract outputs.'
)
parser.add_argument('file', help="input file")
parser.add_argument('-a', '--averaging', default='True', help='Uses averaging scripts to average quantities in the porous medium. True = averaging. False = no averaging')
parser.add_argument('-o', '--obstacle_shape', default='squared', help='Obstacle shape.  Options: squares(default) and circles.')
args = vars(parser.parse_args())

# Specification
inputFile = args['file']
averaging = True if args['averaging']=='True' else False
obstacle_shape = args['obstacle_shape']

# Run the model
model_exe = 'stokes_darcybox_regular_2d'
model_dir = 'stokesdarcy'
pvdfile1 = 'regular_2D_stokesdarcy_250_micron_stokes'
pvdfile2 = 'regular_2D_stokesdarcy_250_micron_darcy'
darcyfile = 'regular_2D_stokesdarcy_250_micron_darcy-00000.vtu'
stokesfile = 'regular_2D_stokesdarcy_250_micron_stokes-00000.vtu'
velocity_points = "../velocity_points.csv"
pressure_points = "../pressure_points.csv"

# If stockesdarcyER, update the interface params
if 'ER' in inputFile:
    replace_ER_interface_params(inputFile,obs_shape=obstacle_shape)

NewCommand = model_exe + ' ' + inputFile

Process1 = os.system('timeout 1000 ./%s > log.out 2>&1' %NewCommand)

if Process1 != 0:
    print('\nMessage 1:')
    print('\tIf value of \'%d\' is a non-zero value, then compilation problems \n' % Process1)

# Averaging
if averaging:
    Command_avg = "./averaging_stokesdarcy.py \
        -fd "+darcyfile+" -fs "+stokesfile+ " -p "

    Process4 = os.system(Command_avg + velocity_points + ' ' + pressure_points)
    if Process4 != 0:
        print('\nMessage 4:')
        print('\tIf value of \'%d\' is a non-zero value, then compilation problems \n' % Process4)

# Zip auxillary files
keys = ['vtu', 'vtp', 'pvd', '.csv', '.out']
filePaths = [path for path in os.listdir('.') for key in keys if key in path]

zip_file = zipfile.ZipFile('outFiles.zip', 'w')
with zip_file:
    # writing each file one by one
    for path in filePaths:
        if 'final' not in path:
            zip_file.write(path)

for path in filePaths:
    if 'final' not in path:
        os.remove(path)
