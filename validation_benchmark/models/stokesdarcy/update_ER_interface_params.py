#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 2021

This script updates the interface parameters N and M in the inputfile for the
stokes-darcy model with general coupling condition (ER).

@author: Farid Mohammadi, M.Sc.
Department of Hydromechanics and Modelling of Hydrosystems (LH2)
Institute for Modelling Hydraulic and Environmental Systems (IWS),
University of Stuttgart
www.iws.uni-stuttgart.de/lh2/
Pfaffenwaldring 61
70569 Stuttgart
Room: 1.009
"""

import numpy as np
from scipy.interpolate import interp1d
import pandas as pd

def interpol_1D(path_to_csv, newintLoc):
    data = pd.read_csv(path_to_csv)
    intLoc = data['Interface-location']/1e3 # mm to m
    N = data['N']
    M = data['M']

    f_N = interp1d(intLoc, N)
    f_M = interp1d(intLoc, M)

    return np.hstack((f_N(newintLoc),f_M(newintLoc)))

def update_InputFile(inputFile, newParams):
    """
    Finds this pattern with <IC1> and replace it with the new value from
    the newParams array.
    """
    NofPa = newParams.shape[0]
    text_to_search_list = ['<IC%s>'%(i+1) for i in range(NofPa)]

    # Read in the file
    with open(inputFile, 'r') as file :
      filedata = file.read()

    # Replace the target string
    for text_to_search, param in zip(text_to_search_list, newParams):
        filedata = filedata.replace(text_to_search, '{:0.3e}'.format(param))

    # Write the file out again
    with open(inputFile, 'w') as file:
      file.write(filedata)

    return

def replace_ER_interface_params(inputFile,obs_shape='squared'):
    path_to_csv = './../../data/BLvalues-{}.csv'.format(obs_shape)
    keyword = 'CavityHeight' # [5.025,5.175]

    # Find the interface location
    filedata = open(inputFile).readlines()
    for line in filedata:
        if keyword in line:
            newintLoc = float(line.split('= ')[1].split(' ')[0])

    # Compute the interface params
    newParams = interpol_1D(path_to_csv,newintLoc)

    # Replace the values in the input file
    update_InputFile(inputFile,newParams)
