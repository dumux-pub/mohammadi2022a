#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import sys
import numpy as np
from vtk import vtkXMLPolyDataReader
from vtk.util.numpy_support import vtk_to_numpy
import meshio

# parse arguments
parser = argparse.ArgumentParser(
  prog='\033[1m\033[94m' + 'python' + '\033[0m' + ' ' + sys.argv[0],
  description='Averaging the outputs using control volumes.'
)
parser.add_argument('-fd', '--filePNM', nargs='+', required=True, help="PNM vtu file to be processed")
parser.add_argument('-fs', '--fileStokes', nargs='+', required=True, help="stokes vtu file to be processed")
parser.add_argument('-c', '--cvsize', nargs="+", default=[0.0005,0.0005,0.0005], help="size of the cv along the y-axis")
parser.add_argument('-o', '--outputDirectory', default='', help="Directory to which the .csv files are written")
parser.add_argument('-of', '--outFile', nargs="+", default=['averagedVelocityValues','averagedPressureValues'], help="Basename of the written csv file")
parser.add_argument('-p', '--points', type=str, nargs=2, required=True, help=' List of coordinates of the probed point (in 3D)')
parser.add_argument('-v', '--verbosity', default=False, help='Verbosity of the output. True = print progress. False = print data columns')
args = vars(parser.parse_args())

## User Parameters
vtuFilenamePNM=args['filePNM'][0]
vtuFilenameStokes=args['fileStokes'][0]
verbose = args['verbosity']
dim=2

## Implementation
# Control volumes
cvSize=np.array(args['cvsize'])
if dim!=3: cvSize[2]=1.0

# Read vtp data of PNM
reader = vtkXMLPolyDataReader()
reader.SetFileName(vtuFilenamePNM)
reader.Update()
polydata = reader.GetOutput()

pointsPNM = vtk_to_numpy(polydata.GetPoints().GetData())

# Get 1d cells of PNM
cellsPNM = vtk_to_numpy(polydata.GetLines().GetData())
cellsPNM = np.reshape(cellsPNM, (polydata.GetNumberOfLines(), 3))
cellsPNM = cellsPNM[:,1:3]

cellData = polydata.GetCellData()
pointData = polydata.GetPointData()
pPNM = vtk_to_numpy(pointData.GetArray("p"))
pPNM = np.reshape(pPNM, (len(pointsPNM),1))
velocityPNM = vtk_to_numpy(cellData.GetArray("velocity_liq (m/s)"))
throatLength = vtk_to_numpy(cellData.GetArray("throatLength"))
refThroatLength = throatLength[0] # we assume the same throat Length
cellVolumePNM = refThroatLength * refThroatLength # and a uniform grain distribution

meshStokes= meshio.read(vtuFilenameStokes)
pointsStokes = meshStokes.points
cellsStokes = meshStokes.cells[0][1]
pStokes = meshStokes.cell_data["p"][0]
velocityStokes = meshStokes.cell_data["velocity_liq (m/s)"][0]

# Mesh width
representationCell = cellsStokes[0]
distances = np.diff(pointsStokes[representationCell,:], axis=0)#calc 3 of the 4 distances along the "axis" in the first cell
meshWidthStokes = np.amax(np.abs(distances),axis=0)
cellVolumeStokes = round(np.product(meshWidthStokes[0:dim]),14)

# Calculate averages for velocities and pressures
pressurePoints = np.genfromtxt(args['points'][1], delimiter=',',skip_header=1, usecols=[1,2,3],dtype=float)
velocityPoints = np.genfromtxt(args['points'][0], delimiter=',',skip_header=1, usecols=[1,2,3],dtype=float)
pressureCVs = np.genfromtxt(args['points'][1], delimiter=',',skip_header=1, usecols=[4,5,6],dtype=float)
velocityCVs = np.genfromtxt(args['points'][0], delimiter=',',skip_header=1, usecols=[4,5,6],dtype=float)
#pressurePoints = pressurePoints[np.where(pressurePoints[:,1]< 0.005)]
#velocityPoints = velocityPoints[np.where(velocityPoints[:,1]< 0.005)]

velocityAverages = np.zeros(np.shape(velocityPoints))
pressureAverages = np.zeros((len(pressurePoints),1))
velocityCVVolume = np.zeros((len(velocityAverages),1))
pressureCVVolume = np.zeros((len(pressureAverages),1))

# Averaging: Go through all cells in the grid and calculate average
def midpoint(cellPoints):
    return np.mean(cellPoints,axis=0)

def isYThroat(cellPoints):
    assert len(cellPoints)==2, "PNM only allows 1d lines"
    dirVector = np.abs(cellPoints[1,:] - cellPoints[0,:])
    if dirVector[1] > 1e-15:
        return True
    else:
        return False

def checkVelocitypointsPNM(cellPoints):
    cellMidpoint = midpoint(cellPoints)
    for pvIdx in range(0,len(velocityPoints)):
        if( all(np.abs(cellMidpoint-velocityPoints[pvIdx]) < 0.51*velocityCVs[pvIdx]) ):
            return True, pvIdx  #Assumption: each grid cell only contained in one control volume (cv)
    return False, -1

def checkVelocityPointsStokes(cellPoints):
    cellMidpoint = midpoint(cellPoints)
    for pvIdx in range(0,len(velocityPoints)):
        if( all(np.abs(cellMidpoint-velocityPoints[pvIdx]) < 0.5*velocityCVs[pvIdx]) ):
            return True, pvIdx  #Assumption: each grid cell only contained in one control volume (cv)
    return False, -1

def checkPressurePointsPNM(point):
    for pvIdx in range(0,len(pressurePoints)):
        if( all(np.abs(point-pressurePoints[pvIdx]) < 0.001*pressureCVs[pvIdx]) ):
            return True, pvIdx  #Assumption: each grid cell only contained in one control volume (cv)
    return False, -1

def checkPressurePointsStokes(cellPoints):
    cellMidpoint = midpoint(cellPoints)
    for pvIdx in range(0,len(pressurePoints)):
        if( all(np.abs(cellMidpoint-pressurePoints[pvIdx]) < 0.5*pressureCVs[pvIdx]) ):
            return True, pvIdx  #Assumption: each grid cell only contained in one control volume (cv)
    return False, -1

for cellIdx in range(0,len(cellsPNM)):
    cellPoints = pointsPNM[cellsPNM[cellIdx]]
    valid, idx = checkVelocitypointsPNM(cellPoints)
    if valid:
        velocityCVVolume[idx] = np.prod(velocityCVs[idx])
        if isYThroat(cellPoints):
            factor = 1.5 if (throatLength[cellIdx] < refThroatLength) else 2
            # print("factor ", factor, "pos", midpoint(cellPoints))
            velocityAverages[idx] += factor*cellVolumePNM*velocityPNM[cellIdx]
        else:
            velocityAverages[idx] += 0.5*cellVolumePNM*velocityPNM[cellIdx]

for pIdx in range(0,len(pointsPNM)):
    valid, idx = checkPressurePointsPNM(pointsPNM[pIdx])
    if valid:
        pressureAverages[idx] = pPNM[pIdx]
        pressureCVVolume[idx] = 1.0

for cellIdx in range(0,len(cellsStokes)):
    valid, idx = checkVelocityPointsStokes(pointsStokes[cellsStokes[cellIdx]])
    if valid:
        velocityAverages[idx] += cellVolumeStokes*velocityStokes[cellIdx]
        velocityCVVolume[idx] += cellVolumeStokes
    valid, idx = checkPressurePointsStokes(pointsStokes[cellsStokes[cellIdx]])
    if valid:
        pressureAverages[idx] += cellVolumeStokes*pStokes[cellIdx]
        pressureCVVolume[idx] += cellVolumeStokes


# Divide by REV volume.
for idx in range(0,len(velocityAverages)):
    velocityAverages[idx] /= velocityCVVolume[idx]

for idx in range(0,len(pressureAverages)):
    pressureAverages[idx] /= pressureCVVolume[idx]

# export
outDir = args['outputDirectory']
np.savetxt(outDir+args['outFile'][0]+".csv", velocityAverages, delimiter=",")
np.savetxt(outDir+args['outFile'][1]+".csv", pressureAverages, delimiter=",")
