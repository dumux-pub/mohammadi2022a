import argparse
import csv
import fileinput
import os
import sys
from paraview.numpy_support import vtk_to_numpy
import numpy as np
from multiprocessing import Pool

# parse arguments
parser = argparse.ArgumentParser(
  prog='\033[1m\033[94m' + 'pvpython' + '\033[0m' + ' ' + sys.argv[0],
  description='Extract data from the paraview probeLocation and plotOverTime filters.'
)
parser.add_argument('-f', '--files', nargs='+', required=True, help="pvd files to be processed")
parser.add_argument('-o', '--outputDirectory', default='.', help="Directory to which the .csv files are written")
parser.add_argument('-of', '--outFile', default='', help="Basename of the written csv file")
parser.add_argument('-p', '--point', type=str, nargs=1, required=True, help=' List of coordinates of the probed point (in 3D)')
parser.add_argument('-var', '--variable', default='velocity_liq (m/s)', help="Output to extract")
parser.add_argument('-v', '--verbosity', type=int, default=2, help='Verbosity of the output. 1 = print progress. 2 = print data columns')
args = vars(parser.parse_args())

try:
    from paraview.simple import *
except ImportError:
    print("`paraview.simple` not found. Make sure using pvpython instead of python.")

def reader(point_loc):
    point, loc, Id = point_loc

    curFile = args['files'][0] if len(args['files'])==1 or loc == 'ff' else args['files'][1]

    pnm = True if 'pnm' in curFile else False
    indices_vel = [256, 232, 240, 248, 330, 338, 314, 322, 393, 397, 401, 405]
    indices_p = [112,136]
    poreThroat = True if Id in indices_vel else False
    poreBody = True if Id in indices_p else False

    # load pvd file
    pvdFile = PVDReader(FileName=curFile)

    if pnm and poreThroat:
        pvdFile.UpdatePipeline(time=1)
        data = vtk_to_numpy(servermanager.Fetch(pvdFile).GetCellData().GetArray(args['variable']))
        return np.array([data[Id]])

    elif pnm and poreBody:
        pvdFile.UpdatePipeline(time=1)
        data = vtk_to_numpy(servermanager.Fetch(pvdFile).GetPointData().GetArray(args['variable']))
        return np.array([data[Id]])

    else:
        # create probe location
        probeLocation = ProbeLocation()
        probeLocation.Input = pvdFile
        pointSource = probeLocation.ProbeType
        pointSource.Center.SetData(point)
        probeLocation.UpdatePipeline(time=1)
        return vtk_to_numpy(servermanager.Fetch(probeLocation).GetPointData().GetArray(args['variable']))

# Read coordinates
points_list = [[float(x.split(',')[j+1]) for j in range(3)] for x in open(args['point'][0]).readlines()[1:]]

# Check if the point is to be found in curFile
pointsloc_list = [x.split(',')[8][:2] for x in open(args['point'][0]).readlines()[1:]]

# IDs
pointsID_list = [int(x.split(',')[-1]) for x in open(args['point'][0]).readlines()[1:]]

pool = Pool()
results = pool.map(reader, zip(points_list,pointsloc_list, pointsID_list))
results = np.concatenate(results, axis=0)

# Check if some points have different location based on the interface location
if args['variable'] != 'p':
    zero_idx = np.where(~results.any(axis=1))[0]
    if len(zero_idx)!=0:
        pointsloc_list = ['ff' if i in zero_idx else loc for i,loc in enumerate(pointsloc_list)]
        results = pool.map(reader, zip(points_list,pointsloc_list, pointsID_list))
        results = np.concatenate(results, axis=0)

# save results in a csv file
var = args['variable'] if args['variable'] == 'p' else 'velocity'
name = args['outputDirectory']+'/'+'ffpm_'+args['files'][0].split('_')[2]+'_'+var+'_final.csv'
np.savetxt(name, results, delimiter=",")

