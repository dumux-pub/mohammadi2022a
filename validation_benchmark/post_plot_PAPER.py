#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 08:28:13 2020

@author: farid
"""
import numpy as np
import os, sys, joblib
import seaborn as sns
import h5py
from scipy.stats import norm
import pandas as pd

try:
    import cPickle as pickle
except ModuleNotFoundError:
    import _pickle as pickle
from matplotlib.patches import Patch
import matplotlib.lines as mlines
from matplotlib import ticker
import matplotlib.pylab as plt
# Add BayesValidRox path
sys.path.insert(0,'./../../../../BayesValidRox/')
plt.style.use('seaborn-deep') # talk, paper, poster
#fsize = 50
#params = {'legend.fontsize': fsize,
          #'axes.labelsize': fsize,
          #'axes.titlesize': fsize,
          #'xtick.labelsize' :fsize,
          #'ytick.labelsize': fsize,
          #'grid.color': 'k',
          #'grid.linestyle': ':',
          #'grid.linewidth': 0.5,
##          'mathtext.fontset' : 'stix',
##          'mathtext.rm'      : 'serif',
          #'font.family'      : 'serif',
          #'font.serif'       : "Times New Roman", # or "Times"
          #'figure.figsize'   : (32, 24),
          ## 'savefig.dpi':1500,
          #'text.usetex': True
         #}
#plt.rcParams.update(params)

def upper_rugplot(data, height=.05, ax=None, **kwargs):
    from matplotlib.collections import LineCollection
    ax = ax or plt.gca()
    kwargs.setdefault("linewidth", 1)
    segs = np.stack((np.c_[data, data],
                     np.c_[np.ones_like(data), np.ones_like(data)-height]),
                    axis=-1)
    lc = LineCollection(segs, transform=ax.get_xaxis_transform(), **kwargs)
    ax.add_collection(lc)

def postPredictiveplot(modelName, errorPrec, averaging=True, case='Calib',inletLoc='top', bins='auto'):
    result_folder = './outputs_{}/'.format(modelName.split('-v')[0])
    # result_folder = './'
    directory = result_folder+'Outputs_Bayes_'+modelName+'_'+case
    OutputDir = ('postPred_'+modelName+'_'+case)
    if not os.path.exists(OutputDir): os.makedirs(OutputDir)

    # Load Post pred file
    f = h5py.File(directory+'/'+"postPredictive.hdf5", 'r+')

    # Load PCEModel
    with open(result_folder+'PCEModel_'+modelName+'.pkl', 'rb') as input:
        PCEModel = joblib.load(input)

    if case == 'Calib':
        data = pd.read_csv(PCEModel.ModelObj.MeasurementFile)
    else:
        data = pd.read_csv(PCEModel.ModelObj.MeasurementFileValid)

    # Generate Prior Predictive
    #priorPred, std = PCEModel.eval_metamodel(nsamples=10000)


    for OutputName in ['velocity [m/s]', 'p']:
        # x_coords = np.array(f["x_values/"+OutputName])

        # Find pointIDs
        csv_file = 'pressure_points.csv' if OutputName == 'p' else 'velocity_points.csv'
        if case == 'Calib':
            pointIDs = pd.read_csv('./models/'+csv_file).query("__vtkIsSelected__ \
                                                                   == 'Calibration'")['vtkOriginalPointIds'].to_numpy()
        else:
            pointIDs = pd.read_csv('./models/'+csv_file).query("__vtkIsSelected__ \
                                                                   == 'Validation'")['vtkOriginalPointIds'].to_numpy()
        import matplotlib
        gs00 = matplotlib.gridspec.GridSpec(3, 4)
        fig = plt.figure()
        cnt=0
        for idx, x in enumerate(pointIDs):

            if idx!=0 and idx%4==0: cnt += 1

            if idx > 7:
                ax = fig.add_subplot(gs00[cnt,idx%4+1:idx%4+2])
            else:
                ax = fig.add_subplot(gs00[cnt,idx%4])
            # Prior predictive
            #outputs = priorPred[OutputName][:,idx]
            #sns.histplot(outputs[outputs>0], ax=ax,# bins=50,
                         #color='blue', alpha=0.4,stat="count")

            # Posterior predictive
            postPred = np.array(f["EDY/"+OutputName])[:,idx]
            sns.histplot(postPred[postPred>0], ax=ax, bins=bins,
                         color='orange',stat="count") #normalizes counts so that the sum of the bar heights is 1

            # Reference data from the pore-scale simulation
            ax.axvline(x=data[OutputName][idx], linewidth=8, color='green')
            #sns.histplot(np.random.normal(data[OutputName][idx], errorPrec*data[OutputName][idx], len(postPred[postPred>0])), ax=ax,bins=bins,
                          #color='green', alpha=0.5, stat="count")

            # Print conidence interval of ExpDesign.Y (Trained area)
            modelRuns = PCEModel.ExpDesign.Y[OutputName][:,idx]
            upper_rugplot(modelRuns, ax=ax, alpha=0.75, color='grey')

            # fmt = ticker.StrMethodFormatter("{x}")
            # ax.xaxis.set_major_formatter(fmt)
            # ax.yaxis.set_major_formatter(fmt)

            legend_elements = [
                Patch(facecolor='orange', edgecolor='orange',label='Posterior Pred.'),
                Patch(facecolor='green', edgecolor='green',alpha=0.5, label='Ref. Data'),
                mlines.Line2D([], [], marker='|', color='grey', alpha=0.75,
                              linestyle='None', markersize=15, markeredgewidth=1.5, label='Orig. Responses')]

            font = {'family': 'serif',
                    'weight': 'normal',
                    'size': 28,
                    }

            ax.legend(handles=legend_elements, fontsize=font['size']-12)


            #x_label = 'Pressure [Pa]' if OutputName == 'p' else 'velocity [m/s]'
            #ax.set_xlabel(x_label, fontdict=font)
            ax.set_ylabel('Count', fontdict=font)
            # ax.set_xscale('log')
            ax.tick_params(axis='both', which='major', labelsize=font['size'])
            plt.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
            plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
            ax.yaxis.get_offset_text().set_fontsize(font['size'])
            ax.xaxis.get_offset_text().set_fontsize(font['size'])
            plt.grid(True)

            title = 'Point ID: '+str(x)
            plt.title(title,fontdict=font)

        plt.tight_layout(pad=0.95)
        plotname = OutputName if OutputName == 'p' else 'velocity'
        fig.savefig('./'+OutputDir+'/'+plotname+'.svg', bbox_inches='tight')
        fig.savefig('./'+OutputDir+'/'+plotname+'.pdf', bbox_inches='tight')
        plt.close()

# inletLoc = 'top' # top or left
# inclusion = 'squared' # squared or circular
# Name = 'stokespnmNA' #stokesdarcyER stokesdarcyBJ stokespnm stokespnmNA
# modelName = 'ffpm-{}_{}_inclusion_{}Inflow'.format(Name,inclusion,inletLoc)
# postPredictiveplot(modelName, errorPrec=0.05, averaging=False, case='Calib',inletLoc=inletLoc, bins=20)
# postPredictiveplot(modelName+'-valid', errorPrec=0.05, case='Valid',bins=20)
