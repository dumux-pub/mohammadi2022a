#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 16:48:59 2019

@author: farid
"""
import pandas as pd
import numpy as np

def read_ffpm(ModelName, FileNames):
    
    if 'valid' in ModelName.lower():
        case = 'Validation'
    elif 'testset' in ModelName.lower():
        case = 'ValidationSet'
    else:
        case = 'Calibration'

    sorted_PointIDs = dict()
    modelOutputs = ['velocity [m/s]', 'p']
    
    # Get the sorted index
    Velocity = pd.read_csv("models/velocity_points.csv")
    if case !='ValidationSet':
        Velocity = Velocity[Velocity.__vtkIsSelected__==case]
    Pressure = pd.read_csv("models/pressure_points.csv")
    if case !='ValidationSet':
        Pressure = Pressure[Pressure.__vtkIsSelected__==case]
    
    sorted_idx_vel = Velocity.sort_values(['__vtkIsSelected__','vtkOriginalPointIds'], 
                                          ascending=[True, True]).index.tolist()
    sorted_idx_p = Pressure.sort_values(['__vtkIsSelected__','vtkOriginalPointIds'], 
                                        ascending=[True, True]).index.tolist()
    
    sorted_PointIDs[modelOutputs[0]] = Velocity['vtkOriginalPointIds'][sorted_idx_vel].tolist()
    sorted_PointIDs[modelOutputs[1]] = Pressure['vtkOriginalPointIds'][sorted_idx_p].tolist()    
    
    # Compute the velocity magnitude
    V_x = pd.read_csv(FileNames[0],header=None).to_numpy()[sorted_idx_vel,0]
    V_y = pd.read_csv(FileNames[0],header=None).to_numpy()[sorted_idx_vel,1]
    V = np.sqrt(V_x**2+V_y**2)
    
    # Prepare the pressure
    p = pd.read_csv(FileNames[1],header=None).to_numpy().reshape(1,-1)[0][sorted_idx_p]
    
    return [sorted_PointIDs, V, p]