#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 09:53:11 2020

@author: Farid Mohammadi, M.Sc.
    Email: farid.mohammadi@iws.uni-stuttgart.de
"""
import sys
import os
import numpy as np
import scipy.stats as stats
import matplotlib
import chaospy as cp
import h5py
import pandas as pd

matplotlib.use('agg')

# Add BayesValidRox path
sys.path.insert(0, './../../../BayesValidRox/')

from PyLink.PyLinkForwardModel import PyLinkForwardModel

# =====================================================
# ============   COMPUTATIONAL MODEL   ================
# =====================================================
modelDir = './models/stokesdarcy/'

inclusion = 'squared'  # squared or circular
couplingcond = 'BJ'  # ER or BJ
inletLoc = 'top'  # left or top


Model = PyLinkForwardModel()
Model.nrCPUs = 4
Model.Type = 'PyLink'
Model.Name = f'ffpm-stokesdarcy{couplingcond}_{inclusion}_inclusion_'\
    f'{inletLoc}Inflow-testset'
Model.InputFile = f'{modelDir}params_{couplingcond}_{inclusion}_inclusion_'\
    f'{inletLoc}Inflow.input'
Model.InputTemplate = f'{modelDir}params_{couplingcond}_{inclusion}_'\
    f'inclusion_{inletLoc}Inflow.tpl.input'


Model.Command = f"model_exe.py stokesdarcy -o {inclusion} {Model.InputFile}"
Model.ExecutionPath = os.getcwd()
Model.Output.Parser = 'read_ffpm'
Model.Output.Names = ['velocity [m/s]', 'p']
Model.Output.FileNames = ["ffpm_stokesdarcy_velocity_final.csv",
                          "ffpm_stokesdarcy_p_final.csv"]


# =====================================================
# =========   PROBABILISTIC INPUT MODEL  ==============
# =====================================================
n_samples = 150

if couplingcond == 'BJ':
    params = [4.9*1e-3, 5.1*1e-3]
else:
    params = [5.0*1e-3, 5.1*1e-3]

allParams = [(5e-4, 1.5e-3),  # 'VyMaxTop'
             (params[0], params[1]),  # '$\\Gamma$'
             (1.5e-09, 1.0e-9)  # '$K$'
             ]

if couplingcond == 'BJ':
    allParams.append((0.1, 4.0))  # '$\\alpha_{BJ}$'

parametersets = np.zeros((n_samples, len(allParams)))
for parIdx, params in enumerate(allParams):
    if parIdx != 2:
        x = stats.uniform(loc=params[0],
                          scale=params[1]-params[0]).rvs(size=n_samples)
        parametersets[:, parIdx] = x
    else:
        # Mu = np.log(params[0]**2 / np.sqrt(params[0]**2 + params[1]**2))
        # Sigma = np.sqrt(np.log(1 + params[1]**2 / params[0]**2))

        # parametersets[:, parIdx] = cp.LogNormal(mu=Mu,
        #                                         sigma=Sigma).sample(n_samples)

        x = stats.uniform(loc=1e-10, scale=1e-8-1e-10).rvs(size=n_samples)
        parametersets[:, parIdx] = x

# =====================================================
# =============  RUN THE SIMULATIONS  =================
# =====================================================
# Run the models for the samples
OutputMatrix, _ = Model.Run_Model_Parallel(parametersets)


# Cleanup
# Zip the subdirectories
try:
    dir_name = Model.Name + '_ValidSet'
    key = Model.Name + '_'
    Model.zip_subdirs(dir_name, key)
except:
    pass

# =====================================================
# ==========  SLICE VALIDSET HDF5 File  ===============
# =====================================================
# Prepare two separate hdf5 for calibration and validation
ValidSets = h5py.File("ExpDesign_"+Model.Name+".hdf5", 'r+')
validSamples = np.array(ValidSets["EDX/init_"])
OutputNames = ['velocity [m/s]', 'p']

for case in ['Calibration', 'Validation']:
    hdf5file = "ExpDesign_"+Model.Name+"_"+case+".hdf5"
    file = h5py.File(hdf5file, 'a')

    # Save Samples
    grpX = file.create_group("EDX")
    grpX.create_dataset("init_", data=validSamples)

    # Get the sorted index
    sorted_indices = {}
    Velocity = pd.read_csv("models/velocity_points.csv")
    Pressure = pd.read_csv("models/pressure_points.csv")
    if case == 'Calibration':
        sorted_indices[OutputNames[0]] = list(range(0, 10))
        sorted_indices[OutputNames[1]] = list(range(0, 3))
    else:
        sorted_indices[OutputNames[0]] = list(range(10, 20))
        sorted_indices[OutputNames[1]] = list(range(3, 5))

    # Extract x values
    grp_x_values = file.create_group("x_values/")
    validModelRuns = dict()
    for varIdx, var in enumerate(OutputNames):
        validModelRuns[var] = np.array(ValidSets[f"x_values/{var}"])
        grp_x_values.create_dataset(var, data=validModelRuns[var])

    # Extract Y values
    for varIdx, var in enumerate(OutputNames):
        validModelRuns[var] = OutputMatrix[var][:, sorted_indices[var]]
        grpY = file.create_group(f"EDY/{var}")
        grpY.create_dataset("init_", data=validModelRuns[var])

    # close
    file.close()

# close
ValidSets.close()

# Remove original hdf5
os.remove("ExpDesign_"+Model.Name+".hdf5")
