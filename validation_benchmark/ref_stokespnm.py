#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 09:53:11 2020

@author: farid
"""
import os
import numpy as np
import chaospy as cp
import h5py
import pandas as pd
from bayesvalidrox import PyLinkForwardModel
import matplotlib
matplotlib.use('agg')

# Add BayesValidRox path
# sys.path.insert(0, './../../../BayesValidRox/')
# from PyLink.PyLinkForwardModel import PyLinkForwardModel


# =====================================================
# ============   COMPUTATIONAL MODEL   ================
# =====================================================
modelDir = './models/stokespnm/'

averaging = False  # True or False
inletLoc = 'top'  # left or top
inclusion = 'squared'  # only squared

Model = PyLinkForwardModel()
Model.type = 'PyLink'

if averaging:
    Model.name = f'ffpm-stokespnm_{inclusion}_inclusion_'\
        f'{inletLoc}Inflow-testset'
else:
    Model.name = f'ffpm-stokespnmNA_{inclusion}_inclusion_'\
        f'{inletLoc}Inflow-testset'

Model.input_file = f'{modelDir}params_{inclusion}_inclusion_'\
    f'{inletLoc}Inflow.input'
Model.input_template = f'{modelDir}params_{inclusion}_inclusion_'\
    f'{inletLoc}Inflow.tpl.input'


Model.shell_command = f"model_exe.py stokespnm {Model.input_file} "\
    f"--averaging {averaging}"
Model.exe_path = os.getcwd()
Model.Output.parser = 'read_ffpm'
Model.Output.names = ['velocity [m/s]', 'p']
Model.Output.file_names = ["ffpm_stokespnm_velocity_final.csv",
                           "ffpm_stokespnm_p_final.csv"]

# =====================================================
# =========   PROBABILISTIC INPUT MODEL  ==============
# =====================================================
allParams = [(5e-4, 1.5e-3),  # 'VyMaxTop'
             (1.0e-07, 1.0e-05),  # 'TransmissibilityTotal'
             # (1.0e-07,1.0e-04), # 'TransmissibilityThroat'
             # (1.0e-07,1.0e-04), # 'TransmissibilityHalfPore'
             (1.0e3, 1.0e5)  # 'beta'
             ]

origJoints = []
for params in allParams:
    origJoints.append(cp.Uniform(lower=params[0], upper=params[1]))
origSpaceDist = cp.J(*origJoints)

# =====================================================
# =============  RUN THE SIMULATIONS  =================
# =====================================================
SamplingMethod = 'random'
NrSamples = 5 #150

# Generate samples with chaospy
parametersets = cp.generate_samples(NrSamples, domain=origSpaceDist,
                                    rule=SamplingMethod).T


# Run the models for the samples
OutputMatrix, _ = Model.run_model_parallel(parametersets)


# Cleanup
# Zip the subdirectories
try:
    dir_name = Model.name + '_ValidSet'
    key = Model.name + '_'
    Model.zip_subdirs(dir_name, key)
except:
    pass

# =====================================================
# ==========  SLICE VALIDSET HDF5 File  ===============
# =====================================================
# Prepare two separate hdf5 for calibration and validation

ValidSets = h5py.File("ExpDesign_"+Model.name+".hdf5", 'r+')
validSamples = np.array(ValidSets["EDX/init_"])
OutputNames = ['velocity [m/s]', 'p']

for case in ['Calibration', 'Validation']:
    hdf5file = "ExpDesign_"+Model.name+"_"+case+".hdf5"
    file = h5py.File(hdf5file, 'a')

    # Save Samples
    grpX = file.create_group("EDX")
    grpX.create_dataset("init_", data=validSamples)

    # Get the sorted index
    sorted_indices = {}
    Velocity = pd.read_csv("models/velocity_points.csv")
    Pressure = pd.read_csv("models/pressure_points.csv")
    if case == 'Calibration':
        sorted_indices[OutputNames[0]] = list(range(0, 10))
        sorted_indices[OutputNames[1]] = list(range(0, 3))
    else:
        sorted_indices[OutputNames[0]] = list(range(10, 20))
        sorted_indices[OutputNames[1]] = list(range(3, 5))
    # Extract x values
    grp_x_values = file.create_group("x_values/")
    validModelRuns = dict()
    for varIdx, var in enumerate(OutputNames):
        validModelRuns[var] = np.array(ValidSets[f"x_values/{var}"])[sorted_indices[var]]
        grp_x_values.create_dataset(var, data=validModelRuns[var])

    # Extract Y values
    for varIdx, var in enumerate(OutputNames):
        validModelRuns[var] = OutputMatrix[var][:, sorted_indices[var]]
        grpY = file.create_group(f"EDY/{var}")
        grpY.create_dataset("init_", data=validModelRuns[var])

    # close
    file.close()

# close
ValidSets.close()

# Remove original hdf5
os.remove(f"ExpDesign_{Model.name}.hdf5")
