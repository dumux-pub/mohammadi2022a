#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 18 16:19:43 2021

@author: farid
"""

import sys
import os
import joblib
import numpy as np
import pandas as pd
import seaborn as sns
import h5py
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pylab as plt

matplotlib.use("agg")


inletLoc = "top"  # top or left
inclusion = "squared"  # squared or circular
case = "Calib"  # Calib or Valid

Name = "stokesdarcyER"  # stokesdarcyER stokesdarcyBJ stokespnm stokespnmNA
modelName = f"ffpm-{Name}_{inclusion}_inclusion_{inletLoc}Inflow"

path = f"../outputs_{modelName}/"

# Load the objects
if case.lower() == "calib":
    pkl_name = f"{path}PCEModel_{modelName}"
else:
    pkl_name = f"{path}PCEModel_{modelName}-valid"

with open(pkl_name + ".pkl", "rb") as input:
    PCEModel = joblib.load(input)


SIZE = 30
plt.rc("figure", figsize=(24, 16))
plt.rc("font", family="serif", serif="Arial")
plt.rc("font", size=SIZE)
plt.rc("text", usetex=True)
plt.rc("axes", linewidth=3)
plt.rc("axes", grid=True)
plt.rc("grid", linestyle="-")
plt.rc("axes", titlesize=SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=SIZE)  # fontsize of the tick labels
plt.rc("legend", fontsize=SIZE)  # legend fontsize
plt.rc("figure", titlesize=SIZE)  # fontsize of the figure title

# Add BayesValidRox path
sys.path.insert(0, "./../../../BayesValidRox/")
pdf = PdfPages("EDY.pdf")

# Prescribe Output
output = "velocity [m/s]"  # 'velocity [m/s]' 'p'


# Select the ExpDesign
sim_tpye = "Calibration" if case.lower() == "calib" else "Validation"
HDF5File = f"ExpDesign_{modelName}-testset_{sim_tpye}.hdf5"
ValidSets = h5py.File(f"../data/ValidationSets/{HDF5File}", "r+")
validSamples = np.array(ValidSets["EDX/init_"])
validSamplesY = np.array(ValidSets[f"EDY/{output}/init_"])

EDX = np.vstack((PCEModel.ExpDesign.X, validSamples))
EDY = np.vstack((PCEModel.ExpDesign.Y[output], validSamplesY))
ValidSets.close()

# Run the surrogate model
# EDY, _, EDX = PCEModel.eval_metamodel(nsamples=100000, return_samples=True)
# EDY = EDY[output]

# Data
if case.lower() == "calib":
    data = pd.read_csv("../" + PCEModel.ModelObj.MeasurementFile)
else:
    data = pd.read_csv("../" + PCEModel.ModelObj.MeasurementFileValid)

# Filter based on data range
# freezed_ID = range(1, 11)
freezed_ID = [7, 8, 9, 10]
# freezed_ID = [9,10]
bound = 0.05

l_limit, u_limit = {}, {}
critIdx = np.array([True] * len(EDY))
for i, f_idx in enumerate(freezed_ID):
    l_limit[f_idx] = data[output][f_idx - 1] * (1 - bound)
    u_limit[f_idx] = data[output][f_idx - 1] * (1 + bound)
    critIdx *= (EDY[:, int(f_idx) - 1] >= l_limit[f_idx]) * (
        EDY[:, int(f_idx) - 1] <= u_limit[f_idx]
    )

# Select the filtered sets
critEDX = EDX  # [critIdx]
critEDY = EDY  # [critIdx]


print(
    f"\nNumber of simulations found within the given range: \n"
    f"{critEDX.shape[0]} out of {EDX.shape[0]}"
)
while critEDX.shape[0] == 0:
    raise Exception("No parameter set found!")

# Run the surrogate model
y_hat, y_std = PCEModel.eval_metamodel(samples=critEDX)

# Plot for each Point ID
for idx in range(1, 11):

    # Plot the predictions vs simulations
    fig, ax = plt.subplots()
    plt.title(f"Point ID: {idx}")

    # Simulations
    plt.plot(
        critEDY[:, idx - 1], color="navy", marker="x", linewidth=2, label="Simulation"
    )

    # Plot the predition and its uncertainty
    plt.plot(
        y_hat[output][:, idx - 1],
        color="green",
        ls="--",
        marker="x",
        linewidth=2,
        label="Surrogate Approximation",
    )

    plt.fill_between(
        np.arange(len(critEDY)),
        y_hat[output][:, idx - 1] - 1 * y_std[output][:, idx - 1],
        y_hat[output][:, idx - 1] + 1 * y_std[output][:, idx - 1],
        color="green",
        alpha=0.25,
    )

    if idx in freezed_ID:
        f_ID = np.where(np.array(freezed_ID) == idx)[0][0]
        try:
            plt.fill_between(
                np.arange(len(critEDY)),
                l_limit[idx],
                u_limit[idx],
                color="red",
                alpha=0.25,
            )
        except:
            pass

    # Plot data
    plt.hlines(
        data[output][idx - 1],
        0,
        len(critEDY[:, idx - 1]),
        linewidth=3,
        color="red",
        label="Ref. data",
    )

    plt.ylabel(output)
    plt.xlabel("Run number")
    plt.legend(loc="best")
    # plt.show()

    # save the current figure
    pdf.savefig(fig, bbox_inches="tight")
    # Destroy the current plot
    plt.clf()

pdf.close()


SIZE = 12
plt.rc("font", size=SIZE)
plt.rc("axes", titlesize=SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=SIZE)  # fontsize of the tick labels
plt.rc("legend", fontsize=SIZE)
labels = [PCEModel.Inputs.Marginals[i].Name for i in range(PCEModel.NofPa)]
df1 = pd.DataFrame(EDX, columns=labels)
df2 = pd.DataFrame(critEDX, columns=labels)
df = pd.concat([df1.assign(dataset="EDX"), df2.assign(dataset="ValidSet")])
g = sns.pairplot(df, hue="dataset", diag_kind="hist")
g.savefig("validSamples.svg", bbox_inches="tight")
