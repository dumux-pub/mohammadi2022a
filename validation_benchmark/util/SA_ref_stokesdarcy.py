#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 09:53:11 2020

@author: Farid Mohammadi, M.Sc.
    Email: farid.mohammadi@iws.uni-stuttgart.de
"""
import sys, os
import numpy as np
import scipy.stats as stats
import matplotlib
import h5py
import chaospy
import pandas as pd
matplotlib.use('agg')
import matplotlib.pylab as plt
SIZE = 30
plt.rc('figure', figsize = (24, 16))
plt.rc('font', family='serif', serif='Arial')
plt.rc('font', size=SIZE)
plt.rc('text', usetex=True)
plt.rc('axes', linewidth=3)
plt.rc('axes', grid=True)
plt.rc('grid', linestyle="-")
plt.rc('axes', titlesize=SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SIZE)    # legend fontsize
plt.rc('figure', titlesize=SIZE)  # fontsize of the figure title

# Add BayesValidRox path
sys.path.insert(0,'./../../../BayesValidRox/')

from PyLink.PyLinkForwardModel import PyLinkForwardModel

#=====================================================
#============   SET PARAMETERS   =====================
#=====================================================
inclusion = 'squared' # squared or circular
couplingconds = ['BJ','ER'] # ER or BJ
inletLoc = 'top' # left or top
params = ['Vtop', 'Gamma', 'K'] # 'Vtop' 'Gamma' 'K' 'alpha' (BJ)
NrSamples = 30 #30
P_ID = 7 # PID to explore
# outputs = ['horizontal velocity [m/s]', 'vertical velocity [m/s]']#'velocity [m/s]'
outputs = ['horizontal velocity_4X [m/s]', 'vertical velocity_4X [m/s]']
# outputs = ['horizontal velocity [m/s]', 'vertical velocity [m/s]',
#         'horizontal velocity_2X [m/s]', 'vertical velocity_2X [m/s]',
#         'horizontal velocity_4X [m/s]', 'vertical velocity_4X [m/s]']
case = 'valid' # valid or calib

#=====================================================
#============   COMPUTATIONAL MODEL   ================
#=====================================================
modelDir = './models/stokesdarcy/'
models_dict = {}
for couplingcond in couplingconds:
    Model = PyLinkForwardModel()
    Model.nrCPUs = 4 #8
    Model.Type = 'PyLink'
    Model.Name = 'ffpm-stokesdarcy{}_{}_inclusion_{}Inflow-testset'.format(couplingcond,inclusion,inletLoc)
    Model.InputFile = modelDir+'params_{}_{}_inclusion_{}Inflow.input'.format(couplingcond,inclusion,inletLoc)
    Model.InputTemplate = modelDir+'params_{}_{}_inclusion_{}Inflow.tpl.input'.format(couplingcond,inclusion,inletLoc)
    
    
    Model.Command = "model_exe.py stokesdarcy -o {} {}".format(inclusion, Model.InputFile)
    Model.ExecutionPath = os.getcwd()
    Model.Output.Parser = 'read_ffpm'
    # Model.Output.Names = ['horizontal velocity [m/s]', 'vertical velocity [m/s]', 'p'] #['velocity [m/s]', 'p']
    # Model.Output.Names = ['horizontal velocity [m/s]', 'vertical velocity [m/s]',
                          # 'horizontal velocity_2X [m/s]', 'vertical velocity_2X [m/s]',
                          # 'horizontal velocity_4X [m/s]', 'vertical velocity_4X [m/s]',
                          # 'p']
    Model.Output.Names =['horizontal velocity_4X [m/s]', 'vertical velocity_4X [m/s]',
                           'p']
    Model.Output.FileNames = ["ffpm_stokesdarcy_velocity_final_4X.csv",
                              "ffpm_stokesdarcy_p_final_4X.csv"]
                             #["ffpm_stokesdarcy_velocity_final.csv",
                              #"ffpm_stokesdarcy_velocity_final_2X.csv",
                              #"ffpm_stokesdarcy_velocity_final_4X.csv",
                              #"ffpm_stokesdarcy_p_final.csv"]
    
    models_dict[couplingcond] = Model
#=====================================================
#===============   SET PARAMETERS  ===================
#=====================================================
params_dict = {}

# Permeability
k_params = (1.0e-08, 1.5e-08)
Mu = np.log(k_params[0]**2 / np.sqrt(k_params[0]**2 + k_params[1]**2))
Sigma  = np.sqrt(np.log(1 + k_params[1]**2 / k_params[0]**2))
K = chaospy.LogNormal(mu=Mu,sigma=Sigma).sample(NrSamples)

for couplingcond in couplingconds:
    if couplingcond == 'BJ':
        paramList = [4.9*1e-3, 5.1*1e-3] #[0.005-0.5*l, 0.005+0.25*l]
    else:
        paramList = [5.00*1e-3, 5.1*1e-3] #[5.025*1e-3, 5.1*1e-3]
    
    allParams = [(9e-4, 1.1e-3), # 'VyMaxTop'
                 (paramList[0], paramList[1]), # '$\\Gamma$'
                 (1.5e-09, 1.0e-9)# '$K$'
                 ]
    
    if couplingcond == 'BJ':
        allParams.append((0.1 , 4.0)) # '$\\alpha_{BJ}$'
    
    # Number of requested params
    NofPa = len(params)
    
    # Generate parameter sets for SA
    if couplingcond == 'BJ':
        parametersets = np.repeat([[1e-3,0.005,3.25e-9,1.0]],NrSamples*NofPa,axis=0)
    else:
        # parametersets = np.repeat([[1e-3,0.005025,3.25e-9]],NrSamples*NofPa,axis=0)
        parametersets = np.repeat([[1e-3,0.005,3.25e-9]],NrSamples*NofPa,axis=0)
    
    # Velocity
    Vel = np.linspace(allParams[0][0],allParams[0][1],NrSamples)
    
    # Gamma
    Gamma = np.linspace(paramList[0],paramList[1],NrSamples)
    # if couplingcond == 'BJ':
    #     Gamma = np.linspace(paramList[0],paramList[1],NrSamples)
    # else:
    #     Gammas = np.loadtxt('./data/BLvalues-squared.csv',delimiter=',',skiprows=1)[:,0]*1e-3
    #     Gamma = np.hstack((np.linspace(allParams[1][0],0.005025,10),
    #                       Gammas[range(NrSamples-10)]))
    
    
    # Alpha
    if couplingcond == 'BJ':
        alpha = np.linspace(allParams[3][0],allParams[3][1],NrSamples)
 
    # Take parameters to explore
    for idx,p in enumerate(params):
        if p.lower() == 'vtop':
            p_idx = 0
            newValues = np.sort(Vel)
        elif p.lower() == 'gamma':
            p_idx = 1
            newValues = np.sort(Gamma)
        elif p.lower() == 'k':
            p_idx = 2
            newValues = np.sort(K)
        elif p.lower() == 'alpha':
            p_idx = 3
            newValues = np.sort(alpha)
    
        parametersets[idx*NrSamples:(idx+1)*NrSamples,p_idx] = newValues
    
    # For the case where you already have some runs
    HDF5File = "ExpDesign_ffpm-stokesdarcy{}_squared_inclusion_topInflow-testset.hdf5".format(couplingcond)
    ValidSets = h5py.File("./fig/sensitivity/18_10_2021/"+HDF5File, 'r+')
    parametersets = np.array(ValidSets["EDX/init_"])
    ValidSets.close()
    
    params_dict[couplingcond] = parametersets

#=====================================================
#=============  RUN THE SIMULATIONS  =================
#=====================================================
OutputMatrix_dict = {}
for couplingcond in couplingconds:
    Model = models_dict[couplingcond]
    parametersets = params_dict[couplingcond]
    # Run the models for the samples
    OutputMatrix, _ = Model.Run_Model_Parallel(parametersets)
    
    # Cleanup
    #Zip the subdirectories
    try:
        dir_name = Model.Name + '_ValidSet'
        key = Model.Name + '_'
        Model.zip_subdirs(dir_name, key)
    except:
        pass
    OutputMatrix_dict[couplingcond] = OutputMatrix

#=====================================================
#=============  RUN THE SIMULATIONS  =================
#=====================================================

# Plot the outputs versus data
for output in outputs:
    for pidx, p in enumerate(params):
        fig, ax = plt.subplots()
        plt.title("{}-Point ID: {}".format(case,P_ID))
        if case.lower() == 'valid': index = P_ID + 10
        
        for couplingcond in couplingconds:
            OutputMatrix = OutputMatrix_dict[couplingcond]
            parametersets = params_dict[couplingcond]
            # Simulations
            if p.lower() == 'vtop':
                name = '$V^{top}$'
                p_idx = 0
            elif p.lower() == 'gamma':
                name = '$\\Gamma$'
                p_idx = 1
            elif p.lower() == 'k':
                name = '$K$'
                p_idx = 2
            elif p.lower() == 'alpha':
                name = '$\\alpha_{BJ}$'
                p_idx = 3
            
            x = parametersets[pidx*NrSamples:(pidx+1)*NrSamples,p_idx]
            vels = OutputMatrix[output][pidx*NrSamples:(pidx+1)*NrSamples,index-1]
        
            # Plot simulations
            Color = 'navy' if couplingcond == 'ER' else 'darkgreen'
            plt.plot(x,vels, color=Color,marker='o',ms = 10, linewidth=2,
                     label='Simulations ({})'.format(couplingcond))
        
        # Plot data
        if p_idx == 1:
                x = np.linspace(4.9*1e-3, 5.1*1e-3,NrSamples)
        # data = {'horizontal velocity [m/s]':8.02691477132714E-05,
        #         'vertical velocity [m/s]':-0.000117977719652}#pd.read_csv('data/stokesDataValid_{}_inclusion_{}Inflow.csv'.format(inclusion,inletLoc))
        data = {'horizontal velocity [m/s]':8.02691477132714E-05,
                'vertical velocity [m/s]':-0.000117977719652,
                'horizontal velocity_2X [m/s]':7.64210246421928E-05,
                'vertical velocity_2X [m/s]':-0.00011001164523,
                'horizontal velocity_4X [m/s]':9.04780556373485E-05,
                'vertical velocity_4X [m/s]':-0.000101049565529}
        plt.hlines(data[output],min(x),max(x), linewidth=3, 
                   color='red',label='Ref. data') #[P_ID-1]
        
        plt.ylabel(output.replace('_','$-$'))
        plt.xlabel(name)
        plt.legend(loc="best")
        try:
            volName = output.split('_')[1].split(' ')[0]
        except:
            volName = '1X'
        if 'horiz' in output:
            plotname = 'SA_StokesDarcy_horiz_{}_{}_PID_{}.png'.format(volName,p,P_ID)
        else:
            plotname = 'SA_StokesDarcy_vertic_{}_{}_PID_{}.png'.format(volName,p,P_ID)
        fig.savefig(plotname,bbox_inches='tight')