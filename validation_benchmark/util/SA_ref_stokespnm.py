#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 09:53:11 2020

@author: Farid Mohammadi, M.Sc.
    Email: farid.mohammadi@iws.uni-stuttgart.de
"""
import sys, os
import numpy as np
import scipy.stats as stats
import matplotlib
import h5py
import chaospy
import pandas as pd
matplotlib.use('agg')
import matplotlib.pylab as plt
SIZE = 30
plt.rc('figure', figsize = (24, 16))
plt.rc('font', family='serif', serif='Arial')
plt.rc('font', size=SIZE)
plt.rc('text', usetex=True)
plt.rc('axes', linewidth=3)
plt.rc('axes', grid=True)
plt.rc('grid', linestyle="-")
plt.rc('axes', titlesize=SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SIZE)    # legend fontsize
plt.rc('figure', titlesize=SIZE)  # fontsize of the figure title

# Add BayesValidRox path
sys.path.insert(0,'./../../../BayesValidRox/')

from PyLink.PyLinkForwardModel import PyLinkForwardModel

#=====================================================
#============   SET PARAMETERS   =====================
#=====================================================
inclusion = 'squared' # squared
averaging = True #True or False
inletLoc = 'top' # left or top
params = ['$V^{top}$', '$g_{t,ij}$', '$g_{p,i}$'] # '$V^{top}$' '$g_{t,ij}$' '$g_{p,i}$' '$\\beta_{pore}$'
NrSamples = 30 #30
P_ID = 6 # PID to explore
# outputs = ['horizontal velocity [m/s]', 'vertical velocity [m/s]']#'velocity [m/s]'
outputs = ['velocity [m/s]']

case = 'calib' # valid or calib

#=====================================================
#============   COMPUTATIONAL MODEL   ================
#=====================================================
modelDir = './models/stokespnm/'
models_dict = {}

Model = PyLinkForwardModel()
#Model.nrCPUs = 4
Model.Type = 'PyLink'

if averaging:
    Model.Name = 'ffpm-stokespnm_{}_inclusion_{}Inflow-testset'.format(inclusion,inletLoc)
else:
    Model.Name = 'ffpm-stokespnmNA_{}_inclusion_{}Inflow-testset'.format(inclusion,inletLoc)

Model.InputFile = modelDir+'params_{}_inclusion_{}Inflow.input'.format(inclusion,inletLoc)
Model.InputTemplate = modelDir+'params_{}_inclusion_{}Inflow.tpl.input'.format(inclusion,inletLoc)


Model.Command = "model_exe.py stokespnm {} --averaging {}".format(Model.InputFile,averaging)
Model.ExecutionPath = os.getcwd()
Model.Output.Parser = 'read_ffpm'
Model.Output.Names = ['velocity [m/s]', 'p']
Model.Output.FileNames = ["ffpm_stokespnm_velocity_final.csv",
                          "ffpm_stokespnm_p_final.csv"]

#=====================================================
#===============   SET PARAMETERS  ===================
#=====================================================
params_dict = {}


    
allParams = [(5e-4, 1.5e-3), # 'VyMaxTop'
             (1.0e-07,1.0e-04), # 'TransmissibilityThroat'
             (1.0e-07,1.0e-04), # 'TransmissibilityHalfPore'
             (1.0e3, 1.0e5)  # 'beta'
             ]
    
    
# Number of requested params
NofPa = len(params)
    
# Generate parameter sets for SA
parametersets = np.repeat([[1e-3,5.26480244078e-06,1.7642875191945115e-05,
                          35237.668793254459160]],NrSamples*NofPa,axis=0)
    
# Velocity
Vel = np.linspace(allParams[0][0],allParams[0][1],NrSamples)
    
# TransmissibilityThroat '$g_{t,ij}$'
g_tij = np.linspace(allParams[1][0],allParams[1][1],NrSamples)
    
# TransmissibilityHalfPore '$g_{p,i}$'
g_pi = np.linspace(allParams[2][0],allParams[2][1],NrSamples)

# beta '$\\beta_{pore}$'
beta = np.linspace(allParams[3][0],allParams[3][1],NrSamples)

# Take parameters to explore
for idx,p in enumerate(params):
    if p == '$V^{top}$':
        p_idx = 0
        newValues = Vel
    elif p == '$g_{t,ij}$':
        p_idx = 1
        newValues = g_tij
    elif p == '$g_{p,i}$':
        p_idx = 2
        newValues = g_pi 
    elif p == '$\\beta_{pore}$':
        p_idx = 3
        newValues = beta

    parametersets[idx*NrSamples:(idx+1)*NrSamples,p_idx] = newValues
    
    # # For the case where you already have some runs
    # HDF5File = "ExpDesign_ffpm-stokesdarcy{}_squared_inclusion_topInflow-testset.hdf5".format(couplingcond)
    # ValidSets = h5py.File("./fig/sensitivity/18_10_2021/"+HDF5File, 'r+')
    # parametersets = np.array(ValidSets["EDX/init_"])
    # ValidSets.close()
    
    # params_dict[couplingcond] = parametersets

#=====================================================
#=============  RUN THE SIMULATIONS  =================
#=====================================================
# Run the models for the samples
OutputMatrix, _ = Model.Run_Model_Parallel(parametersets)

# Cleanup
#Zip the subdirectories
try:
    dir_name = Model.Name + '_ValidSet'
    key = Model.Name + '_'
    Model.zip_subdirs(dir_name, key)
except:
    pass

#=====================================================
#=============  RUN THE SIMULATIONS  =================
#=====================================================
# Plot the outputs versus data
for output in outputs:
    for pidx, p in enumerate(params):
        fig, ax = plt.subplots()
        plt.title("{}-Point ID: {}".format(case,P_ID))
        index = P_ID if case.lower() == 'calib' else  P_ID + 10
        
        # Simulations
        if p == '$V^{top}$':
            p_idx = 0
        elif p == '$g_{t,ij}$':
            p_idx = 1
        elif p == '$g_{p,i}$':
            p_idx = 2
        elif p == '$\\beta_{pore}$':
            p_idx = 3

        
        x = parametersets[pidx*NrSamples:(pidx+1)*NrSamples,p_idx]
        vels = OutputMatrix[output][pidx*NrSamples:(pidx+1)*NrSamples,index-1]
    
        # Plot simulations
        Color = 'navy'
        plt.plot(x,vels, color=Color,marker='o',ms = 10, linewidth=2,
                 label='Simulations (Avg={})'.format(averaging))
    
        # Plot data
        data = pd.read_csv('data/stokesDataValid_{}_inclusion_{}Inflow.csv'.format(inclusion,inletLoc))
        plt.hlines(data[output][P_ID-1],min(x),max(x), linewidth=3, 
                   color='red',label='Ref. data') #[P_ID-1]
        
        plt.ylabel(output.replace('_','$-$'))
        plt.xlabel(p)
        plt.legend(loc="best")
        try:
            volName = output.split('_')[1].split(' ')[0]
        except:
            volName = '1X'
        if 'horiz' in output:
            plotname = 'SA_StokesDarcy_horiz_{}_{}_PID_{}.png'.format(volName,p,P_ID)
        else:
            plotname = 'SA_StokesDarcy_vertic_{}_{}_PID_{}.png'.format(volName,p,P_ID)
        fig.savefig(plotname,bbox_inches='tight')