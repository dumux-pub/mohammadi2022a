#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 09:53:11 2020

@author: farid
"""
import sys
import os
import joblib
import numpy as np
import pandas as pd
import h5py
import corner

# Local
sys.path.insert(0, './../../../../BayesValidRox/')

inletLoc = 'top'  # top or left
inclusion = 'squared'  # squared or circular

name = 'stokesdarcyBJ'  # stokesdarcyER stokesdarcyBJ stokespnm stokespnmNA
modelName = f'ffpm-{name}_{inclusion}_inclusion_{inletLoc}Inflow'

# Load the pickle objects
data_dir = '../Results_topInflow/24_01_2022/outputs_{}/'.format(modelName)
# data_dir = '../outputs_{}/'.format(modelName)

# with open(data_dir+'PCEModel_ffpm-{}.pkl'.format(modelName), 'rb') as input:
#     PCEModel = pickle.load(input)

with open(data_dir+'PA_A_Bayes{}.pkl'.format(modelName), 'rb') as input:
    Bayes = joblib.load(input)

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
SMALL_SIZE = 28
MEDIUM_SIZE = 34
BIGGER_SIZE = 30#38
lw = 3
alpha = 0.45
plt.rc('figure', figsize = (24, 16))
plt.rc('font', family='serif', serif='Arial')
# plt.rc('text', usetex=True)
plt.rc('axes', linewidth=lw)
plt.rc('axes', grid=False)
plt.rc('grid', linestyle="-")
plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# =====================================================
# ==========  DEFINITION OF THE METAMODEL  ============
# =====================================================
PCEModel = Bayes.PCEModel
# Update slicing index
# Extract the experimental design
EDX = PCEModel.ExpDesign.X
EDYDict = PCEModel.ExpDesign.Y
JDist = PCEModel.ExpDesign.JDist
X_train = PCEModel.ExpDesign.X
EDY = PCEModel.ExpDesign.Y
Outputs = ['velocity [m/s]', 'p']
# labels = [PCEModel.Inputs.Marginals[i].Name for i in range(PCEModel.NofPa)]
labels = list(Bayes.Posterior_df.keys())
if '$K$' in labels:
    index = labels.index('$K$')
    labels[index] = r"$\mathsf{K}$"
# =====================================================
# =================  Visualization  ===================
# =====================================================
# Make directory
outDir = './posteriorPlot'
if not os.path.exists(outDir):
    os.makedirs(outDir)

# ----- Posterior plot ---------
# import emcee
# path = '../Results_22_06_2021/outputs_ffpm-{}/Outputs_Bayes_ffpm-{}_Calib/emcee_sampler.h5'.format(modelName,modelName)
# sampler = emcee.backends.HDFBackend(path)
# try:
#     tau = sampler.get_autocorr_time(tol=0)
# except emcee.autocorr.AutocorrError:
#     tau = 5

# if all(np.isnan(tau)): tau = 5

# burnin = int(2*np.nanmax(tau))
# thin = int(0.5*np.nanmin(tau)) if int(0.5*np.nanmin(tau))!=0 else 1
# postSamples = sampler.get_chain(discard=burnin, flat=True, thin=thin)

# Remove bad chains
limit = {1: 0.00090, 2:2.0e-5, 3:1.5e-5} # pnm
# limit = {1: 0.00088, 2:5e-3, 3:4e-8} #{1: 0.0007, 2:0, 3:4e-8} ER

# postSamples = postSamples.reshape (-1, postSamples.shape[-1])
postSamples = Bayes.Posterior_df.to_numpy()
goodIndices = (postSamples[:,2]>limit[3]) #(postSamples[:,0]>limit[1]) & (postSamples[:,1]>limit[2])#&
                          # (postSamples[:,2]>limit[3])]
goodpostSamples = postSamples#[goodIndices]

# Save new Posterior_df in Bayes.pkl file
# Bayes.Posterior_df = pd.DataFrame(goodpostSamples, columns=labels)
# bayesDir = 'Outputs_Bayes_{}_Calib/'.format(modelName)
# # Save new pkl
# with open(data_dir+'PA_A_Bayes{}.pkl'.format(modelName), 'wb') as output:
#     joblib.dump(Bayes, output, 2)

# # Remove take good samples in postPredictive
# # Load Post pred file
# f = h5py.File(data_dir+bayesDir+'/'+"postPredictive.hdf5", 'r+')
# for out in Outputs:
#     postPred = f["EDY/"+out]
#     newpostPred = np.array(postPred)[goodIndices]
#     del f["EDY/"+out]
#     dset = f.create_dataset("EDY/"+out, data=newpostPred)
# f.close()


# Plot with cornerplot
figPosterior = corner.corner(goodpostSamples, labels=labels,
                             # range=PCEModel.ExpDesign.BoundTuples,
                             # color='grey',
                             use_math_text=True,
                             quantiles=[0.15, 0.5, 0.85],
                             show_titles=True,
                             labelpad=0.25,
                             title_fmt='.2e',
                             plot_datapoints=False,
                             plot_density=False,
                             fill_contours=True,
                             smooth=0.5,
                             smooth1d=0.5,
                             title_kwargs={"fontsize": SMALL_SIZE})

# Loop over axes and set x limits
# axes = np.array(figPosterior.axes).reshape((PCEModel.NofPa, PCEModel.NofPa))
# for yi in range(PCEModel.NofPa):
#     if yi !=2 or 'pnm' in modelName:
#         ax = axes[yi, yi]
#         ax.set_xlim(PCEModel.BoundTuples[yi])
#         for xi in range(yi):
#             if xi !=2 or 'pnm' in modelName:
#                 ax = axes[yi, xi]
#                 ax.set_xlim(PCEModel.BoundTuples[xi])


figPosterior.set_size_inches((24,18))
figPosterior.savefig(outDir + '/Posterior_dist_{}.pdf'.format(modelName),
                       bbox_inches='tight')
