#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 08:28:13 2020

@author: farid
"""
import numpy as np
import os
import sys
import joblib
import seaborn as sns
import h5py
import pandas as pd
from matplotlib.patches import Patch
import matplotlib.lines as mlines
from matplotlib import gridspec
import matplotlib.pylab as plt

# Add BayesValidRox path
sys.path.insert(0, "./../../../../BayesValidRox/")
plt.style.use("seaborn-deep")  # talk, paper, poster


def upper_rugplot(data, height=0.05, ax=None, **kwargs):
    from matplotlib.collections import LineCollection

    ax = ax or plt.gca()
    kwargs.setdefault("linewidth", 1)
    segs = np.stack(
        (np.c_[data, data], np.c_[np.ones_like(data),
                                  np.ones_like(data) - height]), axis=-1
    )
    lc = LineCollection(segs, transform=ax.get_xaxis_transform(), **kwargs)
    ax.add_collection(lc)


def postPredictiveplot(
    model_name, averaging=True, case="Calib", inletLoc="top", bins="auto"
):
    # result_folder = '../Results_14_06_2021/outputs_{}/'.format(model_name.split('-v')[0])
    result_folder = "."
    directory = f"{result_folder}/Outputs_Bayes_{model_name}_{case}"
    output_dir = f"postPred_{model_name}_{case}"
    os.makedirs(output_dir, exist_ok=True)

    # Load Post pred file
    f = h5py.File(f"{directory}/postPredictive.hdf5", "r+")

    # Load PCEModel
    with open(f"{result_folder}/PCEModel_{model_name}.pkl", "rb") as input:
        PCEModel = joblib.load(input)

    if case == "Calib":
        data = pd.read_csv(PCEModel.ModelObj.meas_file)
    else:
        data = pd.read_csv(PCEModel.ModelObj.meas_file_valid)

    # Generate Prior Predictive
    # priorPred, std = PCEModel.eval_metamodel(nsamples=10000)

    my_cmap = plt.get_cmap("Dark2")

    for out_name in PCEModel.ModelObj.Output.names:

        # ---- Bar chart ----
        width = 0.35 if out_name != "p" else 0.1
        fig = plt.figure()
        x_coords = np.array(f["x_values/" + out_name])

        refdata = data[out_name][~np.isnan(data[out_name])].values
        postPred = np.array(f["EDY/" + out_name])
        meanPred = np.mean(postPred, axis=0)
        stdPred = np.std(postPred, axis=0)
        plt.bar(
            np.array(x_coords) - 0.5 * width,
            meanPred,
            yerr=stdPred,
            width=width,
            color=my_cmap.colors[1],
            capsize=7,
        )
        plt.bar(
            np.array(x_coords) + 0.5 * width, refdata, width,
            color=my_cmap.colors[0]
        )
        plt.xlabel("Point ID")
        if out_name != "p":
            plt.ylabel(out_name)
        else:
            plt.ylabel("pressure [bar]")
        plt.legend(["Prediction", "Ref. Data"])
        plt.xticks(x_coords)

        # save the current figure
        plotname = out_name if out_name == "p" else "velocity"
        fig.savefig(
            f"./{output_dir}/{plotname}_barchart.svg", bbox_inches="tight"
        )
        fig.savefig(
            f"./{output_dir}/{plotname}_barchart.pdf", bbox_inches="tight"
        )
        plt.close()

        # ---- Hist plot ----
        # Find pointIDs
        if out_name == "p":
            csv_file = "pressure_points.csv"
        else:
            csv_file = "velocity_points.csv"
        if case == "Calib":
            pointIDs = (
                pd.read_csv("./models/" + csv_file)
                .query("__vtkIsSelected__== 'Calibration'"
                       )["vtkOriginalPointIds"].values
            )
        else:
            pointIDs = (
                pd.read_csv("./models/" + csv_file)
                .query("__vtkIsSelected__== 'Validation'"
                       )["vtkOriginalPointIds"].values
            )
        gs00 = gridspec.GridSpec(3, 4)
        fig = plt.figure()
        cnt = 0
        for idx, x in enumerate(pointIDs):

            if idx != 0 and idx % 4 == 0:
                cnt += 1

            if idx > 7:
                ax = fig.add_subplot(gs00[cnt, idx % 4 + 1: idx % 4 + 2])
            else:
                ax = fig.add_subplot(gs00[cnt, idx % 4])
            # Prior predictive
            # outputs = priorPred[OutputName][:,idx]
            # sns.histplot(outputs[outputs>0], ax=ax,# bins=50,
            # color='blue', alpha=0.4,stat="count")

            # Posterior predictive
            postPred = np.array(f["EDY/" + out_name])[:, idx]
            sns.histplot(
                postPred[postPred > 0], ax=ax, bins=bins, color="orange",
                stat="count"
            )  # normalizes counts so that the sum of the bar heights is 1

            # Reference data from the pore-scale simulation
            ax.axvline(x=data[out_name][idx], linewidth=5, color="green")

            # Print conidence interval of ExpDesign.Y (Trained area)
            modelRuns = PCEModel.ExpDesign.Y[out_name][:, idx]
            upper_rugplot(modelRuns, ax=ax, alpha=0.75, color="grey")

            # fmt = ticker.StrMethodFormatter("{x}")
            # ax.xaxis.set_major_formatter(fmt)
            # ax.yaxis.set_major_formatter(fmt)

            legend_elements = [
                Patch(
                    facecolor="orange", edgecolor="orange",
                    label="Posterior Pred."),
                Patch(
                    facecolor="green", edgecolor="green", alpha=0.5,
                    label="Ref. Data"
                ),
            ]
            if case == "Calib":
                legend_elements.append(
                    mlines.Line2D(
                        [],
                        [],
                        marker="|",
                        color="grey",
                        alpha=0.75,
                        linestyle="None",
                        markersize=15,
                        markeredgewidth=1.5,
                        label="Orig. Responses",
                    )
                )

            font = {"family": "serif", "weight": "normal", "size": 28}

            ax.legend(handles=legend_elements, fontsize=font["size"] - 12)

            ax.set_ylabel("Count", fontdict=font)
            # ax.set_xscale('log')
            ax.tick_params(axis="both", which="major", labelsize=font["size"])
            plt.ticklabel_format(axis="x", style="sci", scilimits=(0, 0))
            plt.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))
            ax.yaxis.get_offset_text().set_fontsize(font["size"])
            ax.xaxis.get_offset_text().set_fontsize(font["size"])
            plt.grid(True)
            # if case == "Calib":
            #     ax.set_xlim(np.min(modelRuns), np.max(modelRuns))

            title = "Point ID: " + str(x)
            plt.title(title, fontdict=font)

        plt.tight_layout(pad=0.95)
        plotname = out_name if out_name == "p" else "velocity"
        fig.savefig(f"./{output_dir}/{plotname}.svg", bbox_inches="tight")
        fig.savefig(f"./{output_dir}/{plotname}.pdf", bbox_inches="tight")
        plt.close()


# model_name = 'ffpm-stokespnm' #stokespnm stokesdarcyER stokesdarcyBJ
# postPredictiveplot(model_name, errorPrec=0.05,averaging=True, case='Calib', bins=20)
# postPredictiveplot(model_name+'-valid', errorPrec=0.05, case='Valid',bins=20)
