#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 17:28:39 2021

@author: farid
"""
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
SIZE = 60
plt.rc('figure', figsize=(24,16))
plt.rc('font', family='serif', serif='Arial')
plt.rc('font', size=SIZE)
plt.rc('axes', grid=True)
plt.rc('text', usetex=True)
plt.rc('axes', linewidth=3)
plt.rc('axes', grid=True)
plt.rc('grid', linestyle="-")
plt.rc('axes', titlesize=SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SIZE)    # legend fontsize
plt.rc('figure', titlesize=SIZE)  # fontsize of the figure title

result_dir = '../Results_09_01_2022_topInflow/'

inletLoc = "top"  # top or left
inclusion = "squared"  # squared or circular

Name = "stokesdarcyBJ"  # stokesdarcyER stokesdarcyBJ stokespnm stokespnmNA
modelName = "ffpm-{}_{}_inclusion_{}Inflow".format(Name, inclusion, inletLoc)

fig = plt.figure()

xlabel = 'Point ID'
x_values = {'velocity [m_s]': np.arange(1, 11), 'p': np.arange(1, 4)}
outNames = ['velocity [m_s]', 'p']


for outIdx, Output in enumerate(outNames):

    total_sobols = pd.read_csv(f'{result_dir}outputs_{modelName}/'\
                               f'Outputs_PostProcessing_calib/totalsobol_'\
                               f'{Output}.csv',
                               delimiter=',')
    total_sobol = total_sobols.to_numpy()
    parNames = list(total_sobols.keys())
    if '$K$' in parNames:
        index = parNames.index('$K$')
        parNames[index] = r"$\mathsf{K}$"

    ax = fig.add_axes([0, 0, 1, 1])
    dict1 = {xlabel: x_values[Output]}
    dict2 = {param: sobolIndices for param, sobolIndices in zip(parNames,
                                                                total_sobol.T)}

    df = pd.DataFrame({**dict1, **dict2})
    df.plot(x=xlabel, y=parNames, kind="bar", ax=ax, rot=0, colormap='Dark2',
            edgecolor="black", lw=1)
    ax.set_ylabel('Total Sobol indices, $S^T$')

    # save the current figure
    fig.savefig(f'Sobol_indices_{Output}_{modelName}.pdf', bbox_inches='tight')

    # Destroy the current plot
    plt.clf()
