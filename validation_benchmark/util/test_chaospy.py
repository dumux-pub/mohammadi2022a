#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 10:11:40 2020

@author: farid
"""

import chaospy
import numpy as np
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.pyplot as plt

def plotter(PCEModel, validsamples):
    fig = plt.figure()
    
    # Evaluate PCE model
    Y_PC_Val_ = np.zeros((validsamples.shape[1]))
    for i, X in enumerate(validsamples.T):
        x1, x2, x3 = X
        Y_PC_Val_[i]=PCEModel(x1, x2, x3)
    
    Y_PC_Val_ = Y_PC_Val_[:,None]
    Y_Val_ = model_solver(validsamples)[:,None]
    
    regression_model = linear_model.LinearRegression()
    regression_model.fit(Y_PC_Val_, Y_Val_)
    
    # Predict
    x_new = np.linspace(np.min(Y_PC_Val_), np.max(Y_Val_), 100)
    y_predicted = regression_model.predict(x_new[:, np.newaxis])
    
    plt.scatter(Y_PC_Val_, Y_Val_, color='gold', linewidth=2)
    plt.plot(x_new, y_predicted, color = 'k')
    
    # Calculate the adjusted R_squared and RMSE
    # the total number of explanatory variables in the model (not including the constant term)
    #length_list = [len(value) for Key, value in aPCE.BasisDict[key].items()]
    NofPredictors =len(PCEModel.todict())
    TotalSampleSize = validsamples.shape[1] #sample size
    
    R2 = r2_score(Y_PC_Val_, Y_Val_)
    AdjR2 = 1 - (1 - R2) * (TotalSampleSize - 1)/(TotalSampleSize - NofPredictors - 1)
    RMSE = np.sqrt(mean_squared_error(Y_PC_Val_, Y_Val_))
    
    plt.annotate('RMSE = '+ str(round(RMSE, 3)) + '\n' + r'Adjusted $R^2$ = '+ str(round(AdjR2, 3)), xy=(0.05, 0.85), xycoords='axes fraction')

    plt.ylabel("Original Model")
    plt.xlabel("PCE Model")
    
    plt.grid()
    plt.show()
    
def model_solver(param, *args):
    param = param.T
    x1 = param[:,0]
    x2 = param[:,1]
    x3 = param[:,2]
    
    if (len(args) == 0):
        	a = 7
        	b = 0.1
    elif (len(args) == 1):
        	b = 0.1

    term1 = np.sin(x1)
    term2 = a * (np.sin(x2))**2
    term3 = b * x3**4 * np.sin(x1)

    return np.array(term1 + term2 + term3)

def model_solverNew(xx, t=None):
    xx = xx.T
    nParamSets, nParams = xx.shape
    
    if t is None: t = np.arange(0, 10, 1.) / 9
    
    term1 = (xx[:,0]**2 + xx[:,1] - 1)**2
    
    term2 = xx[:,0]**2
    
    term3 = 0.1 * xx[:,0] * np.exp(xx[:,1])
    
    term5 = 0
    if nParams > 2:
        for i in range(2, nParams):
            term5 = term5 + xx[:,i]**3/i
    
    const = term1 + term2 + term3 + 1 + term5
    
    # Compute time dependent term
    term4 = np.zeros((nParamSets,len(t)))
    for idx in range(nParamSets):
        term4[idx] = -2 * xx[idx,0] * np.sqrt(0.5*t)
    
    return term4 + np.repeat(const[:,None], len(t), axis=1)


if __name__ == "__main__":
    
    nParams = 4
    polyoder = 1
    q_norm = 0.75
    
    ## Sampling method (See Page 25)
    samplingMethod = "L" #hammersley
    
    ## Regression method p.128
    # 1)LS  4)BRR 
    # 5)ARD 6)LARS 7)OMP
    RegMethod = 'OMP'
    
    
    ## Select a Distributions
    # distribution = chaospy.Iid(chaospy.Uniform(-np.pi, np.pi), nParams)
    distribution = chaospy.J(chaospy.Uniform(1e-4, 1e-2),
                               chaospy.Uniform(1.0e-07, 1.0e-04),
                               chaospy.Uniform(1.0e-07, 1.0e-04),
                               chaospy.Uniform(1000.0, 100000.0))
    
    ## Generate Orthogonal Polynomial Expansion
    # cross_truncation = 1/q_norm
    # full_tensor_product -> cross_truncation=0
    orthogonal_expansion = chaospy.orth_ttr(polyoder, distribution, cross_truncation=1/q_norm)

    
    ## Generate samples
    # samples = distribution.sample(3*len(orthogonal_expansion), rule=samplingMethod)
    import h5py
    hdf5File = "ExpDesign_ffpm-stokespnm.hdf5"
    f = h5py.File(hdf5File, 'r+')
    samples = np.array(f["EDX/init_"]).T
    
    ## Evaluate the model at generated samples
    var = 'velocity [m/s]'
    factor = 1000
    evals = np.array(f["EDY/"+var+"/init_"])[:,0]* factor#model_solver(samples)
    #solves = model_solverNew(samples)
    f.close()
    
    ## fit PCE Model
    # Define the fitting algorithm
    from sklearn import linear_model
    if RegMethod == 'BRR':
        Method = linear_model.BayesianRidge(n_iter=1000, tol=1e-7,
                                              fit_intercept = False)
    elif RegMethod == 'LARS':
        Method = linear_model.LarsCV(fit_intercept = False, max_iter=5)
    
    elif RegMethod == 'elastic net':
        Method = linear_model.ElasticNet(fit_intercept = False, alpha=0.2)
        
    elif RegMethod == 'ARD':
        Method = linear_model.ARDRegression(n_iter=1000, tol=1e-7,
                                              fit_intercept = False)
    elif RegMethod == 'OMP':
        Method = linear_model.OrthogonalMatchingPursuit(fit_intercept=False, 
                                                        n_nonzero_coefs=3)
    else:
        Method = None
        
        
    # fit the model
    PCEModel, coef_ = chaospy.fit_regression(orthogonal_expansion, samples, evals,
                                          model=Method, retall=True)
    # report the coeffs
    # print("\nBases and coeffs:\n", PCEModel.todict())
    # coef_ = np.asarray(list(PCEModel.todict().values()))

    
    ## Extract statistical moments
    # Compute Refrenc
    # Mean
    Mean = chaospy.E(PCEModel, distribution)
    print("\nMean : ",Mean)
    
    # Std
    Std = chaospy.Std(PCEModel, distribution)
    print("Std : ",Std)
    
    
    ## Validate
    hdf5File = "./data/ValidationSets/ExpDesign_ffpm-stokespnm-testset_Calibration.hdf5"
    f = h5py.File(hdf5File, 'r+')
    vallidSamples = np.array(f["EDX/init_"]).T
    Y_Val_ = np.array(f["EDY/"+var+"/init_"])[:,0]* factor
    f.close()
    
    # Evaluate PCEModel
    Y_PC_Val_ = PCEModel(*vallidSamples)
    
    R2 = r2_score(Y_PC_Val_, Y_Val_)
    NofPredictors =len(PCEModel.todict())
    TotalSampleSize = vallidSamples.shape[1]
    AdjR2 = 1 - (1 - R2) * (TotalSampleSize - 1)/(TotalSampleSize - NofPredictors - 1)
    RMSE = np.sqrt(mean_squared_error(Y_PC_Val_, Y_Val_))
    
    validError = mean_squared_error(Y_Val_, Y_PC_Val_)/\
            np.var(Y_Val_,ddof=1)
            
    print("\nR2: {0:.3f}".format(R2))
    print("RMSE: {0:.3e}".format(RMSE))
    print("Valid error: {0:.3e}".format(validError))
    
    
    # plotter(PCEModel, validsamples)
    
    