#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Based on Xu, T., A. J. Valocchi, M. Ye, and F. Liang (2017),
Quantifying model structural error: Efficient Bayesian calibration
of a regional groundwater flow model using surrogates and a data-driven error model,
Water Resour. Res., 53, 4084–4105, doi:http://doi.wiley.com/10.1002/2016WR019831

@author: Farid Mohammadi, M.Sc.
E-Mail Address: farid.mohammadi@iws.uni-stuttgart.de
Created on Tue Dec 14 2021
"""
import joblib,sys,os
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans,MiniBatchKMeans,DBSCAN,OPTICS
from sklearn.preprocessing import StandardScaler
import h5py
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from tqdm import tqdm

# Add BayesValidRox path
sys.path.insert(0,'./../../../BayesValidRox/')

inletLoc = 'top' # top or left
inclusion = 'squared' # squared or circular
n_clusters_max = 100
OutputNames = ['velocity [m/s]', 'p']
case = 'Calib' #'Calib' or 'Valid'

Name = 'stokesdarcyBJ' #stokesdarcyER stokesdarcyBJ stokespnm stokespnmNA

# resultFolder = '../'
resultFolder = '../Results_30_12_2021_topInflow/'
modelName = 'ffpm-{}_{}_inclusion_{}Inflow'.format(Name,inclusion,inletLoc)
path = '{}outputs_{}/'.format(resultFolder,modelName)
outDir = 'discrepancy_diagnosis_{}_{}_{}/'.format(Name,inclusion,inletLoc)
os.makedirs(outDir,exist_ok=True)
if case.lower() != 'valid':
    pkl_name = '{}PA_A_Bayes{}.pkl'.format(path,modelName)
else:
    pkl_name = '{}PA_A_Bayes{}-valid.pkl'.format(path,modelName)
    
with open(pkl_name, 'rb') as input:
    BayesCalib = joblib.load(input)


PCEModel = BayesCalib.PCEModel
errorMetaModel = BayesCalib.errorMetaModel
BiasInputs = BayesCalib.BiasInputs
EDY = PCEModel.ExpDesign.Y
x_values = EDY['x_values']
posteriorSamples = BayesCalib.Posterior_df.values
PCEModel.ModelObj.Output.Names = OutputNames
NofPa = PCEModel.NofPa
posterior = posteriorSamples[:,:NofPa]
sigma2s = posteriorSamples[:,NofPa:]


# Left hand term in Equation A3
# Load Post pred file
modelName += '-valid' if case.lower() == 'valid' else ''
directory = path+'Outputs_Bayes_'+modelName+'_'+case
f_withnoise = h5py.File(directory+'/'+"postPredictive.hdf5", 'r+')
f_wo_noise = h5py.File(directory+'/'+"postPredictive_wo_noise.hdf5", 'r+')
Mu_total, V_total = {},{}
postPreds, postPreds_noise_free = {},{}
for i,out in enumerate(OutputNames):
    postPreds[out] = np.array(f_withnoise["EDY/"+out])
    postPreds_noise_free[out] = np.array(f_wo_noise["EDY/"+out])
    V_total[out] = np.var(postPreds[out], axis=0)
f_withnoise.close()
f_wo_noise.close()

# First term in Equation A3
V_noise = {}
for i,out in enumerate(OutputNames):
    weights = BayesCalib.Discrepancy.TotalSigma2[out]
    if case.lower() == 'valid':
        sigma2 = weights[~np.isnan(weights)].reshape(1,-1)
    else:
        sigma2 = np.multiply(weights[~np.isnan(weights)],
                              sigma2s[:,i].reshape(-1,1))
    
    V_noise[out] = np.mean(sigma2, axis=0)
    
    V_noise[out] /= V_total[out]

# Expected surrogate model 
# V_surrogate = {}
# for i,out in enumerate(OutputNames):
#     if PCEModel.RMSE is not None:
#         surr_sigma2 = PCEModel.RMSE[out]
#     else:
#         surr_sigma2 = BayesCalib._stdPCEPriorPred[out]
#     V_surrogate[out] = np.mean(surr_sigma2**3, axis=0)/V_total[out]

# Cluster the post distribution with k-means
scaler = StandardScaler()
scaled_features = scaler.fit_transform(posterior)
kmeans_kwargs = {
    "init": "random",
    "n_init": 10,
    "max_iter": 300,
    "random_state": 42,
}

# A list holds the SSE values for each k
sseold = -np.inf
for k in range(1, n_clusters_max+1):
    kmeans = MiniBatchKMeans(n_clusters=k, **kmeans_kwargs)
    kmeans.fit(scaled_features)
    sse = kmeans.inertia_
    converged = np.all(np.abs(sseold - sse) / sse < 0.01)
    if converged:
        break
    sseold = sse
n_clusters = k
# kmeans = DBSCAN().fit(scaled_features)

# Report clustering 
print("-"*20)
print("Converged at {} clusters.".format(n_clusters))
print("The lowest SSE value: {0:.2f}".format(kmeans.inertia_))
print("The number of iterations required to converge: {}".format(kmeans.n_iter_))
print("-"*20)

# compute corrected values
mean = {out:np.zeros((n_clusters,EDY[out].shape[1])) for out in OutputNames}
var = {out:np.zeros((n_clusters,EDY[out].shape[1])) for out in OutputNames}
for i in tqdm(range(n_clusters),ascii=True, desc ="Binning method to approximately calculate variance decomposition"):
    indices = np.where(kmeans.labels_==i)[0]
    for out in OutputNames:
        preds = postPreds_noise_free[out][indices]
        if PCEModel.RMSE is not None:
            cov = np.diag(PCEModel.RMSE[out]**3)
            preds -= np.random.multivariate_normal(np.zeros(preds.shape[1]), cov, len(preds))
        else:
            stds = BayesCalib._stdPCEPriorPred[out][indices]
            for j,pred in enumerate(preds):
                cov = np.diag(stds[i]**3)
                preds[j] -= np.random.multivariate_normal(np.zeros(preds.shape[1]), 
                                                          cov, 1)[0]
            
        mean[out][i] = np.mean(preds,axis=0)
        var[out][i]  = np.var(preds,axis=0)

# Second and third term in Equation (A3)
V_b_star,V_theta = {},{}
for out in OutputNames:
    V_b_star[out] = np.mean(var[out],axis=0)/V_total[out]
    V_theta[out] = np.mean((mean[out]-np.mean(mean[out],axis=0))**2,axis=0)/V_total[out]

#------------------------------------
#-------- Plot diagnosis ------------
#------------------------------------
# plot variance decomposition
pdf = PdfPages(outDir+"varDesomposition_{}.pdf".format(case))
my_cmap = plt.get_cmap("Dark2")
fig = plt.figure()
for out in OutputNames:
    plt.bar(x_values[out], V_theta[out], width=0.5,color=my_cmap.colors[0])
    # plt.bar(x_values[out], V_surrogate[out], width=0.5, bottom=V_theta[out],color=my_cmap.colors[1])
    plt.bar(x_values[out], V_b_star[out], width=0.5, bottom=V_theta[out], color=my_cmap.colors[2])
    plt.bar(x_values[out], V_noise[out], width=0.5, bottom=V_theta[out]+V_b_star[out], color=my_cmap.colors[3])
    plt.xlabel("Point ID")
    plt.ylabel("Fraction")
    plt.legend(["Parameter","Model structure","Unresolvable"])
    plt.title("Decomposition of prediction variance for {}".format(out))
    plt.xticks(x_values[out])
    # save the current figure
    pdf.savefig(fig, bbox_inches='tight')
    # Destroy the current plot
    plt.clf()
pdf.close()


if case == 'Calib':
    PCEModel.ModelObj.MeasurementFile = '../{}'.format(PCEModel.ModelObj.MeasurementFile)
else:
    PCEModel.ModelObj.MeasurementFile = '../{}'.format(PCEModel.ModelObj.MeasurementFileValid)
refData = pd.read_csv(PCEModel.ModelObj.MeasurementFile)

y_hat, y_std = PCEModel.eval_metamodel(samples=posterior,name=case)

# plot expected Model discrepancy
pdf = PdfPages(outDir+"expectedDiscrepancy_{}.pdf".format(case))
my_cmap = plt.get_cmap("Dark2")
width=0.35
fig = plt.figure()


for out in OutputNames:
    data = refData[out][~np.isnan(refData[out])].values
    diff = (postPreds_noise_free[out]-y_hat[out])
    meanPred = np.mean(diff,axis=0)
    stdPred = np.std(diff,axis=0)
    plt.bar(np.array(x_values[out]), meanPred, yerr=stdPred,
            width=width, color=my_cmap.colors[0], capsize=7)
    plt.axhline(y=0,color='r',lw=2)
    plt.xlabel("Point ID")
    plt.ylabel(out) if out != 'p' else plt.ylabel('pressure [bar]')
    plt.title("Expected Discrepancy for {}".format(out))
    plt.xticks(x_values[out])
    plt.ticklabel_format(axis="y", style="sci", scilimits=(0,0))
    # save the current figure
    pdf.savefig(fig, bbox_inches='tight')
    # Destroy the current plot
    plt.clf()
pdf.close()

# plot expected Model discrepancy
pdf = PdfPages(outDir+"predAbility_{}.pdf".format(case))
my_cmap = plt.get_cmap("Dark2")
width=0.35
fig = plt.figure()
for out in OutputNames:
    data = refData[out][~np.isnan(refData[out])].values
    meanPred = np.mean(postPreds_noise_free[out],axis=0)
    stdPred = np.std(postPreds_noise_free[out],axis=0)
    plt.bar(np.array(x_values[out])-0.5*width, meanPred, yerr=stdPred,
            width=width, color=my_cmap.colors[1], capsize=7)
    plt.bar(np.array(x_values[out])+0.5*width, data,
            width,color=my_cmap.colors[0])
    plt.xlabel("Point ID")
    plt.ylabel(out) if out != 'p' else plt.ylabel('pressure [bar]')
    plt.legend(["Simulation","Ref. Data"])
    plt.title("Prediction vs Ref. Data for {}".format(out))
    plt.xticks(x_values[out])
    # save the current figure
    pdf.savefig(fig, bbox_inches='tight')
    # Destroy the current plot
    plt.clf()
pdf.close()