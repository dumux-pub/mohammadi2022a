#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 18 16:19:43 2021

@author: farid
"""

import sys, os, joblib
import numpy as np
import scipy.stats as stats
import shutil
import pandas as pd
import seaborn as sns
import h5py
import matplotlib
from sklearn.metrics import mean_squared_error
from matplotlib.backends.backend_pdf import PdfPages

matplotlib.use("agg")

path = "../"
inletLoc = "top"  # top or left
inclusion = "squared"  # squared or circular

Name = "stokesdarcyBJ"  # stokesdarcyER stokesdarcyBJ stokespnm stokespnmNA
modelName = "ffpm-{}_{}_inclusion_{}Inflow".format(Name, inclusion, inletLoc)

# Load the objects
with open(path + "PCEModel_" + modelName + ".pkl", "rb") as input:
    PCEModel = joblib.load(input)

import matplotlib.pylab as plt

SIZE = 30
plt.rc("figure", figsize=(24, 16))
plt.rc("font", family="serif", serif="Arial")
plt.rc("font", size=SIZE)
plt.rc("text", usetex=True)
plt.rc("axes", linewidth=3)
plt.rc("axes", grid=True)
plt.rc("grid", linestyle="-")
plt.rc("axes", titlesize=SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=SIZE)  # fontsize of the tick labels
plt.rc("legend", fontsize=SIZE)  # legend fontsize
plt.rc("figure", titlesize=SIZE)  # fontsize of the figure title

# Add BayesValidRox path
sys.path.insert(0, "./../../../BayesValidRox/")

output = "velocity [m/s]"  #'velocity [m/s]' 'p'

EDX = PCEModel.ExpDesign.X
EDY = PCEModel.ExpDesign.Y[output]

# Data
data = pd.read_csv("../" + PCEModel.ModelObj.MeasurementFile)

pdf = PdfPages("{}_Simulations_vs_PCE.pdf".format(Name))

# Load Validsets
HDF5File = "ExpDesign_" + modelName + "-testset_Calibration.hdf5"
ValidSets = h5py.File("../data/ValidationSets/" + HDF5File, "r+")
validSamples = np.array(ValidSets["EDX/init_"])

EDX = validSamples
EDY = np.array(ValidSets["EDY/" + output + "/init_"])
ValidSets.close()

critIdx = [EDX[:, 1] >= 0.005]
# critIdx = [EDX[:,1]<0.005]

# Critical Velocities
critEDX = EDX  # [critIdx]
critEDY = EDY  # [critIdx]

# Plot for each Point ID
print("\n>>>>> Errors of {} <<<<<".format(output))
print("\nIndex  |  RMSE")
print("-" * 35)
for idx in range(1, 11):

    # Run the surrogate model
    y_hat, y_std = PCEModel.eval_metamodel(samples=critEDX)

    # Plot the predictions vs simulations
    fig, ax = plt.subplots()
    plt.title("Point ID: {}".format(idx))

    # Simulations
    plt.plot(
        critEDY[:, idx - 1], color="navy", marker="x", linewidth=2, label="Simulation"
    )

    # Plot the predition and its uncertainty
    plt.plot(
        y_hat[output][:, idx - 1],
        color="green",
        ls="--",
        marker="x",
        linewidth=2,
        label="Surrogate Approximation",
    )

    plt.fill_between(
        np.arange(len(critEDY)),
        y_hat[output][:, idx - 1] - 1 * y_std[output][:, idx - 1],
        y_hat[output][:, idx - 1] + 1 * y_std[output][:, idx - 1],
        color="green",
        alpha=0.25,
    )

    RMSE = mean_squared_error(
        critEDY[:, idx - 1],
        y_hat[output][:, idx - 1],
        squared=False,
        multioutput="raw_values",
    )

    # Plot data
    plt.hlines(
        data[output][idx - 1],
        0,
        len(critEDY[:, idx - 1]),
        linewidth=3,
        color="red",
        label="Ref. data",
    )

    # Print a report table
    print("\n".join("{0}  |  {1:.3e}".format(idx, k) for i, k in enumerate(RMSE)))
    # plt.plot(np.arange(len(critEDY),len(EDX)), EDY[normIdx][:,idx-1], color='red', linewidth=2)

    plt.ylabel(output)
    plt.xlabel("Run number")
    plt.legend(loc="best")
    plt.show()

    # save the current figure
    pdf.savefig(fig, bbox_inches="tight")
    # Destroy the current plot
    plt.clf()

pdf.close()


# df1 = pd.DataFrame(PCEModel.ExpDesign.X)
# df2 = pd.DataFrame(critEDX)
# df = pd.concat([df1.assign(dataset='EDX'), df2.assign(dataset='ValidSet')])
# g = sns.pairplot(df, hue='dataset')
# g.savefig('validSamples.svg',bbox_inches='tight')
